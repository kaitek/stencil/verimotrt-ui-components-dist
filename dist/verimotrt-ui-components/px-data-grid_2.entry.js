import{r as t,c as e,h as i,g as s}from"./index-3fe55327.js";import{b as r,t as o,F as n,D as a,c as l,m as h,h as d,e as c,f as u,i as p,g as m,G as g,d as f,C as v,I as b,j as A,k as x,B as y,l as w,u as _,n as C}from"./iron-resizable-behavior-f009b1bb.js";
/**
@license
Copyright (c) 2020 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/class k{static detectScrollType(){const t=document.createElement("div");t.textContent="ABCD",t.dir="rtl",t.style.fontSize="14px",t.style.width="4px",t.style.height="1px",t.style.position="absolute",t.style.top="-1000px",t.style.overflow="scroll",document.body.appendChild(t);let e="reverse";return t.scrollLeft>0?e="default":(t.scrollLeft=2,t.scrollLeft<2&&(e="negative")),document.body.removeChild(t),e}static getNormalizedScrollLeft(t,e,i){const{scrollLeft:s}=i;if("rtl"!==e||!t)return s;switch(t){case"negative":return i.scrollWidth-i.clientWidth+s;case"reverse":return i.scrollWidth-i.clientWidth-s}return s}static setNormalizedScrollLeft(t,e,i,s){if("rtl"===e&&t)switch(t){case"negative":i.scrollLeft=i.clientWidth-i.scrollWidth+s;break;case"reverse":i.scrollLeft=i.scrollWidth-i.clientWidth-s;break;default:i.scrollLeft=s}else i.scrollLeft=s}}const z=[];let I;new MutationObserver((function(){const t=T();z.forEach(e=>{S(e,t)})})).observe(document.documentElement,{attributes:!0,attributeFilter:["dir"]});const S=function(t,e){e?t.setAttribute("dir",e):t.removeAttribute("dir")},T=function(){return document.documentElement.getAttribute("dir")},B=t=>class extends t{static get properties(){return{dir:{type:String,readOnly:!0}}}static finalize(){super.finalize(),I||(I=k.detectScrollType())}connectedCallback(){super.connectedCallback(),this.hasAttribute("dir")||(this.__subscribe(),S(this,T()))}attributeChangedCallback(t,e,i){if(super.attributeChangedCallback(t,e,i),"dir"!==t)return;const s=i===T()&&-1===z.indexOf(this),r=!i&&e&&-1===z.indexOf(this),o=i!==T()&&e===T();s||r?(this.__subscribe(),S(this,T())):o&&this.__subscribe(!1)}disconnectedCallback(){super.disconnectedCallback(),this.__subscribe(!1),this.removeAttribute("dir")}__subscribe(t=!0){t?-1===z.indexOf(this)&&z.push(this):z.indexOf(this)>-1&&z.splice(z.indexOf(this),1)}__getNormalizedScrollLeft(t){return k.getNormalizedScrollLeft(I,this.getAttribute("dir")||"ltr",t)}__setNormalizedScrollLeft(t,e){return k.setNormalizedScrollLeft(I,this.getAttribute("dir")||"ltr",t,e)}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/;class E extends class extends r{}{static get is(){return"vaadin-grid-templatizer"}static get properties(){return{dataHost:Object,template:Object,_templateInstances:{type:Array,value:function(){return[]}},_parentPathValues:{value:function(){return{}}},_grid:Object}}static get observers(){return["_templateInstancesChanged(_templateInstances.*, _parentPathValues.*)"]}constructor(){super(),this._instanceProps={detailsOpened:!0,index:!0,item:!0,selected:!0,expanded:!0,level:!0}}createInstance(){this._ensureTemplatized();const t=new this._TemplateClass({});return this.addInstance(t),t}addInstance(t){-1===this._templateInstances.indexOf(t)&&(this._templateInstances.push(t),requestAnimationFrame(()=>this.notifyPath("_templateInstances.*",this._templateInstances)))}removeInstance(t){const e=this._templateInstances.indexOf(t);this.splice("_templateInstances",e,1)}_ensureTemplatized(){this._TemplateClass||(this._TemplateClass=o(this.template,this,{instanceProps:this._instanceProps,parentModel:!0,forwardHostProp:function(t,e){this._forwardParentProp(t,e),this._templateInstances&&this._templateInstances.forEach(i=>i.notifyPath(t,e))},notifyInstanceProp:function(t,e,i){if("index"===e||"item"===e)return;const s=`__${e}__`;if(t[s]===i)return;t[s]=i;const r=Array.from(this._grid.$.items.children).filter(e=>this._grid._itemsEqual(e._item,t.item))[0];if(r&&Array.from(r.children).forEach(t=>{t._instance&&(t._instance[s]=i,t._instance.notifyPath(e,i))}),Array.isArray(this._grid.items)&&0===e.indexOf("item.")){const s=this._grid.items.indexOf(t.item),r=e.slice("item.".length);this._grid.notifyPath(`items.${s}.${r}`,i)}const o=`_${e}InstanceChangedCallback`;this._grid&&this._grid[o]&&this._grid[o](t,i)}}))}_forwardParentProp(t,e){this._parentPathValues[t]=e,this._templateInstances.forEach(i=>i.notifyPath(t,e))}_templateInstancesChanged(t,e){let i,s;if("_templateInstances"===t.path)i=0,s=this._templateInstances.length;else{if("_templateInstances.splices"!==t.path)return;i=t.value.index,s=t.value.addedCount}Object.keys(this._parentPathValues||{}).forEach(t=>{for(var e=i;e<i+s;e++)this._templateInstances[e].set(t,this._parentPathValues[t])})}}customElements.define(E.is,E);
/**
@license
Copyright (c) 2018 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
const F=t=>class extends t{static get properties(){return{resizable:{type:Boolean,value:function(){if("vaadin-grid-column-group"===this.localName)return;const t=this.parentNode;return t&&"vaadin-grid-column-group"===t.localName&&t.resizable||!1}},_headerTemplate:{type:Object},_footerTemplate:{type:Object},frozen:{type:Boolean,value:!1},hidden:{type:Boolean},header:{type:String},textAlign:{type:String},_lastFrozen:{type:Boolean,value:!1},_order:Number,_reorderStatus:Boolean,_emptyCells:Array,_headerCell:Object,_footerCell:Object,_grid:Object,headerRenderer:Function,footerRenderer:Function}}static get observers(){return["_widthChanged(width, _headerCell, _footerCell, _cells.*)","_frozenChanged(frozen, _headerCell, _footerCell, _cells.*)","_flexGrowChanged(flexGrow, _headerCell, _footerCell, _cells.*)","_pathOrHeaderChanged(path, header, _headerCell, _footerCell, _cells.*, renderer, headerRenderer, _bodyTemplate, _headerTemplate)","_textAlignChanged(textAlign, _cells.*, _headerCell, _footerCell)","_orderChanged(_order, _headerCell, _footerCell, _cells.*)","_lastFrozenChanged(_lastFrozen)","_setBodyTemplateOrRenderer(_bodyTemplate, renderer, _cells, _cells.*)","_setHeaderTemplateOrRenderer(_headerTemplate, headerRenderer, _headerCell)","_setFooterTemplateOrRenderer(_footerTemplate, footerRenderer, _footerCell)","_resizableChanged(resizable, _headerCell)","_reorderStatusChanged(_reorderStatus, _headerCell, _footerCell, _cells.*)","_hiddenChanged(hidden, _headerCell, _footerCell, _cells.*)"]}connectedCallback(){super.connectedCallback(),this._bodyTemplate&&(this._bodyTemplate.templatizer._grid=this._grid),this._headerTemplate&&(this._headerTemplate.templatizer._grid=this._grid),this._footerTemplate&&(this._footerTemplate.templatizer._grid=this._grid),this._templateObserver.flush(),this._bodyTemplate||this._templateObserver.callback(),requestAnimationFrame(()=>{this._allCells.forEach(t=>{t._content.parentNode||this._grid&&this._grid.appendChild(t._content)})})}disconnectedCallback(){super.disconnectedCallback(),requestAnimationFrame(()=>{this._findHostGrid()||this._allCells.forEach(t=>{t._content.parentNode&&t._content.parentNode.removeChild(t._content)})}),this._gridValue=void 0}_findHostGrid(){let t=this;for(;t&&!/^vaadin.*grid(-pro)?$/.test(t.localName);)t=t.assignedSlot?t.assignedSlot.parentNode:t.parentNode;return t||void 0}get _grid(){return this._gridValue||(this._gridValue=this._findHostGrid()),this._gridValue}get _allCells(){return[].concat(this._cells||[]).concat(this._emptyCells||[]).concat(this._headerCell).concat(this._footerCell).filter(t=>t)}constructor(){super(),this._templateObserver=new n(this,()=>{this._headerTemplate=this._prepareHeaderTemplate(),this._footerTemplate=this._prepareFooterTemplate(),this._bodyTemplate=this._prepareBodyTemplate()})}_prepareHeaderTemplate(){return this._prepareTemplatizer(this._findTemplate(!0)||null,{})}_prepareFooterTemplate(){return this._prepareTemplatizer(this._findTemplate(!1,!0)||null,{})}_prepareBodyTemplate(){return this._prepareTemplatizer(this._findTemplate()||null)}_prepareTemplatizer(t,e){if(t&&!t.templatizer){const i=new E;i._grid=this._grid,i.dataHost=this.dataHost,i._instanceProps=e||i._instanceProps,i.template=t,t.templatizer=i}return t}_renderHeaderAndFooter(){this.headerRenderer&&this._headerCell&&this.__runRenderer(this.headerRenderer,this._headerCell),this.footerRenderer&&this._footerCell&&this.__runRenderer(this.footerRenderer,this._footerCell)}__runRenderer(t,e,i){const s=[e._content,this];i&&i.item&&s.push(i),t.apply(this,s)}__setColumnTemplateOrRenderer(t,e,i){if(t&&e)throw new Error("You should only use either a renderer or a template");i.forEach(i=>{const s=this._grid.__getRowModel(i.parentElement);if(e)i._renderer=e,(s.item||e===this.headerRenderer||e===this.footerRenderer)&&this.__runRenderer(e,i,s);else if(i._template!==t){i._template=t,i._content.innerHTML="",t.templatizer._grid=t.templatizer._grid||this._grid;const e=t.templatizer.createInstance();i._content.appendChild(e.root),i._instance=e,s.item&&i._instance.setProperties(s)}})}_setBodyTemplateOrRenderer(t,e,i,s){(t||e)&&i&&this.__setColumnTemplateOrRenderer(t,e,i)}_setHeaderTemplateOrRenderer(t,e,i){(t||e)&&i&&this.__setColumnTemplateOrRenderer(t,e,[i])}_setFooterTemplateOrRenderer(t,e,i){(t||e)&&i&&(this.__setColumnTemplateOrRenderer(t,e,[i]),this._grid.__updateHeaderFooterRowVisibility(i.parentElement))}_selectFirstTemplate(t=!1,e=!1){return n.getFlattenedNodes(this).filter(i=>"template"===i.localName&&i.classList.contains("header")===t&&i.classList.contains("footer")===e)[0]}_findTemplate(t,e){const i=this._selectFirstTemplate(t,e);return i&&this.dataHost&&(i._rootDataHost=this.dataHost._rootDataHost||this.dataHost),i}_flexGrowChanged(t,e,i,s){this.parentElement&&this.parentElement._columnPropChanged&&this.parentElement._columnPropChanged("flexGrow"),this._allCells.forEach(e=>e.style.flexGrow=t)}_orderChanged(t,e,i,s){this._allCells.forEach(e=>e.style.order=t)}_widthChanged(t,e,i,s){this.parentElement&&this.parentElement._columnPropChanged&&this.parentElement._columnPropChanged("width"),this._allCells.forEach(e=>e.style.width=t),this._grid&&this._grid.__forceReflow&&this._grid.__forceReflow()}_frozenChanged(t,e,i,s){this.parentElement&&this.parentElement._columnPropChanged&&this.parentElement._columnPropChanged("frozen",t),this._allCells.forEach(e=>this._toggleAttribute("frozen",t,e)),this._grid&&this._grid._frozenCellsChanged&&this._grid._frozenCellsChanged()}_lastFrozenChanged(t){this._allCells.forEach(e=>this._toggleAttribute("last-frozen",t,e)),this.parentElement&&this.parentElement._columnPropChanged&&(this.parentElement._lastFrozen=t)}_pathOrHeaderChanged(t,e,i,s,r,o,n,a,l){const h=void 0!==e;!n&&!l&&h&&i&&this.__setTextContent(i._content,e),t&&r.value&&(o||a||this.__setColumnTemplateOrRenderer(void 0,(e,i,{item:s})=>this.__setTextContent(e,this.get(t,s)),r.value),n||l||h||!i||null===e||this.__setTextContent(i._content,this._generateHeader(t))),i&&this._grid.__updateHeaderFooterRowVisibility(i.parentElement)}__setTextContent(t,e){t.textContent!==e&&(t.textContent=e)}_generateHeader(t){return t.substr(t.lastIndexOf(".")+1).replace(/([A-Z])/g,"-$1").toLowerCase().replace(/-/g," ").replace(/^./,t=>t.toUpperCase())}_toggleAttribute(t,e,i){i.hasAttribute(t)===!e&&(e?i.setAttribute(t,""):i.removeAttribute(t))}_reorderStatusChanged(t,e,i,s){this._allCells.forEach(e=>e.setAttribute("reorder-status",t))}_resizableChanged(t,e){void 0!==t&&void 0!==e&&e&&[e].concat(this._emptyCells).forEach(e=>{if(e){const i=e.querySelector('[part~="resize-handle"]');if(i&&e.removeChild(i),t){const t=document.createElement("div");t.setAttribute("part","resize-handle"),e.appendChild(t)}}})}_textAlignChanged(t,e,i,s){if(void 0===t)return;if(-1===["start","end","center"].indexOf(t))return void console.warn('textAlign can only be set as "start", "end" or "center"');let r;"ltr"===getComputedStyle(this._grid).direction?"start"===t?r="left":"end"===t&&(r="right"):"start"===t?r="right":"end"===t&&(r="left"),this._allCells.forEach(e=>{e._content.style.textAlign=t,getComputedStyle(e._content).textAlign!==t&&(e._content.style.textAlign=r)})}_hiddenChanged(t,e,i,s){this.parentElement&&this.parentElement._columnPropChanged&&this.parentElement._columnPropChanged("hidden",t),!!t!=!!this._previousHidden&&this._grid&&(!0===t&&this._allCells.forEach(t=>{t._content.parentNode&&t._content.parentNode.removeChild(t._content)}),this._grid._debouncerHiddenChanged=a.debounce(this._grid._debouncerHiddenChanged,l,()=>{this._grid&&this._grid._renderColumnTree&&this._grid._renderColumnTree(this._grid._columnTree)}),this._grid._updateLastFrozen&&this._grid._updateLastFrozen(),this._grid.notifyResize&&this._grid.notifyResize(),this._grid._resetKeyboardNavigation&&this._grid._resetKeyboardNavigation()),this._previousHidden=t}};class M extends(F(B(r))){static get is(){return"vaadin-grid-column"}static get properties(){return{width:{type:String,value:"100px"},flexGrow:{type:Number,value:1},renderer:Function,path:{type:String},autoWidth:{type:Boolean,value:!1},_bodyTemplate:{type:Object},_cells:Array}}}customElements.define(M.is,M);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
class G extends(F(r)){static get is(){return"vaadin-grid-column-group"}static get properties(){return{_childColumns:{value:function(){return this._getChildColumns(this)}},flexGrow:{type:Number,readOnly:!0},width:{type:String,readOnly:!0},_visibleChildColumns:Array,_colSpan:Number,_rootColumns:Array}}static get observers(){return["_updateVisibleChildColumns(_childColumns)","_childColumnsChanged(_childColumns)","_groupFrozenChanged(frozen, _rootColumns)","_groupHiddenChanged(hidden, _rootColumns)","_visibleChildColumnsChanged(_visibleChildColumns)","_colSpanChanged(_colSpan, _headerCell, _footerCell)","_groupOrderChanged(_order, _rootColumns)","_groupReorderStatusChanged(_reorderStatus, _rootColumns)","_groupResizableChanged(resizable, _rootColumns)"]}connectedCallback(){super.connectedCallback(),this._addNodeObserver(),this._updateFlexAndWidth()}disconnectedCallback(){super.disconnectedCallback(),this._observer&&this._observer.disconnect()}_columnPropChanged(t,e){"hidden"===t&&(this._preventHiddenCascade=!0,this._updateVisibleChildColumns(this._childColumns),this._preventHiddenCascade=!1),/flexGrow|width|hidden|_childColumns/.test(t)&&this._updateFlexAndWidth(),"frozen"===t&&(this.frozen=this.frozen||e),"lastFrozen"===t&&(this._lastFrozen=this._lastFrozen||e)}_groupOrderChanged(t,e){if(e){const i=e.slice(0);if(!t)return void i.forEach(t=>t._order=0);const s=/(0+)$/.exec(t).pop().length,r=1+~~(Math.log(e.length)/Math.log(Math.LN10)),o=Math.pow(10,s-r);i[0]&&i[0]._order&&i.sort((t,e)=>t._order-e._order),i.forEach((e,i)=>e._order=t+(i+1)*o)}}_groupReorderStatusChanged(t,e){void 0!==t&&void 0!==e&&e.forEach(e=>e._reorderStatus=t)}_groupResizableChanged(t,e){void 0!==t&&void 0!==e&&e.forEach(e=>e.resizable=t)}_updateVisibleChildColumns(t){this._visibleChildColumns=Array.prototype.filter.call(t,t=>!t.hidden)}_childColumnsChanged(t){!this._autoHidden&&this.hidden&&(Array.prototype.forEach.call(t,t=>t.hidden=!0),this._updateVisibleChildColumns(t))}_updateFlexAndWidth(){this._visibleChildColumns&&(this._setWidth(this._visibleChildColumns.length?"calc("+Array.prototype.reduce.call(this._visibleChildColumns,(t,e)=>t+" + "+(e.width||"0px").replace("calc",""),"").substring(3)+")":"0px"),this._setFlexGrow(Array.prototype.reduce.call(this._visibleChildColumns,(t,e)=>t+e.flexGrow,0)))}_groupFrozenChanged(t,e){void 0!==e&&void 0!==t&&!1!==t&&Array.from(e).forEach(e=>e.frozen=t)}_groupHiddenChanged(t,e){e&&!this._preventHiddenCascade&&(this._ignoreVisibleChildColumns=!0,e.forEach(e=>e.hidden=t),this._ignoreVisibleChildColumns=!1),this._columnPropChanged("hidden")}_visibleChildColumnsChanged(t){this._colSpan=t.length,this._ignoreVisibleChildColumns||(0===t.length?this._autoHidden=this.hidden=!0:this.hidden&&this._autoHidden&&(this._autoHidden=this.hidden=!1))}_colSpanChanged(t,e,i){e&&(e.setAttribute("colspan",t),this._grid&&this._grid._a11yUpdateCellColspan(e,t)),i&&(i.setAttribute("colspan",t),this._grid&&this._grid._a11yUpdateCellColspan(i,t))}_getChildColumns(t){return n.getFlattenedNodes(t).filter(this._isColumnElement)}_addNodeObserver(){this._observer=new n(this,t=>{(t.addedNodes.filter(this._isColumnElement).length>0||t.removedNodes.filter(this._isColumnElement).length>0)&&(this._preventHiddenCascade=!0,this._rootColumns=this._getChildColumns(this),this._childColumns=this._rootColumns,this._preventHiddenCascade=!1,h.run(()=>{this._grid&&this._grid._updateColumnTree&&this._grid._updateColumnTree()}))}),this._observer.flush()}_isColumnElement(t){return t.nodeType===Node.ELEMENT_NODE&&/\bcolumn\b/.test(t.localName)}}customElements.define(G.is,G);class D extends HTMLElement{static get version(){return"1.6.0"}}customElements.define("vaadin-lumo-styles",D);const O=document.createElement("template");O.innerHTML='<custom-style>\n  <style>\n    html {\n      /* Base (background) */\n      --lumo-base-color: #FFF;\n\n      /* Tint */\n      --lumo-tint-5pct: hsla(0, 0%, 100%, 0.3);\n      --lumo-tint-10pct: hsla(0, 0%, 100%, 0.37);\n      --lumo-tint-20pct: hsla(0, 0%, 100%, 0.44);\n      --lumo-tint-30pct: hsla(0, 0%, 100%, 0.5);\n      --lumo-tint-40pct: hsla(0, 0%, 100%, 0.57);\n      --lumo-tint-50pct: hsla(0, 0%, 100%, 0.64);\n      --lumo-tint-60pct: hsla(0, 0%, 100%, 0.7);\n      --lumo-tint-70pct: hsla(0, 0%, 100%, 0.77);\n      --lumo-tint-80pct: hsla(0, 0%, 100%, 0.84);\n      --lumo-tint-90pct: hsla(0, 0%, 100%, 0.9);\n      --lumo-tint: #FFF;\n\n      /* Shade */\n      --lumo-shade-5pct: hsla(214, 61%, 25%, 0.05);\n      --lumo-shade-10pct: hsla(214, 57%, 24%, 0.1);\n      --lumo-shade-20pct: hsla(214, 53%, 23%, 0.16);\n      --lumo-shade-30pct: hsla(214, 50%, 22%, 0.26);\n      --lumo-shade-40pct: hsla(214, 47%, 21%, 0.38);\n      --lumo-shade-50pct: hsla(214, 45%, 20%, 0.5);\n      --lumo-shade-60pct: hsla(214, 43%, 19%, 0.61);\n      --lumo-shade-70pct: hsla(214, 42%, 18%, 0.72);\n      --lumo-shade-80pct: hsla(214, 41%, 17%, 0.83);\n      --lumo-shade-90pct: hsla(214, 40%, 16%, 0.94);\n      --lumo-shade: hsl(214, 35%, 15%);\n\n      /* Contrast */\n      --lumo-contrast-5pct: var(--lumo-shade-5pct);\n      --lumo-contrast-10pct: var(--lumo-shade-10pct);\n      --lumo-contrast-20pct: var(--lumo-shade-20pct);\n      --lumo-contrast-30pct: var(--lumo-shade-30pct);\n      --lumo-contrast-40pct: var(--lumo-shade-40pct);\n      --lumo-contrast-50pct: var(--lumo-shade-50pct);\n      --lumo-contrast-60pct: var(--lumo-shade-60pct);\n      --lumo-contrast-70pct: var(--lumo-shade-70pct);\n      --lumo-contrast-80pct: var(--lumo-shade-80pct);\n      --lumo-contrast-90pct: var(--lumo-shade-90pct);\n      --lumo-contrast: var(--lumo-shade);\n\n      /* Text */\n      --lumo-header-text-color: var(--lumo-contrast);\n      --lumo-body-text-color: var(--lumo-contrast-90pct);\n      --lumo-secondary-text-color: var(--lumo-contrast-70pct);\n      --lumo-tertiary-text-color: var(--lumo-contrast-50pct);\n      --lumo-disabled-text-color: var(--lumo-contrast-30pct);\n\n      /* Primary */\n      --lumo-primary-color: hsl(214, 90%, 52%);\n      --lumo-primary-color-50pct: hsla(214, 90%, 52%, 0.5);\n      --lumo-primary-color-10pct: hsla(214, 90%, 52%, 0.1);\n      --lumo-primary-text-color: var(--lumo-primary-color);\n      --lumo-primary-contrast-color: #FFF;\n\n      /* Error */\n      --lumo-error-color: hsl(3, 100%, 61%);\n      --lumo-error-color-50pct: hsla(3, 100%, 60%, 0.5);\n      --lumo-error-color-10pct: hsla(3, 100%, 60%, 0.1);\n      --lumo-error-text-color: hsl(3, 92%, 53%);\n      --lumo-error-contrast-color: #FFF;\n\n      /* Success */\n      --lumo-success-color: hsl(145, 80%, 42%); /* hsl(144,82%,37%); */\n      --lumo-success-color-50pct: hsla(145, 76%, 44%, 0.55);\n      --lumo-success-color-10pct: hsla(145, 76%, 44%, 0.12);\n      --lumo-success-text-color: hsl(145, 100%, 32%);\n      --lumo-success-contrast-color: #FFF;\n    }\n  </style>\n</custom-style><dom-module id="lumo-color">\n  <template>\n    <style>\n      [theme~="dark"] {\n        /* Base (background) */\n        --lumo-base-color: hsl(214, 35%, 21%);\n\n        /* Tint */\n        --lumo-tint-5pct: hsla(214, 65%, 85%, 0.06);\n        --lumo-tint-10pct: hsla(214, 60%, 80%, 0.14);\n        --lumo-tint-20pct: hsla(214, 64%, 82%, 0.23);\n        --lumo-tint-30pct: hsla(214, 69%, 84%, 0.32);\n        --lumo-tint-40pct: hsla(214, 73%, 86%, 0.41);\n        --lumo-tint-50pct: hsla(214, 78%, 88%, 0.5);\n        --lumo-tint-60pct: hsla(214, 82%, 90%, 0.6);\n        --lumo-tint-70pct: hsla(214, 87%, 92%, 0.7);\n        --lumo-tint-80pct: hsla(214, 91%, 94%, 0.8);\n        --lumo-tint-90pct: hsla(214, 96%, 96%, 0.9);\n        --lumo-tint: hsl(214, 100%, 98%);\n\n        /* Shade */\n        --lumo-shade-5pct: hsla(214, 0%, 0%, 0.07);\n        --lumo-shade-10pct: hsla(214, 4%, 2%, 0.15);\n        --lumo-shade-20pct: hsla(214, 8%, 4%, 0.23);\n        --lumo-shade-30pct: hsla(214, 12%, 6%, 0.32);\n        --lumo-shade-40pct: hsla(214, 16%, 8%, 0.41);\n        --lumo-shade-50pct: hsla(214, 20%, 10%, 0.5);\n        --lumo-shade-60pct: hsla(214, 24%, 12%, 0.6);\n        --lumo-shade-70pct: hsla(214, 28%, 13%, 0.7);\n        --lumo-shade-80pct: hsla(214, 32%, 13%, 0.8);\n        --lumo-shade-90pct: hsla(214, 33%, 13%, 0.9);\n        --lumo-shade: hsl(214, 33%, 13%);\n\n        /* Contrast */\n        --lumo-contrast-5pct: var(--lumo-tint-5pct);\n        --lumo-contrast-10pct: var(--lumo-tint-10pct);\n        --lumo-contrast-20pct: var(--lumo-tint-20pct);\n        --lumo-contrast-30pct: var(--lumo-tint-30pct);\n        --lumo-contrast-40pct: var(--lumo-tint-40pct);\n        --lumo-contrast-50pct: var(--lumo-tint-50pct);\n        --lumo-contrast-60pct: var(--lumo-tint-60pct);\n        --lumo-contrast-70pct: var(--lumo-tint-70pct);\n        --lumo-contrast-80pct: var(--lumo-tint-80pct);\n        --lumo-contrast-90pct: var(--lumo-tint-90pct);\n        --lumo-contrast: var(--lumo-tint);\n\n        /* Text */\n        --lumo-header-text-color: var(--lumo-contrast);\n        --lumo-body-text-color: var(--lumo-contrast-90pct);\n        --lumo-secondary-text-color: var(--lumo-contrast-70pct);\n        --lumo-tertiary-text-color: var(--lumo-contrast-50pct);\n        --lumo-disabled-text-color: var(--lumo-contrast-30pct);\n\n        /* Primary */\n        --lumo-primary-color: hsl(214, 86%, 55%);\n        --lumo-primary-color-50pct: hsla(214, 86%, 55%, 0.5);\n        --lumo-primary-color-10pct: hsla(214, 90%, 63%, 0.1);\n        --lumo-primary-text-color: hsl(214, 100%, 70%);\n        --lumo-primary-contrast-color: #FFF;\n\n        /* Error */\n        --lumo-error-color: hsl(3, 90%, 63%);\n        --lumo-error-color-50pct: hsla(3, 90%, 63%, 0.5);\n        --lumo-error-color-10pct: hsla(3, 90%, 63%, 0.1);\n        --lumo-error-text-color: hsl(3, 100%, 67%);\n\n        /* Success */\n        --lumo-success-color: hsl(145, 65%, 42%);\n        --lumo-success-color-50pct: hsla(145, 65%, 42%, 0.5);\n        --lumo-success-color-10pct: hsla(145, 65%, 42%, 0.1);\n        --lumo-success-text-color: hsl(145, 85%, 47%);\n      }\n\n      html {\n        color: var(--lumo-body-text-color);\n        background-color: var(--lumo-base-color);\n      }\n\n      [theme~="dark"] {\n        color: var(--lumo-body-text-color);\n        background-color: var(--lumo-base-color);\n      }\n\n      h1,\n      h2,\n      h3,\n      h4,\n      h5,\n      h6 {\n        color: var(--lumo-header-text-color);\n      }\n\n      a {\n        color: var(--lumo-primary-text-color);\n      }\n\n      blockquote {\n        color: var(--lumo-secondary-text-color);\n      }\n\n      code,\n      pre {\n        background-color: var(--lumo-contrast-10pct);\n        border-radius: var(--lumo-border-radius-m);\n      }\n    </style>\n  </template>\n</dom-module><dom-module id="lumo-color-legacy">\n  <template>\n    <style include="lumo-color">\n      :host {\n        color: var(--lumo-body-text-color) !important;\n        background-color: var(--lumo-base-color) !important;\n      }\n    </style>\n  </template>\n</dom-module>',document.head.appendChild(O.content);const R=document.createElement("template");R.innerHTML="<custom-style>\n  <style>\n    html {\n      --lumo-size-xs: 1.625rem;\n      --lumo-size-s: 1.875rem;\n      --lumo-size-m: 2.25rem;\n      --lumo-size-l: 2.75rem;\n      --lumo-size-xl: 3.5rem;\n\n      /* Icons */\n      --lumo-icon-size-s: 1.25em;\n      --lumo-icon-size-m: 1.5em;\n      --lumo-icon-size-l: 2.25em;\n      /* For backwards compatibility */\n      --lumo-icon-size: var(--lumo-icon-size-m);\n    }\n  </style>\n</custom-style>",document.head.appendChild(R.content);const H=document.createElement("template");H.innerHTML="<custom-style>\n  <style>\n    html {\n      /* Square */\n      --lumo-space-xs: 0.25rem;\n      --lumo-space-s: 0.5rem;\n      --lumo-space-m: 1rem;\n      --lumo-space-l: 1.5rem;\n      --lumo-space-xl: 2.5rem;\n\n      /* Wide */\n      --lumo-space-wide-xs: calc(var(--lumo-space-xs) / 2) var(--lumo-space-xs);\n      --lumo-space-wide-s: calc(var(--lumo-space-s) / 2) var(--lumo-space-s);\n      --lumo-space-wide-m: calc(var(--lumo-space-m) / 2) var(--lumo-space-m);\n      --lumo-space-wide-l: calc(var(--lumo-space-l) / 2) var(--lumo-space-l);\n      --lumo-space-wide-xl: calc(var(--lumo-space-xl) / 2) var(--lumo-space-xl);\n\n      /* Tall */\n      --lumo-space-tall-xs: var(--lumo-space-xs) calc(var(--lumo-space-xs) / 2);\n      --lumo-space-tall-s: var(--lumo-space-s) calc(var(--lumo-space-s) / 2);\n      --lumo-space-tall-m: var(--lumo-space-m) calc(var(--lumo-space-m) / 2);\n      --lumo-space-tall-l: var(--lumo-space-l) calc(var(--lumo-space-l) / 2);\n      --lumo-space-tall-xl: var(--lumo-space-xl) calc(var(--lumo-space-xl) / 2);\n    }\n  </style>\n</custom-style>",document.head.appendChild(H.content);const N=document.createElement("template");N.innerHTML="<custom-style>\n  <style>\n    html {\n      /* Border radius */\n      --lumo-border-radius-s: 0.25em; /* Checkbox, badge, date-picker year indicator, etc */\n      --lumo-border-radius-m: var(--lumo-border-radius, 0.25em); /* Button, text field, menu overlay, etc */\n      --lumo-border-radius-l: 0.5em; /* Dialog, notification, etc */\n      --lumo-border-radius: 0.25em; /* Deprecated */\n\n      /* Shadow */\n      --lumo-box-shadow-xs: 0 1px 4px -1px var(--lumo-shade-50pct);\n      --lumo-box-shadow-s: 0 2px 4px -1px var(--lumo-shade-20pct), 0 3px 12px -1px var(--lumo-shade-30pct);\n      --lumo-box-shadow-m: 0 2px 6px -1px var(--lumo-shade-20pct), 0 8px 24px -4px var(--lumo-shade-40pct);\n      --lumo-box-shadow-l: 0 3px 18px -2px var(--lumo-shade-20pct), 0 12px 48px -6px var(--lumo-shade-40pct);\n      --lumo-box-shadow-xl: 0 4px 24px -3px var(--lumo-shade-20pct), 0 18px 64px -8px var(--lumo-shade-40pct);\n\n      /* Clickable element cursor */\n      --lumo-clickable-cursor: default;\n    }\n  </style>\n</custom-style>",document.head.appendChild(N.content);const P=document.createElement("template");P.innerHTML='<custom-style>\n  <style>\n    html {\n      /* Font families */\n      --lumo-font-family: -apple-system, BlinkMacSystemFont, "Roboto", "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";\n\n      /* Font sizes */\n      --lumo-font-size-xxs: .75rem;\n      --lumo-font-size-xs: .8125rem;\n      --lumo-font-size-s: .875rem;\n      --lumo-font-size-m: 1rem;\n      --lumo-font-size-l: 1.125rem;\n      --lumo-font-size-xl: 1.375rem;\n      --lumo-font-size-xxl: 1.75rem;\n      --lumo-font-size-xxxl: 2.5rem;\n\n      /* Line heights */\n      --lumo-line-height-xs: 1.25;\n      --lumo-line-height-s: 1.375;\n      --lumo-line-height-m: 1.625;\n    }\n\n  </style>\n</custom-style><dom-module id="lumo-typography">\n  <template>\n    <style>\n      html {\n        font-family: var(--lumo-font-family);\n        font-size: var(--lumo-font-size, var(--lumo-font-size-m));\n        line-height: var(--lumo-line-height-m);\n        -webkit-text-size-adjust: 100%;\n        -webkit-font-smoothing: antialiased;\n        -moz-osx-font-smoothing: grayscale;\n      }\n\n      /* Can’t combine with the above selector because that doesn’t work in browsers without native shadow dom */\n      :host {\n        font-family: var(--lumo-font-family);\n        font-size: var(--lumo-font-size, var(--lumo-font-size-m));\n        line-height: var(--lumo-line-height-m);\n        -webkit-text-size-adjust: 100%;\n        -webkit-font-smoothing: antialiased;\n        -moz-osx-font-smoothing: grayscale;\n      }\n\n      small,\n      [theme~="font-size-s"] {\n        font-size: var(--lumo-font-size-s);\n        line-height: var(--lumo-line-height-s);\n      }\n\n      [theme~="font-size-xs"] {\n        font-size: var(--lumo-font-size-xs);\n        line-height: var(--lumo-line-height-xs);\n      }\n\n      h1,\n      h2,\n      h3,\n      h4,\n      h5,\n      h6 {\n        font-weight: 600;\n        line-height: var(--lumo-line-height-xs);\n        margin-top: 1.25em;\n      }\n\n      h1 {\n        font-size: var(--lumo-font-size-xxxl);\n        margin-bottom: 0.75em;\n      }\n\n      h2 {\n        font-size: var(--lumo-font-size-xxl);\n        margin-bottom: 0.5em;\n      }\n\n      h3 {\n        font-size: var(--lumo-font-size-xl);\n        margin-bottom: 0.5em;\n      }\n\n      h4 {\n        font-size: var(--lumo-font-size-l);\n        margin-bottom: 0.5em;\n      }\n\n      h5 {\n        font-size: var(--lumo-font-size-m);\n        margin-bottom: 0.25em;\n      }\n\n      h6 {\n        font-size: var(--lumo-font-size-xs);\n        margin-bottom: 0;\n        text-transform: uppercase;\n        letter-spacing: 0.03em;\n      }\n\n      p,\n      blockquote {\n        margin-top: 0.5em;\n        margin-bottom: 0.75em;\n      }\n\n      a {\n        text-decoration: none;\n      }\n\n      a:hover {\n        text-decoration: underline;\n      }\n\n      hr {\n        display: block;\n        align-self: stretch;\n        height: 1px;\n        border: 0;\n        padding: 0;\n        margin: var(--lumo-space-s) calc(var(--lumo-border-radius-m) / 2);\n        background-color: var(--lumo-contrast-10pct);\n      }\n\n      blockquote {\n        border-left: 2px solid var(--lumo-contrast-30pct);\n      }\n\n      b,\n      strong {\n        font-weight: 600;\n      }\n\n      /* RTL specific styles */\n\n      blockquote[dir="rtl"] {\n        border-left: none;\n        border-right: 2px solid var(--lumo-contrast-30pct);\n      }\n\n    </style>\n  </template>\n</dom-module>',document.head.appendChild(P.content);const Y=document.createElement("template");Y.innerHTML='<dom-module id="lumo-required-field">\n  <template>\n    <style>\n      [part="label"] {\n        align-self: flex-start;\n        color: var(--lumo-secondary-text-color);\n        font-weight: 500;\n        font-size: var(--lumo-font-size-s);\n        margin-left: calc(var(--lumo-border-radius-m) / 4);\n        transition: color 0.2s;\n        line-height: 1;\n        padding-bottom: 0.5em;\n        overflow: hidden;\n        white-space: nowrap;\n        text-overflow: ellipsis;\n        position: relative;\n        max-width: 100%;\n        box-sizing: border-box;\n      }\n\n      :host([has-label])::before {\n        margin-top: calc(var(--lumo-font-size-s) * 1.5);\n      }\n\n      :host([has-label]) {\n        padding-top: var(--lumo-space-m);\n      }\n\n      :host([required]) [part="label"] {\n        padding-right: 1em;\n      }\n\n      [part="label"]::after {\n        content: var(--lumo-required-field-indicator, "•");\n        transition: opacity 0.2s;\n        opacity: 0;\n        color: var(--lumo-primary-text-color);\n        position: absolute;\n        right: 0;\n        width: 1em;\n        text-align: center;\n      }\n\n      :host([required]:not([has-value])) [part="label"]::after {\n        opacity: 1;\n      }\n\n      :host([invalid]) [part="label"]::after {\n        color: var(--lumo-error-text-color);\n      }\n\n      [part="error-message"] {\n        margin-left: calc(var(--lumo-border-radius-m) / 4);\n        font-size: var(--lumo-font-size-xs);\n        line-height: var(--lumo-line-height-xs);\n        color: var(--lumo-error-text-color);\n        will-change: max-height;\n        transition: 0.4s max-height;\n        max-height: 5em;\n      }\n\n      /* Margin that doesn’t reserve space when there’s no error message */\n      [part="error-message"]:not(:empty)::before,\n      [part="error-message"]:not(:empty)::after {\n        content: "";\n        display: block;\n        height: 0.4em;\n      }\n\n      :host(:not([invalid])) [part="error-message"] {\n        max-height: 0;\n        overflow: hidden;\n      }\n\n      /* RTL specific styles */\n\n      :host([dir="rtl"]) [part="label"] {\n        margin-left: 0;\n        margin-right: calc(var(--lumo-border-radius-m) / 4);\n      }\n\n      :host([required][dir="rtl"]) [part="label"] {\n        padding-left: 1em;\n        padding-right: 0;\n      }\n\n      :host([dir="rtl"]) [part="label"]::after {\n        right: auto;\n        left: 0;\n      }\n\n      :host([dir="rtl"]) [part="error-message"] {\n        margin-left: 0;\n        margin-right: calc(var(--lumo-border-radius-m) / 4);\n      }\n\n    </style>\n  </template>\n</dom-module>',document.head.appendChild(Y.content);const j=document.createElement("template");j.innerHTML='<custom-style>\n  <style>\n    @font-face {\n      font-family: \'lumo-icons\';\n      src: url(data:application/font-woff;charset=utf-8;base64,d09GRgABAAAAABEcAAsAAAAAIiwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADsAAABUIIslek9TLzIAAAFEAAAAQwAAAFZAIUuKY21hcAAAAYgAAAD4AAADrsCU8d5nbHlmAAACgAAAC2MAABd4h9To2WhlYWQAAA3kAAAAMAAAADZa/6SsaGhlYQAADhQAAAAdAAAAJAbpA35obXR4AAAONAAAABAAAACspBAAAGxvY2EAAA5EAAAAWAAAAFh55IAsbWF4cAAADpwAAAAfAAAAIAFKAXBuYW1lAAAOvAAAATEAAAIuUUJZCHBvc3QAAA/wAAABKwAAAelm8SzVeJxjYGRgYOBiMGCwY2BycfMJYeDLSSzJY5BiYGGAAJA8MpsxJzM9kYEDxgPKsYBpDiBmg4gCACY7BUgAeJxjYGS+yDiBgZWBgamKaQ8DA0MPhGZ8wGDIyAQUZWBlZsAKAtJcUxgcXjG+0mIO+p/FEMUcxDANKMwIkgMABn8MLQB4nO3SWW6DMABF0UtwCEnIPM/zhLK8LqhfXRybSP14XUYtHV9hGYQwQBNIo3cUIPkhQeM7rib1ekqnXg981XuC1qvy84lzojleh3puxL0hPjGjRU473teloEefAUNGjJkwZcacBUtWrNmwZceeA0dOnLlw5cadB09elPGhGf+j0NTI/65KfXerT6JhqKnpRKtgOpuqaTrtKjPUlqHmhto21I7pL6i6hlqY3q7qGWrfUAeGOjTUkaGODXViqFNDnRnq3FAXhro01JWhrg11Y6hbQ90Z6t5QD4Z6NNSToZ4N9WKoV0O9GerdUB+G+jTUl6GWRvkL24BkEXictVh9bFvVFb/nxvbz+7Rf/N6zHcd2bCfP+Wgc1Z9N0jpNnEL6kbRVS6HA2hQYGh9TGR1CbCqa2rXrWOkQE/sHNJgmtZvoVNZqE1B1DNHxzTQxCehUTYiJTQyENui0qSLezr3PduyQfgmRWOfde8+9551z7rnn/O4jLoJ/bRP0UaKQMLFJjpBAvphLZC3Dk0ok7WBzR2/upJs7Ryw/nfFbln/uuN/apCvwrKLrSvUqRufbm5pn0fs0w4gYxnGVP6qHnO4bWiDQGQgwtS6lm3lB3QoX1M2vwEmuzirF39y+Es2+DJ8d1pkyqBIqoze3D1+Zz4DrFoazxI8dWwMrDlZ2DMqQAR9AROsJU+2cmlTPazTco52F1xTa2a2+K8vvq92dVHmtLoPeQX/AZPRYGthDYOeZjBjKoFsVGulR3lWU95WeCK44qHU7MhWUGUKZDT3oKUcG2GWuh+EDDfUYA/jhAhl0TOsJNYSEu7mQmi3UzfXwZKA4BsVsHLXQYGgRW95uEtpJ1Vfn9XiLriRBlFEqxsDjA09yCNUoQxxwd7KWSTt2y3GTKiflqHRSoWZc3m11Wa/fJdFgXD4sSYfleJBKd8GMz7J8dZn/cGRCcKGDnA2Ge3fKzcvlnTDNthGWLXzX/WaXtUAmRgeLlHSr30r0G9UTXMb0AtmwzOoy73fkSlHZkduw/TYuU9cAD4YutPoxTTsA3797wVr4Z/1NC5zARHr4vtxJjxIfiZMhMkbWk+14BnJZKwqGZwDfswLyxWDSg11rFLJF7Nopxjd1h1/QOT+oezgfu3Yq+Hk+duf5x+40o1GTkaIgikK/IEnC6aYxCUBaZJSN4XTYFjU/YMNIKqJwhDGOCCI8FDXnXmXjtGhGJyShqjAOnBOkW2JG9S7GgYeMWAU5JzhnWmBOaOM+CKEPoqSfFDC2Unq+DLlUgUVUFFLZGJg6jtlojsdsa8kPObPuJdi5dnBdBsLJMGTWDa4t2JvtwuPo9s+Y86suv/W33QG1rAaOAUV+vx4K6f2D04PVKlC7WLSrZzAi45ZV6lIC7WoXqmRyvUqoVwrzUoVsIjeTXWQv+RH5GTlBXiB/In8ln0IbBCAFOajAJrgZYyOHWqOfUe/aHjI12R6OQo1jCgt215l+4f6XPb+0MNou0V+43n2F77tSfRb24d7zitgnKmvYHs69zugaPvBwv6ioXkb2LdL65Atw51uLkXlu1bhMMRcXSPcYoqKIRlh34lQP8/5JbuUFye4vxD6/6MxFF11C0uVLr9Ulgw44tS3pMViNLUExbycFgLIct+QDMibRimx1ydUz8FXZiuOIDBOMVX2nUZc+huNE5XUJ81uiJoiabwqaVF0uacKbau/pl4R2VW0XXlJra6boVrYG646TF5NYzwy4vjENVrDlcNpZPl8DH6XX8XWCx0mvWVZY6KFLrvsY66/zPict5FnxaNUR/juvZCM3TvD60E2W1tZizbXTPDuabcm0nbbzpWKpmA1ayBQ8giedLUM+A0kNjBjQjmuYz7YrgIXYvmF63ZLBwSXrpn9Tb9wwdd/U1H0PMQK3XcO8ul3WT7PyPPdpy0TemKxNRcJNauiXJnnUDpUppQWs4SnUIy0EESGYqJYQLGHxzaGWwVIaS6Y7mQFM8ZjYDQ3axjf61SWjU33JwOZA1pwaG1L9mzf71aHRdX1JHw6Fp0aXhNwbqyeGNg4NbdzGCBxoz4ZXjy4Nu69Zr6sDY6vMrLU5nA1P8JkbdWXJ6ERfMryvNh1JfQ9+T4dIhGvK9w3dxjBBzatsQ/MlOHVIDnYpDz6odAXlQ01t2Pa5Iafd8MMpxAeDKP0C6CjgVLT5osB6icUx01lWjXxzT/GyRF2welEM5Z/7jG3VjQ1SrNn5IbyzOG5dobB3/QHxyZvsXcoz8IoEwS7plCg+zxHQk424q9BfEpkESJbFHQusDBSWFkuBkoPO0kLKwRVYjxGXlHTcTDQMJ/H6TX9afkO7mnraTO1feTnZAXLu4cp7HAXMmNG1yeFk9TgS/NHhZR/4QoBTr/ZB+6hCgyl15Nq1UbN6nE1/ZnP1U2cizCBpvs8cJQZJ4LkYx5N/yZPAUZNQQ0V4f3BQllWrK3YRzl30dOT6RVn2upNur6woSa8CqpdT/aKnBM4o3jNur9d9xqtUT6veBEt9Ca9at+ERzEEhUkR8sa5mQ4aVvJoVeEA8zI4ei5mULXFGyU7z/6TAeYLVcpzSWZY8PYYF5yrTV60sT0+XV141vX++Wf16V2bFeGVPZXxFpkvyeKTWLlzfW0mnKxsY6Y3294/0998SCfX1blm5pbcvFGlq/r07MRAMhYIDiW5JFKWW3vdrEpCsZSJG+om7Zu/PSScZJhNkLbmW5Wsr12pWqW5zKtlwRS4bFOxUw17mCzy6lskCDl1WYOGWDYrADrMA7BDDweWWNd5koiJnR1dz+ytLP2q0SqPB1lnK2ccB7RYe4FSoPks3iB3t4txTSHctb2sy1ivk0pvHuCNm6w1f6wxv3+OCgN78LqdQnUVh7R0oTAp0zOf2rbW770Vu5C2dIyGdTnHo8zSji7dppj0USoVCz+lhRMTh53Teq9VbGfbjuSbAooSdXayY4PYHg374C6f7gl1B/DXuJ4/QXxOBdJFJspFsI3egpoWUUCjlTIFnNYNl+ZyZKmBeYKGHkD1QyDlhaKbKwKcIJqJ4TLJ2OmdY/JWXae4DdGBw8HZ7eXcgFF2zr2SoalDry5iKqoa0Puhe3hPQ2s3elTYM+MI+n3rK0KgL7/La3GeMLt6m7u912vGnvtORiIa0qBmhqVi+XW9XNBmqb8eVgKzIHfGI5bNoG7X0UCzeISmqIcO/nY8FH7U8avX9fx/ST+hx0sezPw9Qy8Mum3GWf2N4Uy/yIYGVBXbJHWIZp7dfTcptdMTr9Qmq7DaiK/ukqCL4kt4RUfS5XPnMtmT22/mQFqF7emSqtrlu8SVElxDRJrZODkpuwe0VfTfjdEp1f7A7v+fozNBXUJ/6WTuK2TtFlpFVZAZ3LcFvUi1Z2p2YT+EMAkGJVStOzLTAPg4IqWIAlzRSjOBkl2zxj3TKycpzT/MnvX3uaSMWM+gU0rkXjohhefVRMaps3/kLMSKv23lT23uxQrkQjyOJleMDsdhAnD6ZGElWZ5MjCXzCE/hkWX+WF4knzGhVOyK2eQZekV3eyo0zL8kuYWCnDCvjjhAkcTPOBDXVdoav3HVcFnQjLvtV9S2p0zA6JegPwMQxt+yFb3ll9zGlq/5dRKb3cEyQYoaNYpharJ7xCB7AWxsLY3jjZXY0XsZj0Wjwc9I6PP/dKABnCZaqHpaZEACxk4ZeLZSKNgZABl+lYQX1sJQOSX3n6r410evcoud5JeAGUXVP9H1tZOKejTq4Ono0z0erro1FrnOpohva1d/hTdtVsQdKN5W9RlT3NjD0nznyKNTgKAMfWNWcyodV0IGLPIHOF0o4JyqufaK4z6WIIzuGh3d8c8cwQg8ER+OVxyrjdm8vNuhts4LoOihGxIMuUdgzwiYN7xhh1+oZnJNuTG7gQZvu4XWZ9GAZZjGEubwePqYhtKDTH+9VQkl17/iGybsnJ+8+sKtyPrcll9ty65Zsdst/9iqpEKh7M5VdBxh3csOdNc6tW3I1uyM1PzOXegSOrLFsFNI2O27M+TF2ApnN9MUv5ud6LjxIvEQnHRzxIu4IsA9MLFkJn2tcZoZ7ON7dXe7ujrc8HrusPKamlqXwd77lQUuLpilau4PUMapueBb7irU4RoUXEYXuVuIGlRGmOp+2lNkaRPVziOqmlaZvaqG4dFgSj0jxEJWrv12IUWntmw+rfQarRE0Aph4ocI6nlUlGqs+u3/+T/ethW62PpHp2eHbZstnh/wOO95yDAHicY2BkYGAA4pmJ6QHx/DZfGbiZXwBFGGpUNzQi6P+vmacy3QJyORiYQKIANoULVXicY2BkYGAO+p8FJF8wAAHzVAZGBlSgDQBW9gNvAAAAeJxjYGBgYH4xNDAAzwQmjwAAAAAATgCaAOgBCgEsAU4BcAGaAcQB7gIaApwC6ASaBLwE1gTyBQ4FKgV6BdAF/gZEBmYGtgcYB5AIGAhSCGoI/glGCb4J2goECjwKggq4CvALUAuWC7x4nGNgZGBg0GZMYRBlAAEmIOYCQgaG/2A+AwAYlAG8AHicbZE9TsMwGIbf9A/RSggEYmHxAgtq+jN2ZGj3Dt3T1GlTOXHkuBW9AyfgEByCgTNwCA7BW/NJlVBtyd/jx+8XKwmAa3whwnFE6Ib1OBq44O6Pm6Qb4Rb5QbiNHh6FO/RD4S6eMRHu4RaaT4halzR3eBVu4Apvwk36d+EW+UO4jXt8Cnfov4W7WOBHuIen6MXsCtvPU1vWc73emcSdxIkW2tW5LdUoHp7kTJfaJV6v1PKg6v167H2mMmcLNbWl18ZYVTm71amPN95Xk8EgEx+ntoDBDgUs+siRspaoMef7rukNEriziXNuwS7Hmoe9wggxv+e55IzJMqQTeNYV00scuNbY8+YxrUfGfcaMZb/CNPQe04bT0lThbEuT0sfYhK6K/23Amf3Lx+H24hcj4GScAAAAeJxtjtlugzAQRbkJUEJIuu/7vqR8lGNPAcWx0YAb5e/LklR96EgenSufGY038PqKvf9rhgGG8BEgxA4ijBBjjAQTTLGLPezjAIc4wjFOcIoznOMCl7jCNW5wizvc4wGPeMIzXvCKN7zjAzN8eonQRWZSSaYmjvug6ase98hFltexMJmmVNmV2WBvdNgZUc+ujAWzXW3UDnu1w43asStHc8GpzAXX/py0jqTQZJTgkcxJLpaCF0lD32xNt+43tAsn29Dft02uDKS2cjGUNgsk26qK2lFthYoU27INPqmiDqg5goe0pqR5qSoqMdek/CUZFywL46rEsiImleqiqoMyt4baXlu/1GLdNFf5zbcNmdr1YUWCZe47o+zUmb/DoStbw3cVsef9ALjjiPQA) format(\'woff\');\n      font-weight: normal;\n      font-style: normal;\n    }\n\n    html {\n      --lumo-icons-align-center: "\\ea01";\n      --lumo-icons-align-left: "\\ea02";\n      --lumo-icons-align-right: "\\ea03";\n      --lumo-icons-angle-down: "\\ea04";\n      --lumo-icons-angle-left: "\\ea05";\n      --lumo-icons-angle-right: "\\ea06";\n      --lumo-icons-angle-up: "\\ea07";\n      --lumo-icons-arrow-down: "\\ea08";\n      --lumo-icons-arrow-left: "\\ea09";\n      --lumo-icons-arrow-right: "\\ea0a";\n      --lumo-icons-arrow-up: "\\ea0b";\n      --lumo-icons-bar-chart: "\\ea0c";\n      --lumo-icons-bell: "\\ea0d";\n      --lumo-icons-calendar: "\\ea0e";\n      --lumo-icons-checkmark: "\\ea0f";\n      --lumo-icons-chevron-down: "\\ea10";\n      --lumo-icons-chevron-left: "\\ea11";\n      --lumo-icons-chevron-right: "\\ea12";\n      --lumo-icons-chevron-up: "\\ea13";\n      --lumo-icons-clock: "\\ea14";\n      --lumo-icons-cog: "\\ea15";\n      --lumo-icons-cross: "\\ea16";\n      --lumo-icons-download: "\\ea17";\n      --lumo-icons-dropdown: "\\ea18";\n      --lumo-icons-edit: "\\ea19";\n      --lumo-icons-error: "\\ea1a";\n      --lumo-icons-eye: "\\ea1b";\n      --lumo-icons-eye-disabled: "\\ea1c";\n      --lumo-icons-menu: "\\ea1d";\n      --lumo-icons-minus: "\\ea1e";\n      --lumo-icons-ordered-list: "\\ea1f";\n      --lumo-icons-phone: "\\ea20";\n      --lumo-icons-photo: "\\ea21";\n      --lumo-icons-play: "\\ea22";\n      --lumo-icons-plus: "\\ea23";\n      --lumo-icons-redo: "\\ea24";\n      --lumo-icons-reload: "\\ea25";\n      --lumo-icons-search: "\\ea26";\n      --lumo-icons-undo: "\\ea27";\n      --lumo-icons-unordered-list: "\\ea28";\n      --lumo-icons-upload: "\\ea29";\n      --lumo-icons-user: "\\ea2a";\n    }\n  </style>\n</custom-style>',document.head.appendChild(j.content);const L=document.createElement("template");L.innerHTML='<dom-module id="lumo-field-button">\n  <template>\n    <style>\n      [part$="button"] {\n        flex: none;\n        width: 1em;\n        height: 1em;\n        line-height: 1;\n        font-size: var(--lumo-icon-size-m);\n        text-align: center;\n        color: var(--lumo-contrast-60pct);\n        transition: 0.2s color;\n        cursor: var(--lumo-clickable-cursor);\n      }\n\n      :host(:not([readonly])) [part$="button"]:hover {\n        color: var(--lumo-contrast-90pct);\n      }\n\n      :host([disabled]) [part$="button"],\n      :host([readonly]) [part$="button"] {\n        color: var(--lumo-contrast-20pct);\n      }\n\n      [part$="button"]::before {\n        font-family: "lumo-icons";\n        display: block;\n      }\n    </style>\n  </template>\n</dom-module>',document.head.appendChild(L.content);const U=d`<dom-module id="lumo-text-field" theme-for="vaadin-text-field">
  <template>
    <style include="lumo-required-field lumo-field-button">
      :host {
        --lumo-text-field-size: var(--lumo-size-m);
        color: var(--lumo-body-text-color);
        font-size: var(--lumo-font-size-m);
        font-family: var(--lumo-font-family);
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        -webkit-tap-highlight-color: transparent;
        padding: var(--lumo-space-xs) 0;
      }

      :host::before {
        height: var(--lumo-text-field-size);
        box-sizing: border-box;
        display: inline-flex;
        align-items: center;
      }

      :host([focused]:not([readonly])) [part="label"] {
        color: var(--lumo-primary-text-color);
      }

      :host([has-helper]) [part="helper-text"]::before {
        content: "";
        display: block;
        height: 0.4em;
      }

      [part="helper-text"],
      [part="helper-text"] ::slotted(*) {
        display: block;
        color: var(--lumo-secondary-text-color);
        font-size: var(--lumo-font-size-xs);
        line-height: var(--lumo-line-height-xs);
        margin-left: calc(var(--lumo-border-radius-m) / 4);
        transition: color 0.2s;
      }

      [part="value"],
      [part="input-field"] ::slotted(input),
      [part="input-field"] ::slotted(textarea),
      /* Slotted by vaadin-select-text-field */
      [part="input-field"] ::slotted([part="value"]) {
        cursor: inherit;
        min-height: var(--lumo-text-field-size);
        padding: 0 0.25em;
        --_lumo-text-field-overflow-mask-image: linear-gradient(to left, transparent, #000 1.25em);
        -webkit-mask-image: var(--_lumo-text-field-overflow-mask-image);
      }

      [part="value"]:focus,
      :host([focused]) [part="input-field"] ::slotted(input),
      :host([focused]) [part="input-field"] ::slotted(textarea) {
        -webkit-mask-image: none;
        mask-image: none;
      }

      /*
        TODO: CSS custom property in \`mask-image\` causes crash in Edge
        see https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/15415089/
      */
      @-moz-document url-prefix() {
        [part="value"],
        [part="input-field"] ::slotted(input),
        [part="input-field"] ::slotted(textarea),
        [part="input-field"] ::slotted([part="value"]) {
          mask-image: var(--_lumo-text-field-overflow-mask-image);
        }
      }

      [part="value"]::-webkit-input-placeholder {
        color: inherit;
        transition: opacity 0.175s 0.05s;
        opacity: 0.5;
      }

      [part="value"]:-ms-input-placeholder {
        color: inherit;
        opacity: 0.5;
      }

      [part="value"]::-moz-placeholder {
        color: inherit;
        transition: opacity 0.175s 0.05s;
        opacity: 0.5;
      }

      [part="value"]::placeholder {
        color: inherit;
        transition: opacity 0.175s 0.1s;
        opacity: 0.5;
      }

      [part="input-field"] {
        border-radius: var(--lumo-border-radius);
        background-color: var(--lumo-contrast-10pct);
        padding: 0 calc(0.375em + var(--lumo-border-radius) / 4 - 1px);
        font-weight: 500;
        line-height: 1;
        position: relative;
        cursor: text;
        box-sizing: border-box;
      }

      /* Used for hover and activation effects */
      [part="input-field"]::after {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        border-radius: inherit;
        pointer-events: none;
        background-color: var(--lumo-contrast-50pct);
        opacity: 0;
        transition: transform 0.15s, opacity 0.2s;
        transform-origin: 100% 0;
      }

      /* Hover */

      :host(:hover:not([readonly]):not([focused])) [part="label"],
      :host(:hover:not([readonly])) [part="helper-text"],
      :host(:hover:not([readonly])) [part="helper-text"] ::slotted(*) {
        color: var(--lumo-body-text-color);
      }

      :host(:hover:not([readonly]):not([focused])) [part="input-field"]::after {
        opacity: 0.1;
      }

      /* Touch device adjustment */
      @media (pointer: coarse) {
        :host(:hover:not([readonly]):not([focused])) [part="label"] {
          color: var(--lumo-secondary-text-color);
        }

        :host(:hover:not([readonly]):not([focused])) [part="input-field"]::after {
          opacity: 0;
        }

        :host(:active:not([readonly]):not([focused])) [part="input-field"]::after {
          opacity: 0.2;
        }
      }

      /* Trigger when not focusing using the keyboard */
      :host([focused]:not([focus-ring]):not([readonly])) [part="input-field"]::after {
        transform: scaleX(0);
        transition-duration: 0.15s, 1s;
      }

      /* Focus-ring */

      :host([focus-ring]) [part="input-field"] {
        box-shadow: 0 0 0 2px var(--lumo-primary-color-50pct);
      }

      /* Read-only and disabled */
      :host([readonly]) [part="value"]::-webkit-input-placeholder,
      :host([disabled]) [part="value"]::-webkit-input-placeholder {
        opacity: 0;
      }

      :host([readonly]) [part="value"]:-ms-input-placeholder,
      :host([disabled]) [part="value"]:-ms-input-placeholder {
        opacity: 0;
      }

      :host([readonly]) [part="value"]::-moz-placeholder,
      :host([disabled]) [part="value"]::-moz-placeholder {
        opacity: 0;
      }

      :host([readonly]) [part="value"]::placeholder,
      :host([disabled]) [part="value"]::placeholder {
        opacity: 0;
      }

      /* Read-only */

      :host([readonly]) [part="input-field"] {
        color: var(--lumo-secondary-text-color);
        background-color: transparent;
        cursor: default;
      }

      :host([readonly]) [part="input-field"]::after {
        background-color: transparent;
        opacity: 1;
        border: 1px dashed var(--lumo-contrast-30pct);
      }

      /* Disabled style */

      :host([disabled]) {
        pointer-events: none;
      }

      :host([disabled]) [part="input-field"] {
        background-color: var(--lumo-contrast-5pct);
      }

      :host([disabled]) [part="label"],
      :host([disabled]) [part="helper-text"],
      :host([disabled]) [part="value"],
      :host([disabled]) [part="input-field"] ::slotted(*) {
        color: var(--lumo-disabled-text-color);
        -webkit-text-fill-color: var(--lumo-disabled-text-color);
      }

      /* Invalid style */

      :host([invalid]) [part="input-field"] {
        background-color: var(--lumo-error-color-10pct);
      }

      :host([invalid]) [part="input-field"]::after {
        background-color: var(--lumo-error-color-50pct);
      }

      :host([invalid][focus-ring]) [part="input-field"] {
        box-shadow: 0 0 0 2px var(--lumo-error-color-50pct);
      }

      :host([input-prevented]) [part="input-field"] {
        color: var(--lumo-error-text-color);
      }

      /* Small theme */

      :host([theme~="small"]) {
        font-size: var(--lumo-font-size-s);
        --lumo-text-field-size: var(--lumo-size-s);
      }

      :host([theme~="small"][has-label]) [part="label"] {
        font-size: var(--lumo-font-size-xs);
      }

      :host([theme~="small"][has-label]) [part="error-message"] {
        font-size: var(--lumo-font-size-xxs);
      }

      /* Text align */

      :host([theme~="align-left"]) [part="value"] {
        text-align: left;
        --_lumo-text-field-overflow-mask-image: none;
      }

      :host([theme~="align-center"]) [part="value"] {
        text-align: center;
        --_lumo-text-field-overflow-mask-image: none;
      }

      :host([theme~="align-right"]) [part="value"] {
        text-align: right;
        --_lumo-text-field-overflow-mask-image: none;
      }

      @-moz-document url-prefix() {
        /* Firefox is smart enough to align overflowing text to right */
        :host([theme~="align-right"]) [part="value"] {
          --_lumo-text-field-overflow-mask-image: linear-gradient(to right, transparent 0.25em, #000 1.5em);
        }
      }

      @-moz-document url-prefix() {
        /* Firefox is smart enough to align overflowing text to right */
        :host([theme~="align-left"]) [part="value"] {
          --_lumo-text-field-overflow-mask-image: linear-gradient(to left, transparent 0.25em, #000 1.5em);
        }
      }
      /* helper-text position */

      :host([has-helper][theme~="helper-above-field"]) [part="helper-text"]::before {
        display: none;
      }

      :host([has-helper][theme~="helper-above-field"]) [part="helper-text"]::after {
        content: "";
        display: block;
        height: 0.4em;
      }

      :host([has-helper][theme~="helper-above-field"]) [part="label"] {
        order: 0;
        padding-bottom: 0.4em;
      }

      :host([has-helper][theme~="helper-above-field"]) [part="helper-text"] {
        order: 1;
      }

      :host([has-helper][theme~="helper-above-field"]) [part="input-field"] {
        order: 2;
      }

      :host([has-helper][theme~="helper-above-field"]) [part="error-message"] {
        order: 3;
      }

      /* Slotted content */

      [part="input-field"] ::slotted(:not([part]):not(iron-icon):not(input):not(textarea)) {
        color: var(--lumo-secondary-text-color);
        font-weight: 400;
      }

      /* Slotted icons */

      [part="input-field"] ::slotted(iron-icon) {
        color: var(--lumo-contrast-60pct);
        width: var(--lumo-icon-size-m);
        height: var(--lumo-icon-size-m);
      }

      /* Vaadin icons are based on a 16x16 grid (unlike Lumo and Material icons with 24x24), so they look too big by default */
      [part="input-field"] ::slotted(iron-icon[icon^="vaadin:"]) {
        padding: 0.25em;
        box-sizing: border-box !important;
      }

      [part="clear-button"]::before {
        content: var(--lumo-icons-cross);
      }

      /* RTL specific styles */

      :host([dir="rtl"]) [part="input-field"]::after {
        transform-origin: 0% 0;
      }

      :host([dir="rtl"]) [part="value"],
      :host([dir="rtl"]) [part="input-field"] ::slotted(input),
      :host([dir="rtl"]) [part="input-field"] ::slotted(textarea) {
        --_lumo-text-field-overflow-mask-image: linear-gradient(to right, transparent, #000 1.25em);
      }

      :host([dir="rtl"]) [part="value"]:focus,
      :host([focused][dir="rtl"]) [part="input-field"] ::slotted(input),
      :host([focused][dir="rtl"]) [part="input-field"] ::slotted(textarea) {
        -webkit-mask-image: none;
        mask-image: none;
      }

      @-moz-document url-prefix() {
        :host([dir="rtl"]) [part="value"],
        :host([dir="rtl"]) [part="input-field"] ::slotted(input),
        :host([dir="rtl"]) [part="input-field"] ::slotted(textarea),
        :host([dir="rtl"]) [part="input-field"] ::slotted([part="value"]) {
          mask-image: var(--_lumo-text-field-overflow-mask-image);
        }
      }

      :host([theme~="align-left"][dir="rtl"]) [part="value"] {
        --_lumo-text-field-overflow-mask-image: none;
      }

      :host([theme~="align-center"][dir="rtl"]) [part="value"] {
        --_lumo-text-field-overflow-mask-image: none;
      }

      :host([theme~="align-right"][dir="rtl"]) [part="value"] {
        --_lumo-text-field-overflow-mask-image: none;
      }

      @-moz-document url-prefix() {
        /* Firefox is smart enough to align overflowing text to right */
        :host([theme~="align-right"][dir="rtl"]) [part="value"] {
          --_lumo-text-field-overflow-mask-image: linear-gradient(to right, transparent 0.25em, #000 1.5em);
        }
      }

      @-moz-document url-prefix() {
        /* Firefox is smart enough to align overflowing text to right */
        :host([theme~="align-left"][dir="rtl"]) [part="value"] {
          --_lumo-text-field-overflow-mask-image: linear-gradient(to left, transparent 0.25em, #000 1.5em);
        }
      }
    </style>
  </template>
</dom-module>`;document.head.appendChild(U.content);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
const Z=d`<dom-module id="vaadin-text-field-shared-styles">
  <template>
    <style>
      :host {
        display: inline-flex;
        outline: none;
      }

      :host::before {
        content: "\\2003";
        width: 0;
        display: inline-block;
        /* Size and position this element on the same vertical position as the input-field element
           to make vertical align for the host element work as expected */
      }

      :host([hidden]) {
        display: none !important;
      }

      .vaadin-text-field-container,
      .vaadin-text-area-container {
        display: flex;
        flex-direction: column;
        min-width: 100%;
        max-width: 100%;
        width: var(--vaadin-text-field-default-width, 12em);
      }

      [part="label"]:empty {
        display: none;
      }

      [part="input-field"] {
        display: flex;
        align-items: center;
        flex: auto;
      }

      .vaadin-text-field-container [part="input-field"] {
        flex-grow: 0;
      }

      /* Reset the native input styles */
      [part="value"],
      [part="input-field"] ::slotted(input),
      [part="input-field"] ::slotted(textarea) {
        -webkit-appearance: none;
        -moz-appearance: none;
        outline: none;
        margin: 0;
        padding: 0;
        border: 0;
        border-radius: 0;
        min-width: 0;
        font: inherit;
        font-size: 1em;
        line-height: normal;
        color: inherit;
        background-color: transparent;
        /* Disable default invalid style in Firefox */
        box-shadow: none;
      }

      [part="input-field"] ::slotted(*) {
        flex: none;
      }

      [part="value"],
      [part="input-field"] ::slotted(input),
      [part="input-field"] ::slotted(textarea),
      /* Slotted by vaadin-select-text-field */
      [part="input-field"] ::slotted([part="value"]) {
        flex: auto;
        white-space: nowrap;
        overflow: hidden;
        width: 100%;
        height: 100%;
      }

      [part="input-field"] ::slotted(textarea) {
        resize: none;
      }

      [part="value"]::-ms-clear,
      [part="input-field"] ::slotted(input)::-ms-clear {
        display: none;
      }

      [part="clear-button"] {
        cursor: default;
      }

      [part="clear-button"]::before {
        content: "✕";
      }
    </style>
  </template>
</dom-module>`;document.head.appendChild(Z.content);const W={default:["list","autofocus","pattern","autocapitalize","autocorrect","maxlength","minlength","name","placeholder","autocomplete","title","disabled","readonly","required"],accessible:["invalid"]},Q={DEFAULT:"default",ACCESSIBLE:"accessible"},V=t=>class extends t{static get properties(){return{autocomplete:{type:String},autocorrect:{type:String},autocapitalize:{type:String},autoselect:{type:Boolean,value:!1},clearButtonVisible:{type:Boolean,value:!1},errorMessage:{type:String,value:"",observer:"_errorMessageChanged"},i18n:{type:Object,value:()=>({clear:"Clear"})},label:{type:String,value:"",observer:"_labelChanged"},helperText:{type:String,value:"",observer:"_helperTextChanged"},maxlength:{type:Number},minlength:{type:Number},name:{type:String},placeholder:{type:String},readonly:{type:Boolean,reflectToAttribute:!0},required:{type:Boolean,reflectToAttribute:!0},value:{type:String,value:"",observer:"_valueChanged",notify:!0},invalid:{type:Boolean,reflectToAttribute:!0,notify:!0,value:!1},hasValue:{type:Boolean,reflectToAttribute:!0},preventInvalidInput:{type:Boolean},_enabledCharPattern:String,_labelId:String,_helperTextId:String,_errorId:String,_inputId:String,_hasSlottedHelper:Boolean}}static get observers(){return["_stateChanged(disabled, readonly, clearButtonVisible, hasValue)","_hostPropsChanged("+W.default.join(", ")+")","_hostAccessiblePropsChanged("+W.accessible.join(", ")+")","_getActiveErrorId(invalid, errorMessage, _errorId, helperText, _helperTextId, _hasSlottedHelper)","_getActiveLabelId(label, _labelId, _inputId)","__observeOffsetHeight(errorMessage, invalid, label, helperText)","__enabledCharPatternChanged(_enabledCharPattern)"]}get focusElement(){if(!this.shadowRoot)return;return this.querySelector(`${this._slottedTagName}[slot="${this._slottedTagName}"]`)||this.shadowRoot.querySelector('[part="value"]')}get inputElement(){return this.focusElement}get _slottedTagName(){return"input"}_createConstraintsObserver(){this._createMethodObserver("_constraintsChanged(required, minlength, maxlength, pattern)")}_onInput(t){if(this.__preventInput)return t.stopImmediatePropagation(),void(this.__preventInput=!1);if(this.preventInvalidInput){const t=this.inputElement;if(t.value.length>0&&!this.checkValidity())return t.value=this.value||"",this.setAttribute("input-prevented",""),void(this._inputDebouncer=a.debounce(this._inputDebouncer,c.after(200),()=>{this.removeAttribute("input-prevented")}))}t.__fromClearButton||(this.__userInput=!0),this.value=t.target.value,this.__userInput=!1}_stateChanged(t,e,i,s){!t&&!e&&i&&s?this.$.clearButton.removeAttribute("hidden"):this.$.clearButton.setAttribute("hidden",!0)}_onChange(t){if(this._valueClearing)return;const e=new CustomEvent("change",{detail:{sourceEvent:t},bubbles:t.bubbles,cancelable:t.cancelable});this.dispatchEvent(e)}_valueChanged(t,e){""===t&&void 0===e||(this.hasValue=""!==t&&null!=t,this.__userInput||(void 0!==t?this.inputElement.value=t:this.value=this.inputElement.value="",this.invalid&&this.validate()))}_labelChanged(t){this._setOrToggleAttribute("has-label",!!t,this)}_helperTextChanged(t){this._setOrToggleAttribute("has-helper",!!t,this)}_errorMessageChanged(t){this._setOrToggleAttribute("has-error-message",!!t,this)}_onHelperSlotChange(){const t=this.shadowRoot.querySelector('[name="helper"]').assignedNodes({flatten:!0});this._hasSlottedHelper=t.filter(t=>3!==t.nodeType).length,this._hasSlottedHelper?this.setAttribute("has-helper","slotted"):""!==this.helperText&&null!==this.helperText||this.removeAttribute("has-helper")}_onSlotChange(){const t=this.querySelector(`${this._slottedTagName}[slot="${this._slottedTagName}"]`);this.value&&(this.inputElement.value=this.value,this.validate()),t&&!this._slottedInput?(this._validateSlottedValue(t),this._addInputListeners(t),this._addIEListeners(t),this._slottedInput=t):!t&&this._slottedInput&&(this._removeInputListeners(this._slottedInput),this._removeIEListeners(this._slottedInput),this._slottedInput=void 0),Object.keys(Q).map(t=>Q[t]).forEach(t=>this._propagateHostAttributes(W[t].map(t=>this[t]),t))}_hostPropsChanged(...t){this._propagateHostAttributes(t,Q.DEFAULT)}_hostAccessiblePropsChanged(...t){this._propagateHostAttributes(t,Q.ACCESSIBLE)}_validateSlottedValue(t){t.value!==this.value&&(console.warn("Please define value on the vaadin-text-field component!"),t.value="")}_propagateHostAttributes(t,e){const i=this.inputElement;W[e].forEach(e===Q.ACCESSIBLE?(e,s)=>{this._setOrToggleAttribute(e,t[s],i),this._setOrToggleAttribute(`aria-${e}`,!!t[s]&&"true",i)}:(e,s)=>{this._setOrToggleAttribute(e,t[s],i)})}_setOrToggleAttribute(t,e,i){t&&i&&(e?i.setAttribute(t,"boolean"==typeof e?"":e):i.removeAttribute(t))}_constraintsChanged(t,e,i,s){this.invalid&&(t||e||i||s?this.validate():this.invalid=!1)}checkValidity(){return this.required||this.pattern||this.maxlength||this.minlength||this.__forceCheckValidity?this.inputElement.checkValidity():!this.invalid}_addInputListeners(t){t.addEventListener("input",this._boundOnInput),t.addEventListener("change",this._boundOnChange),t.addEventListener("blur",this._boundOnBlur),t.addEventListener("focus",this._boundOnFocus),t.addEventListener("paste",this._boundOnPaste),t.addEventListener("drop",this._boundOnDrop),t.addEventListener("beforeinput",this._boundOnBeforeInput)}_removeInputListeners(t){t.removeEventListener("input",this._boundOnInput),t.removeEventListener("change",this._boundOnChange),t.removeEventListener("blur",this._boundOnBlur),t.removeEventListener("focus",this._boundOnFocus),t.removeEventListener("paste",this._boundOnPaste),t.removeEventListener("drop",this._boundOnDrop),t.removeEventListener("beforeinput",this._boundOnBeforeInput)}ready(){super.ready(),this._createConstraintsObserver(),this._boundOnInput=this._onInput.bind(this),this._boundOnChange=this._onChange.bind(this),this._boundOnBlur=this._onBlur.bind(this),this._boundOnFocus=this._onFocus.bind(this),this._boundOnPaste=this._onPaste.bind(this),this._boundOnDrop=this._onDrop.bind(this),this._boundOnBeforeInput=this._onBeforeInput.bind(this);const t=this.shadowRoot.querySelector('[part="value"]');this._slottedInput=this.querySelector(`${this._slottedTagName}[slot="${this._slottedTagName}"]`),this._addInputListeners(t),this._addIEListeners(t),this._slottedInput&&(this._addIEListeners(this._slottedInput),this._addInputListeners(this._slottedInput)),this.shadowRoot.querySelector('[name="input"], [name="textarea"]').addEventListener("slotchange",this._onSlotChange.bind(this)),this._onHelperSlotChange(),this.shadowRoot.querySelector('[name="helper"]').addEventListener("slotchange",this._onHelperSlotChange.bind(this)),window.ShadyCSS&&window.ShadyCSS.nativeCss||this.updateStyles(),this.$.clearButton.addEventListener("mousedown",()=>this._valueClearing=!0),this.$.clearButton.addEventListener("mouseleave",()=>this._valueClearing=!1),this.$.clearButton.addEventListener("click",this._onClearButtonClick.bind(this)),this.addEventListener("keydown",this._onKeyDown.bind(this));var e=V._uniqueId=1+V._uniqueId||0;this._errorId=`${this.constructor.is}-error-${e}`,this._labelId=`${this.constructor.is}-label-${e}`,this._helperTextId=`${this.constructor.is}-helper-${e}`,this._inputId=`${this.constructor.is}-input-${e}`,this.shadowRoot.querySelector('[part="error-message"]').addEventListener("transitionend",()=>{this.__observeOffsetHeight()})}validate(){return!(this.invalid=!this.checkValidity())}clear(){this.value=""}_onBlur(){this.validate()}_onFocus(){this.autoselect&&(this.inputElement.select(),setTimeout(()=>{try{this.inputElement.setSelectionRange(0,9999)}catch(t){}}))}_onClearButtonClick(t){t.preventDefault(),this.inputElement.focus(),this.clear(),this._valueClearing=!1,navigator.userAgent.match(/Trident/)&&(this.__preventInput=!1);const e=new Event("input",{bubbles:!0,composed:!0});e.__fromClearButton=!0;const i=new Event("change",{bubbles:!this._slottedInput});i.__fromClearButton=!0,this.inputElement.dispatchEvent(e),this.inputElement.dispatchEvent(i)}_onKeyDown(t){if(27===t.keyCode&&this.clearButtonVisible){const t=!!this.value;this.clear(),t&&this.inputElement.dispatchEvent(new Event("change",{bubbles:!this._slottedInput}))}this._enabledCharPattern&&!this.__shouldAcceptKey(t)&&t.preventDefault()}__shouldAcceptKey(t){return t.metaKey||t.ctrlKey||!t.key||1!==t.key.length||this.__enabledCharRegExp.test(t.key)}_onPaste(t){if(this._enabledCharPattern){const e=(t.clipboardData||window.clipboardData).getData("text");this.__enabledTextRegExp.test(e)||t.preventDefault()}}_onDrop(t){if(this._enabledCharPattern){const e=t.dataTransfer.getData("text");this.__enabledTextRegExp.test(e)||t.preventDefault()}}_onBeforeInput(t){this._enabledCharPattern&&t.data&&!this.__enabledTextRegExp.test(t.data)&&t.preventDefault()}__enabledCharPatternChanged(t){this.__enabledCharRegExp=t&&new RegExp("^"+t+"$"),this.__enabledTextRegExp=t&&new RegExp("^"+t+"*$")}_addIEListeners(t){navigator.userAgent.match(/Trident/)&&(this._shouldPreventInput=()=>{this.__preventInput=!0,requestAnimationFrame(()=>{this.__preventInput=!1})},t.addEventListener("focusin",this._shouldPreventInput),t.addEventListener("focusout",this._shouldPreventInput),this._createPropertyObserver("placeholder",this._shouldPreventInput))}_removeIEListeners(t){navigator.userAgent.match(/Trident/)&&(t.removeEventListener("focusin",this._shouldPreventInput),t.removeEventListener("focusout",this._shouldPreventInput))}_getActiveErrorId(t,e,i,s,r,o){const n=[];(s||o)&&n.push(r),e&&t&&n.push(i),this._setOrToggleAttribute("aria-describedby",n.join(" "),this.focusElement)}_getActiveLabelId(t,e,i){let s=i;t&&(s=`${e} ${i}`),this.focusElement.setAttribute("aria-labelledby",s)}_getErrorMessageAriaHidden(t,e,i){return(!(e&&t?i:void 0)).toString()}_dispatchIronResizeEventIfNeeded(t,e){const i="__previous"+t;void 0!==this[i]&&this[i]!==e&&this.dispatchEvent(new CustomEvent("iron-resize",{bubbles:!0,composed:!0})),this[i]=e}__observeOffsetHeight(){this.__observeOffsetHeightDebouncer=a.debounce(this.__observeOffsetHeightDebouncer,l,()=>{this._dispatchIronResizeEventIfNeeded("Height",this.offsetHeight)})}attributeChangedCallback(t,e,i){if(super.attributeChangedCallback(t,e,i),window.ShadyCSS&&window.ShadyCSS.nativeCss||!/^(focused|focus-ring|invalid|disabled|placeholder|has-value)$/.test(t)||this.updateStyles(),/^((?!chrome|android).)*safari/i.test(navigator.userAgent)&&this.root){const t="-webkit-backface-visibility";this.root.querySelectorAll("*").forEach(e=>{e.style[t]="visible",e.style[t]=""})}}get __data(){return this.__dataValue||{}}set __data(t){this.__dataValue=t}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/;let J=!1;window.addEventListener("keydown",()=>{J=!0},{capture:!0}),window.addEventListener("mousedown",()=>{J=!1},{capture:!0});const X=t=>class extends((t=>class extends t{static get properties(){var t={tabindex:{type:Number,value:0,reflectToAttribute:!0,observer:"_tabindexChanged"}};return window.ShadyDOM&&(t.tabIndex=t.tabindex),t}})(t)){static get properties(){return{autofocus:{type:Boolean},_previousTabIndex:{type:Number},disabled:{type:Boolean,observer:"_disabledChanged",reflectToAttribute:!0},_isShiftTabbing:{type:Boolean}}}ready(){this.addEventListener("focusin",t=>{t.composedPath()[0]===this?this.contains(t.relatedTarget)||this._focus():-1===t.composedPath().indexOf(this.focusElement)||this.disabled||this._setFocused(!0)}),this.addEventListener("focusout",()=>this._setFocused(!1)),super.ready();const t=t=>{t.composed||t.target.dispatchEvent(new CustomEvent(t.type,{bubbles:!0,composed:!0,cancelable:!1}))};this.shadowRoot.addEventListener("focusin",t),this.shadowRoot.addEventListener("focusout",t),this.addEventListener("keydown",t=>{if(!t.defaultPrevented&&9===t.keyCode)if(t.shiftKey)this._isShiftTabbing=!0,HTMLElement.prototype.focus.apply(this),this._setFocused(!1),setTimeout(()=>this._isShiftTabbing=!1,0);else{const t=window.navigator.userAgent.match(/Firefox\/(\d\d\.\d)/);if(t&&parseFloat(t[1])>=63&&parseFloat(t[1])<66&&this.parentNode&&this.nextSibling){const t=document.createElement("input");t.style.position="absolute",t.style.opacity="0",t.tabIndex=this.tabIndex,this.parentNode.insertBefore(t,this.nextSibling),t.focus(),t.addEventListener("focusout",()=>this.parentNode.removeChild(t))}}}),this.autofocus&&!this.disabled&&window.requestAnimationFrame(()=>{this._focus(),this._setFocused(!0),this.setAttribute("focus-ring","")})}disconnectedCallback(){super.disconnectedCallback(),this.hasAttribute("focused")&&this._setFocused(!1)}_setFocused(t){t?this.setAttribute("focused",""):this.removeAttribute("focused"),t&&J?this.setAttribute("focus-ring",""):this.removeAttribute("focus-ring")}get focusElement(){return window.console.warn(`Please implement the 'focusElement' property in <${this.localName}>`),this}_focus(){this.focusElement&&!this._isShiftTabbing&&(this.focusElement.focus(),this._setFocused(!0))}focus(){this.focusElement&&!this.disabled&&(this.focusElement.focus(),this._setFocused(!0))}blur(){this.focusElement&&(this.focusElement.blur(),this._setFocused(!1))}_disabledChanged(t){this.focusElement.disabled=t,t?(this.blur(),this._previousTabIndex=this.tabindex,this.tabindex=-1,this.setAttribute("aria-disabled","true")):(void 0!==this._previousTabIndex&&(this.tabindex=this._previousTabIndex),this.removeAttribute("aria-disabled"))}_tabindexChanged(t){void 0!==t&&(this.focusElement.tabIndex=t),this.disabled&&this.tabindex&&(-1!==this.tabindex&&(this._previousTabIndex=this.tabindex),this.tabindex=t=void 0),window.ShadyDOM&&this.setProperties({tabIndex:t,tabindex:t})}click(){this.disabled||super.click()}},q=t=>class extends t{static get properties(){return{theme:{type:String,readOnly:!0}}}attributeChangedCallback(t,e,i){super.attributeChangedCallback(t,e,i),"theme"===t&&this._setTheme(i)}},K=t=>class extends(q(t)){static finalize(){super.finalize();const t=this.prototype._template,e=this.template&&this.template.parentElement&&this.template.parentElement.id===this.is,i=Object.getPrototypeOf(this.prototype)._template;i&&!e&&Array.from(i.content.querySelectorAll("style[include]")).forEach(e=>{this._includeStyle(e.getAttribute("include"),t)}),this._includeMatchingThemes(t)}static _includeMatchingThemes(t){const e=u.prototype.modules;let i=!1;const s=this.is+"-default-theme";Object.keys(e).sort((t,e)=>{const i=0===t.indexOf("vaadin-"),s=0===e.indexOf("vaadin-"),r=["lumo-","material-"],o=r.filter(e=>0===t.indexOf(e)).length>0,n=r.filter(t=>0===e.indexOf(t)).length>0;return i!==s?i?-1:1:o!==n?o?-1:1:0}).forEach(r=>{if(r!==s){const s=e[r].getAttribute("theme-for");s&&s.split(" ").forEach(e=>{new RegExp("^"+e.split("*").join(".*")+"$").test(this.is)&&(i=!0,this._includeStyle(r,t))})}}),!i&&e[s]&&this._includeStyle(s,t)}static _includeStyle(t,e){if(e&&!e.content.querySelector(`style[include="${t}"]`)){const i=document.createElement("style");i.setAttribute("include",t),e.content.appendChild(i)}}};let $;window.Vaadin||(window.Vaadin={}),window.Vaadin.registrations=window.Vaadin.registrations||[],window.Vaadin.developmentModeCallback=window.Vaadin.developmentModeCallback||{},window.Vaadin.developmentModeCallback["vaadin-usage-statistics"]=function(){};const tt=new Set,et=t=>class extends(B(t)){static finalize(){super.finalize();const{is:t}=this;t&&!tt.has(t)&&(window.Vaadin.registrations.push(this),tt.add(t),window.Vaadin.developmentModeCallback&&($=a.debounce($,p,()=>{window.Vaadin.developmentModeCallback["vaadin-usage-statistics"]()}),m($)))}constructor(){super(),null===document.doctype&&console.warn('Vaadin components require the "standards mode" declaration. Please add <!DOCTYPE html> to the HTML document.')}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/;class it extends(et(V(X(K(r))))){static get template(){return d`
    <style include="vaadin-text-field-shared-styles">
      /* polymer-cli linter breaks with empty line */
    </style>

    <div class="vaadin-text-field-container">

      <label part="label" on-click="focus" id="[[_labelId]]">[[label]]</label>

      <div part="input-field" id="[[_inputId]]">

        <slot name="prefix"></slot>

        <slot name="input">
          <input part="value">
        </slot>

        <div part="clear-button" id="clearButton" role="button" aria-label\$="[[i18n.clear]]"></div>
        <slot name="suffix"></slot>

      </div>

      <div part="helper-text" on-click="focus" id="[[_helperTextId]]">
        <slot name="helper">[[helperText]]</slot>
      </div>

      <div part="error-message" id="[[_errorId]]" aria-live="assertive" aria-hidden\$="[[_getErrorMessageAriaHidden(invalid, errorMessage, _errorId)]]">[[errorMessage]]</div>

    </div>
`}static get is(){return"vaadin-text-field"}static get version(){return"2.8.0"}static get properties(){return{list:{type:String},pattern:{type:String},title:{type:String}}}}customElements.define(it.is,it);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
class st extends r{static get template(){return d`
    <style>
      :host {
        display: inline-flex;
        max-width: 100%;
      }

      #filter {
        width: 100%;
        box-sizing: border-box;
      }
    </style>
    <slot name="filter">
      <vaadin-text-field id="filter" value="{{value}}"></vaadin-text-field>
    </slot>
`}static get is(){return"vaadin-grid-filter"}static get properties(){return{path:String,value:{type:String,notify:!0},_connected:Boolean}}connectedCallback(){super.connectedCallback(),this._connected=!0}static get observers(){return["_filterChanged(path, value, _connected)"]}ready(){super.ready();const t=this.firstElementChild;t&&"filter"!==t.getAttribute("slot")&&(console.warn('Make sure you have assigned slot="filter" to the child elements of <vaadin-grid-filter>'),t.setAttribute("slot","filter"))}_filterChanged(t,e,i){void 0!==t&&void 0!==e&&i&&(void 0===this._previousValue&&""===e||(this._previousValue=e,this._debouncerFilterChanged=a.debounce(this._debouncerFilterChanged,c.after(200),()=>{this.dispatchEvent(new CustomEvent("filter-changed",{bubbles:!0}))})))}focus(){this.$.filter.focus()}}customElements.define(st.is,st);
/**
@license
Copyright (c) 2018 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
class rt extends M{static get template(){return d`
    <template class="header" id="headerTemplate">
      <vaadin-grid-filter path="[[path]]" value="[[_filterValue]]">
        <vaadin-text-field theme="small" focus-target="" style="max-width: 100%;" slot="filter" value="{{_filterValue}}" label="[[_getHeader(header, path)]]"></vaadin-text-field>
      </vaadin-grid-filter>
    </template>
`}static get is(){return"vaadin-grid-filter-column"}static get properties(){return{path:String,header:String}}_prepareHeaderTemplate(){const t=this._prepareTemplatizer(this.$.headerTemplate);return t.templatizer.dataHost=this,t}_getHeader(t,e){return t||this._generateHeader(e)}}customElements.define(rt.is,rt);const ot=d`<dom-module id="lumo-checkbox" theme-for="vaadin-checkbox">
  <template>
    <style include="lumo-checkbox-style lumo-checkbox-effects">
      /* IE11 only */
      ::-ms-backdrop,
      [part="checkbox"] {
        line-height: 1;
      }
    </style>
  </template>
</dom-module><dom-module id="lumo-checkbox-style">
  <template>
    <style>
      :host {
        -webkit-tap-highlight-color: transparent;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        cursor: default;
        outline: none;
      }

      [part="label"]:not([empty]) {
        margin: 0.1875em 0.875em 0.1875em 0.375em;
      }

      [part="checkbox"] {
        width: calc(1em + 2px);
        height: calc(1em + 2px);
        margin: 0.1875em;
        position: relative;
        border-radius: var(--lumo-border-radius-s);
        background-color: var(--lumo-contrast-20pct);
        transition: transform 0.2s cubic-bezier(.12, .32, .54, 2), background-color 0.15s;
        pointer-events: none;
        line-height: 1.2;
      }

      :host([indeterminate]) [part="checkbox"],
      :host([checked]) [part="checkbox"] {
        background-color: var(--lumo-primary-color);
      }

      /* Needed to align the checkbox nicely on the baseline */
      [part="checkbox"]::before {
        content: "\\2003";
      }

      /* Checkmark */
      [part="checkbox"]::after {
        content: "";
        display: inline-block;
        width: 0;
        height: 0;
        border: 0 solid var(--lumo-primary-contrast-color);
        border-width: 0.1875em 0 0 0.1875em;
        box-sizing: border-box;
        transform-origin: 0 0;
        position: absolute;
        top: 0.8125em;
        left: 0.5em;
        transform: scale(0.55) rotate(-135deg);
        opacity: 0;
      }

      :host([checked]) [part="checkbox"]::after {
        opacity: 1;
        width: 0.625em;
        height: 1.0625em;
      }

      /* Indeterminate checkmark */

      :host([indeterminate]) [part="checkbox"]::after {
        transform: none;
        opacity: 1;
        top: 45%;
        height: 10%;
        left: 22%;
        right: 22%;
        width: auto;
        border: 0;
        background-color: var(--lumo-primary-contrast-color);
        transition: opacity 0.25s;
      }

      /* Focus ring */

      :host([focus-ring]) [part="checkbox"] {
        box-shadow: 0 0 0 3px var(--lumo-primary-color-50pct);
      }

      /* Disabled */

      :host([disabled]) {
        pointer-events: none;
        color: var(--lumo-disabled-text-color);
      }

      :host([disabled]) [part="label"] ::slotted(*) {
        color: inherit;
      }

      :host([disabled]) [part="checkbox"] {
        background-color: var(--lumo-contrast-10pct);
      }

      :host([disabled]) [part="checkbox"]::after {
        border-color: var(--lumo-contrast-30pct);
      }

      :host([indeterminate][disabled]) [part="checkbox"]::after {
        background-color: var(--lumo-contrast-30pct);
      }

      /* RTL specific styles */

      :host([dir="rtl"]) [part="label"]:not([empty]) {
        margin: 0.1875em 0.375em 0.1875em 0.875em;
      }
    </style>
  </template>
</dom-module><dom-module id="lumo-checkbox-effects">
  <template>
    <style>
      /* Transition the checkmark if activated with the mouse (disabled for grid select-all this way) */
      :host(:hover) [part="checkbox"]::after {
        transition: width 0.1s, height 0.25s;
      }

      /* Used for activation "halo" */
      [part="checkbox"]::before {
        color: transparent;
        display: inline-block;
        width: 100%;
        height: 100%;
        border-radius: inherit;
        background-color: inherit;
        transform: scale(1.4);
        opacity: 0;
        transition: transform 0.1s, opacity 0.8s;
      }

      /* Hover */

      :host(:not([checked]):not([indeterminate]):not([disabled]):hover) [part="checkbox"] {
        background-color: var(--lumo-contrast-30pct);
      }

      /* Disable hover for touch devices */
      @media (pointer: coarse) {
        :host(:not([checked]):not([indeterminate]):not([disabled]):hover) [part="checkbox"] {
          background-color: var(--lumo-contrast-20pct);
        }
      }

      /* Active */

      :host([active]) [part="checkbox"] {
        transform: scale(0.9);
        transition-duration: 0.05s;
      }

      :host([active][checked]) [part="checkbox"] {
        transform: scale(1.1);
      }

      :host([active]:not([checked])) [part="checkbox"]::before {
        transition-duration: 0.01s, 0.01s;
        transform: scale(0);
        opacity: 0.4;
      }
    </style>
  </template>
</dom-module>`;document.head.appendChild(ot.content);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
class nt extends(et(X(K(g(r))))){static get template(){return d`
    <style>
      :host {
        display: inline-block;
      }

      :host([hidden]) {
        display: none !important;
      }

      label {
        display: inline-flex;
        align-items: baseline;
        outline: none;
      }

      [part="checkbox"] {
        position: relative;
        display: inline-block;
        flex: none;
      }

      input[type="checkbox"] {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        cursor: inherit;
        margin: 0;
      }

      :host([disabled]) {
        -webkit-tap-highlight-color: transparent;
      }
    </style>

    <label>
      <span part="checkbox">
        <input type="checkbox" checked="{{checked::change}}" disabled\$="[[disabled]]" indeterminate="{{indeterminate::change}}" role="presentation" tabindex="-1">
      </span>

      <span part="label">
        <slot></slot>
      </span>
    </label>
`}static get is(){return"vaadin-checkbox"}static get version(){return"2.5.0"}static get properties(){return{checked:{type:Boolean,value:!1,notify:!0,observer:"_checkedChanged",reflectToAttribute:!0},indeterminate:{type:Boolean,notify:!0,observer:"_indeterminateChanged",reflectToAttribute:!0,value:!1},value:{type:String,value:"on"},_nativeCheckbox:{type:Object}}}constructor(){super()}get name(){return this.checked?this._storedName:""}set name(t){this._storedName=t}ready(){super.ready(),this.setAttribute("role","checkbox"),this._nativeCheckbox=this.shadowRoot.querySelector('input[type="checkbox"]'),this.addEventListener("click",this._handleClick.bind(this)),this._addActiveListeners();const t=this.getAttribute("name");t&&(this.name=t),this.shadowRoot.querySelector('[part~="label"]').querySelector("slot").addEventListener("slotchange",this._updateLabelAttribute.bind(this)),this._updateLabelAttribute()}_updateLabelAttribute(){const t=this.shadowRoot.querySelector('[part~="label"]'),e=t.firstElementChild.assignedNodes();this._isAssignedNodesEmpty(e)?t.setAttribute("empty",""):t.removeAttribute("empty")}_isAssignedNodesEmpty(t){return 0===t.length||1==t.length&&t[0].nodeType==Node.TEXT_NODE&&""===t[0].textContent.trim()}_checkedChanged(t){this.setAttribute("aria-checked",this.indeterminate?"mixed":Boolean(t))}_indeterminateChanged(t){this.setAttribute("aria-checked",t?"mixed":this.checked)}_addActiveListeners(){this._addEventListenerToNode(this,"down",t=>{this.__interactionsAllowed(t)&&this.setAttribute("active","")}),this._addEventListenerToNode(this,"up",()=>this.removeAttribute("active")),this.addEventListener("keydown",t=>{this.__interactionsAllowed(t)&&32===t.keyCode&&(t.preventDefault(),this.setAttribute("active",""))}),this.addEventListener("keyup",t=>{this.__interactionsAllowed(t)&&32===t.keyCode&&(t.preventDefault(),this._toggleChecked(),this.removeAttribute("active"),this.indeterminate&&(this.indeterminate=!1))})}get focusElement(){return this.shadowRoot.querySelector("input")}__interactionsAllowed(t){return!this.disabled&&"a"!==t.target.localName}_handleClick(t){this.__interactionsAllowed(t)&&(this.indeterminate?(this.indeterminate=!1,t.preventDefault(),this._toggleChecked()):t.composedPath()[0]!==this._nativeCheckbox&&(t.preventDefault(),this._toggleChecked()))}_toggleChecked(){this.checked=!this.checked,this.dispatchEvent(new CustomEvent("change",{composed:!1,bubbles:!0}))}}customElements.define(nt.is,nt);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
class at extends M{static get template(){return d`
    <template class="header" id="defaultHeaderTemplate">
      <vaadin-checkbox class="vaadin-grid-select-all-checkbox" aria-label="Select All" hidden\$="[[_selectAllHidden]]" on-checked-changed="_onSelectAllCheckedChanged" checked="[[_isChecked(selectAll, _indeterminate)]]" indeterminate="[[_indeterminate]]"></vaadin-checkbox>
    </template>
    <template id="defaultBodyTemplate">
      <vaadin-checkbox aria-label="Select Row" checked="{{selected}}"></vaadin-checkbox>
    </template>
`}static get is(){return"vaadin-grid-selection-column"}static get properties(){return{width:{type:String,value:"58px"},flexGrow:{type:Number,value:0},selectAll:{type:Boolean,value:!1,notify:!0},autoSelect:{type:Boolean,value:!1},_indeterminate:Boolean,_previousActiveItem:Object,_selectAllHidden:Boolean}}static get observers(){return["_onSelectAllChanged(selectAll)"]}_pathOrHeaderChanged(t,e,i,s,r,o,n,a,l){!r.value||void 0===t&&void 0===o||(this._bodyTemplate=a=void 0,this.__cleanCellsOfTemplateProperties(r.value)),!i||void 0===e&&void 0===n||(this._headerTemplate=l=void 0,this.__cleanCellsOfTemplateProperties([i])),super._pathOrHeaderChanged(t,e,i,s,r,o,n,a,l)}__cleanCellsOfTemplateProperties(t){t.forEach(t=>{t._content.innerHTML="",delete t._instance,delete t._template})}_prepareHeaderTemplate(){const t=this._prepareTemplatizer(this._findTemplate(!0)||this.$.defaultHeaderTemplate);return t.templatizer.dataHost=t===this.$.defaultHeaderTemplate?this:this.dataHost,t}_prepareBodyTemplate(){const t=this._prepareTemplatizer(this._findTemplate()||this.$.defaultBodyTemplate);return t.templatizer.dataHost=t===this.$.defaultBodyTemplate?this:this.dataHost,t}constructor(){super(),this._boundOnActiveItemChanged=this._onActiveItemChanged.bind(this),this._boundOnDataProviderChanged=this._onDataProviderChanged.bind(this),this._boundOnSelectedItemsChanged=this._onSelectedItemsChanged.bind(this)}disconnectedCallback(){if(this._grid.removeEventListener("active-item-changed",this._boundOnActiveItemChanged),this._grid.removeEventListener("data-provider-changed",this._boundOnDataProviderChanged),this._grid.removeEventListener("filter-changed",this._boundOnSelectedItemsChanged),this._grid.removeEventListener("selected-items-changed",this._boundOnSelectedItemsChanged),/^((?!chrome|android).)*safari/i.test(navigator.userAgent)&&window.ShadyDOM&&this.parentElement){const t=this.parentElement,e=this.nextElementSibling;t.removeChild(this),e?t.insertBefore(this,e):t.appendChild(this)}super.disconnectedCallback()}connectedCallback(){super.connectedCallback(),this._grid&&(this._grid.addEventListener("active-item-changed",this._boundOnActiveItemChanged),this._grid.addEventListener("data-provider-changed",this._boundOnDataProviderChanged),this._grid.addEventListener("filter-changed",this._boundOnSelectedItemsChanged),this._grid.addEventListener("selected-items-changed",this._boundOnSelectedItemsChanged))}_onSelectAllChanged(t){void 0!==t&&this._grid&&(this._selectAllChangeLock||(this._grid.selectedItems=t&&Array.isArray(this._grid.items)?this._grid._filter(this._grid.items):[]))}_arrayContains(t,e){for(var i=0;t&&e&&e[i]&&t.indexOf(e[i])>=0;i++);return i==e.length}_onSelectAllCheckedChanged(t){this.selectAll=this._indeterminate||t.target.checked}_isChecked(t,e){return e||t}_onActiveItemChanged(t){const e=t.detail.value;if(this.autoSelect){const t=e||this._previousActiveItem;t&&this._grid._toggleItem(t)}this._previousActiveItem=e}_onSelectedItemsChanged(t){this._selectAllChangeLock=!0,Array.isArray(this._grid.items)&&(this._grid.selectedItems.length?this._arrayContains(this._grid.selectedItems,this._grid._filter(this._grid.items))?(this.selectAll=!0,this._indeterminate=!1):(this.selectAll=!1,this._indeterminate=!0):(this.selectAll=!1,this._indeterminate=!1)),this._selectAllChangeLock=!1}_onDataProviderChanged(t){this._selectAllHidden=!Array.isArray(this._grid.items)}}customElements.define(at.is,at);const lt=d`<dom-module id="lumo-grid-sorter" theme-for="vaadin-grid-sorter">
  <template>
    <style>
      :host {
        justify-content: flex-start;
        align-items: baseline;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      [part="content"] {
        display: inline-block;
        overflow: hidden;
        text-overflow: ellipsis;
      }

      [part="indicators"] {
        margin-left: var(--lumo-space-s);
      }

      :host(:not([direction])) [part="indicators"]::before {
        opacity: 0.2;
      }

      :host([direction]) {
        color: var(--lumo-primary-text-color);
      }

      [part="order"] {
        font-size: var(--lumo-font-size-xxs);
        line-height: 1;
      }

      /* RTL specific styles */

      :host([dir="rtl"]) [part="indicators"] {
        margin-right: var(--lumo-space-s);
        margin-left: 0;
      }
    </style>
  </template>
</dom-module>`;document.head.appendChild(lt.content);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
const ht=document.createElement("template");ht.innerHTML="<custom-style>\n  <style>\n    @font-face {\n      font-family: 'vaadin-grid-sorter-icons';\n      src: url(data:application/font-woff;charset=utf-8;base64,d09GRgABAAAAAAQwAA0AAAAABuwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABGRlRNAAAEFAAAABkAAAAcfep+mUdERUYAAAP4AAAAHAAAAB4AJwAOT1MvMgAAAZgAAAA/AAAAYA8TBPpjbWFwAAAB7AAAAFUAAAFeF1fZ4mdhc3AAAAPwAAAACAAAAAgAAAAQZ2x5ZgAAAlgAAABcAAAAnMvguMloZWFkAAABMAAAAC8AAAA2C5Ap72hoZWEAAAFgAAAAHQAAACQGbQPHaG10eAAAAdgAAAAUAAAAHAoAAABsb2NhAAACRAAAABIAAAASAIwAYG1heHAAAAGAAAAAFgAAACAACwAKbmFtZQAAArQAAAECAAACZxWCgKhwb3N0AAADuAAAADUAAABZCrApUXicY2BkYGAA4rDECVrx/DZfGbhZGEDgyqNPOxH0/wNMq5kPALkcDEwgUQBWRA0dAHicY2BkYGA+8P8AAwMLAwgwrWZgZEAFbABY4QM8AAAAeJxjYGRgYOAAQiYGEICQSAAAAi8AFgAAeJxjYGY6yziBgZWBgWkm0xkGBoZ+CM34msGYkZMBFTAKoAkwODAwvmRiPvD/AIMDMxCD1CDJKjAwAgBktQsXAHicY2GAAMZQCM0EwqshbAALxAEKeJxjYGBgZoBgGQZGBhCIAPIYwXwWBhsgzcXAwcAEhIwMCi+Z/v/9/x+sSuElA4T9/4k4K1gHFwMMMILMY2QDYmaoABOQYGJABUA7WBiGNwAAJd4NIQAAAAAAAAAACAAIABAAGAAmAEAATgAAeJyNjLENgDAMBP9tIURJwQCMQccSZgk2i5fIYBDAidJjycXr7x5EPwE2wY8si7jmyBNXGo/bNBerxJNrpxhbO3/fEFpx8ZICpV+ghxJ74fAMe+h7Ox14AbrsHB14nK2QQWrDMBRER4mTkhQK3ZRQKOgCNk7oGQqhhEIX2WSlWEI1BAlkJ5CDdNsj5Ey9Rncdi38ES+jzNJo/HwTgATcoDEthhY3wBHc4CE+pfwsX5F/hGe7Vo/AcK/UhvMSz+mGXKhZU6pww8ISz3oWn1BvhgnwTnuEJf8Jz1OpFeIlX9YULDLdFi4ASHolkSR0iuYdjLak1vAequBhj21D61Nqyi6l3qWybGPjySbPHGScGJl6dP58MYcQRI0bts7mjebBqrFENH7t3qWtj0OuqHnXcW7b0HOTZFnKryRGW2hFX1m0O2vEM3opNMfTau+CS6Z3Vx6veNnEXY6jwDxhsc2gAAHicY2BiwA84GBgYmRiYGJkZmBlZGFkZ2djScyoLMgzZS/MyDQwMwLSrpYEBlIbxjQDrzgsuAAAAAAEAAf//AA94nGNgZGBg4AFiMSBmYmAEQnYgZgHzGAAD6wA2eJxjYGBgZACCKyoz1cD0o087YTQATOcIewAAAA==) format('woff');\n      font-weight: normal;\n      font-style: normal;\n    }\n  </style>\n</custom-style>",document.head.appendChild(ht.content);class dt extends(K(B(r))){static get template(){return d`
    <style>
      :host {
        display: inline-flex;
        cursor: pointer;
        max-width: 100%;
      }

      [part="content"] {
        flex: 1 1 auto;
      }

      [part="indicators"] {
        position: relative;
        align-self: center;
        flex: none;
      }

      [part="order"] {
        display: inline;
        vertical-align: super;
      }

      [part="indicators"]::before {
        font-family: 'vaadin-grid-sorter-icons';
        display: inline-block;
      }

      :host(:not([direction])) [part="indicators"]::before {
        content: "\\e901";
      }

      :host([direction=asc]) [part="indicators"]::before {
        content: "\\e900";
      }

      :host([direction=desc]) [part="indicators"]::before {
        content: "\\e902";
      }
    </style>

    <div part="content">
      <slot></slot>
    </div>
    <div part="indicators">
      <span part="order">[[_getDisplayOrder(_order)]]</span>
    </div>
`}static get is(){return"vaadin-grid-sorter"}static get properties(){return{path:String,direction:{type:String,reflectToAttribute:!0,notify:!0,value:null},_order:{type:Number,value:null},_isConnected:{type:Boolean,value:!1}}}static get observers(){return["_pathOrDirectionChanged(path, direction, _isConnected)","_directionOrOrderChanged(direction, _order)"]}ready(){super.ready(),this.addEventListener("click",this._onClick.bind(this))}connectedCallback(){super.connectedCallback(),this._isConnected=!0}disconnectedCallback(){super.disconnectedCallback(),this._isConnected=!1}_pathOrDirectionChanged(t,e,i){void 0!==t&&void 0!==e&&void 0!==i&&i&&this.dispatchEvent(new CustomEvent("sorter-changed",{bubbles:!0,composed:!0}))}_getDisplayOrder(t){return null===t?"":t+1}_onClick(t){const e=this.getRootNode().activeElement;this!==e&&this.contains(e)||(t.preventDefault(),this.direction="asc"===this.direction?"desc":"desc"===this.direction?null:"asc")}_directionOrOrderChanged(t,e){void 0!==t&&void 0!==e&&/^((?!chrome|android).)*safari/i.test(navigator.userAgent)&&this.root&&this.root.querySelectorAll("*").forEach((function(t){t.style["-webkit-backface-visibility"]="visible",t.style["-webkit-backface-visibility"]=""}))}}customElements.define(dt.is,dt);
/**
@license
Copyright (c) 2018 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
class ct extends M{static get template(){return d`
    <template class="header" id="headerTemplate">
      <vaadin-grid-sorter path="[[path]]" direction="{{direction}}">[[_getHeader(header, path)]]</vaadin-grid-sorter>
    </template>
`}static get is(){return"vaadin-grid-sort-column"}static get properties(){return{path:String,direction:{type:String,notify:!0}}}_prepareHeaderTemplate(){const t=this._prepareTemplatizer(this.$.headerTemplate);return t.templatizer.dataHost=this,t}_getHeader(t,e){return t||this._generateHeader(e)}}customElements.define(ct.is,ct);const ut=d`<dom-module id="lumo-grid-tree-toggle" theme-for="vaadin-grid-tree-toggle">
  <template>
    <style>
      :host {
        --vaadin-grid-tree-toggle-level-offset: 2em;
        align-items: center;
        vertical-align: middle;
        margin-left: calc(var(--lumo-space-s) * -1);
        -webkit-tap-highlight-color: transparent;
      }

      :host(:not([leaf])) {
        cursor: default;
      }

      [part="toggle"] {
        display: inline-block;
        font-size: 1.5em;
        line-height: 1;
        width: 1em;
        height: 1em;
        text-align: center;
        color: var(--lumo-contrast-50pct);
        /* Increase touch target area */
        padding: calc(1em / 3);
        margin: calc(1em / -3);
      }

      :host(:not([dir="rtl"])) [part="toggle"] {
        margin-right: 0;
      }

      @media (hover: hover) {
        :host(:hover) [part="toggle"] {
          color: var(--lumo-contrast-80pct);
        }
      }

      [part="toggle"]::before {
        font-family: "lumo-icons";
        display: inline-block;
        height: 100%;
      }

      :host(:not([expanded])) [part="toggle"]::before {
        content: var(--lumo-icons-angle-right);
      }

      :host([expanded]) [part="toggle"]::before {
        content: var(--lumo-icons-angle-right);
        transform: rotate(90deg);
      }

      /* Experimental support for hierarchy connectors, using an unsupported selector */
      :host([theme~="connectors"]) #level-spacer {
        position: relative;
        z-index: -1;
        font-size: 1em;
        height: 1.5em;
      }

      :host([theme~="connectors"]) #level-spacer::before {
        display: block;
        content: "";
        margin-top: calc(var(--lumo-space-m) * -1);
        height: calc(var(--lumo-space-m) + 3em);
        background-image: linear-gradient(to right, transparent calc(var(--vaadin-grid-tree-toggle-level-offset) - 1px), var(--lumo-contrast-10pct) calc(var(--vaadin-grid-tree-toggle-level-offset) - 1px));
        background-size: var(--vaadin-grid-tree-toggle-level-offset) var(--vaadin-grid-tree-toggle-level-offset);
        background-position: calc(var(--vaadin-grid-tree-toggle-level-offset) / 2 - 2px) 0;
      }

      /* RTL specific styles */

      :host([dir="rtl"]) {
        margin-left: 0;
        margin-right: calc(var(--lumo-space-s) * -1);
      }

      :host([dir="rtl"]) [part="toggle"] {
        margin-left: 0;
      }

      :host([dir="rtl"][expanded]) [part="toggle"]::before {
        transform: rotate(-90deg);
      }

      :host([dir="rtl"][theme~="connectors"]) #level-spacer::before {
        background-image: linear-gradient(to left, transparent calc(var(--vaadin-grid-tree-toggle-level-offset) - 1px), var(--lumo-contrast-10pct) calc(var(--vaadin-grid-tree-toggle-level-offset) - 1px));
        background-position: calc(100% - (var(--vaadin-grid-tree-toggle-level-offset) / 2 - 2px)) 0;
      }

      :host([dir="rtl"]:not([expanded])) [part="toggle"]::before,
      :host([dir="rtl"][expanded]) [part="toggle"]::before {
        content: var(--lumo-icons-angle-left);
      }
    </style>
  </template>
</dom-module>`;document.head.appendChild(ut.content);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
const pt=t=>class extends t{static get properties(){return{activeItem:{type:Object,notify:!0,value:null}}}ready(){super.ready(),this.$.scroller.addEventListener("click",this._onClick.bind(this)),this.addEventListener("cell-activate",this._activateItem.bind(this))}_activateItem(t){const e=t.detail.model,i=e?e.item:null;i&&(this.activeItem=this._itemsEqual(this.activeItem,i)?null:i)}_onClick(t){if(t.defaultPrevented)return;const e=t.composedPath(),i=e[e.indexOf(this.$.table)-3];if(!i||i.getAttribute("part").indexOf("details-cell")>-1)return;const s=i._content,r=this.getRootNode().activeElement;s.contains(r)&&(!this._ie||this._isFocusable(r))||this._isFocusable(t.target)||this.dispatchEvent(new CustomEvent("cell-activate",{detail:{model:this.__getRowModel(i.parentElement)}}))}_isFocusable(t){return mt(t)}},mt=t=>{if(!t.parentNode)return!1;const e=-1!==Array.from(t.parentNode.querySelectorAll("[tabindex], button, input, select, textarea, object, iframe, label, a[href], area[href]")).filter(t=>"cell body-cell"!==t.getAttribute("part")).indexOf(t);return!t.disabled&&e},gt=document.createElement("template");gt.innerHTML="<custom-style>\n  <style>\n    @font-face {\n      font-family: \"vaadin-grid-tree-icons\";\n      src: url(data:application/font-woff;charset=utf-8;base64,d09GRgABAAAAAAQkAA0AAAAABrwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABGRlRNAAAECAAAABoAAAAcgHwa6EdERUYAAAPsAAAAHAAAAB4AJwAOT1MvMgAAAZQAAAA/AAAAYA8TBIJjbWFwAAAB8AAAAFUAAAFeGJvXWmdhc3AAAAPkAAAACAAAAAgAAAAQZ2x5ZgAAAlwAAABLAAAAhIrPOhFoZWFkAAABMAAAACsAAAA2DsJI02hoZWEAAAFcAAAAHQAAACQHAgPHaG10eAAAAdQAAAAZAAAAHAxVAgBsb2NhAAACSAAAABIAAAASAIAAVG1heHAAAAF8AAAAGAAAACAACgAFbmFtZQAAAqgAAAECAAACTwflzbdwb3N0AAADrAAAADYAAABZQ7Ajh3icY2BkYGAA4twv3Vfi+W2+MnCzMIDANSOmbGSa2YEZRHEwMIEoAAoiB6sAeJxjYGRgYD7w/wADAwsDCDA7MDAyoAI2AFEEAtIAAAB4nGNgZGBg4GBgZgDRDAxMDGgAAAGbABB4nGNgZp7JOIGBlYGBaSbTGQYGhn4IzfiawZiRkwEVMAqgCTA4MDA+38d84P8BBgdmIAapQZJVYGAEAGc/C54AeJxjYYAAxlAIzQTELAwMBxgZGB0ACy0BYwAAAHicY2BgYGaAYBkGRgYQiADyGMF8FgYbIM3FwMHABISMDArP9/3/+/8/WJXC8z0Q9v8nEp5gHVwMMMAIMo+RDYiZoQJMQIKJARUA7WBhGN4AACFKDtoAAAAAAAAAAAgACAAQABgAJgA0AEIAAHichYvBEYBADAKBVHBjBT4swl9KS2k05o0XHd/yW1hAfBFwCv9sIlJu3nZaNS3PXAaXXHI8Lge7DlzF7C1RgXc7xkK6+gvcD2URmQB4nK2RQWoCMRiFX3RUqtCli65yADModOMBLLgQSqHddRFnQghIAnEUvEA3vUUP0LP0Fj1G+yb8R5iEhO9/ef/7FwFwj28o9EthiVp4hBlehcfUP4Ur8o/wBAv8CU+xVFvhOR7UB7tUdUdlVRJ6HnHWTnhM/V24In8JT5j/KzzFSi2E53hUz7jCcrcIiDDwyKSW1JEct2HdIPH1DFytbUM0PofWdNk5E5oUqb/Q6HHBiVGZpfOXkyUMEj5IyBuNmYZQjBobfsuassvnkKLe1OuBBj0VQ8cRni2xjLWsHaM0jrjx3peYA0/vrdmUYqe9iy7bzrX6eNP7Jh1SijX+AaUVbB8AAHicY2BiwA84GBgYmRiYGJkZmBlZGFkZ2djScyoLMgzZS/MyDQwMwLSruZMzlHaB0q4A76kLlwAAAAEAAf//AA94nGNgZGBg4AFiMSBmYmAEQnYgZgHzGAAD6wA2eJxjYGBgZACCKxJigiD6mhFTNowGACmcA/8AAA==) format('woff');\n      font-weight: normal;\n      font-style: normal;\n    }\n  </style>\n</custom-style>",document.head.appendChild(gt.content);class ft extends(K(B(r))){static get template(){return d`
    <style>
      :host {
        display: inline-flex;
        align-items: baseline;

        /* CSS API for :host */
        --vaadin-grid-tree-toggle-level-offset: 1em;

        /*
          ShadyCSS seems to polyfill :dir(rtl) only for :host, thus using
          a host custom CSS property for ltr/rtl toggle icon choice.
         */
        ---collapsed-icon: "\\e7be\\00a0";
      }

      :host(:dir(rtl)) {
        ---collapsed-icon: "\\e7bd\\00a0";
      }

      :host([hidden]) {
        display: none !important;
      }

      :host(:not([leaf])) {
        cursor: pointer;
      }

      #level-spacer,
      [part="toggle"] {
        flex: none;
      }

      #level-spacer {
        display: inline-block;
        width: calc(var(---level, '0') * var(--vaadin-grid-tree-toggle-level-offset));
      }

      [part="toggle"]::before {
        font-family: "vaadin-grid-tree-icons";
        line-height: 1em; /* make icon font metrics not affect baseline */
      }

      :host(:not([expanded])) [part="toggle"]::before {
        content: var(---collapsed-icon);
      }

      :host([expanded]) [part="toggle"]::before {
        content: "\\e7bc\\00a0"; /* icon glyph + single non-breaking space */
      }

      :host([leaf]) [part="toggle"] {
        visibility: hidden;
      }
    </style>

    <span id="level-spacer"></span>
    <span part="toggle"></span>
    <slot></slot>
`}static get is(){return"vaadin-grid-tree-toggle"}static get properties(){return{level:{type:Number,value:0,observer:"_levelChanged"},leaf:{type:Boolean,value:!1,reflectToAttribute:!0},expanded:{type:Boolean,value:!1,reflectToAttribute:!0,notify:!0}}}ready(){super.ready(),this.addEventListener("click",t=>this._onClick(t))}_onClick(t){this.leaf||mt(t.target)||(t.preventDefault(),this.expanded=!this.expanded)}_levelChanged(t){const e=Number(t).toString();this.style["---level"]=e,this._debouncerUpdateLevel=a.debounce(this._debouncerUpdateLevel,h,()=>this.updateStyles({"---level":e}))}}customElements.define(ft.is,ft);
/**
@license
Copyright (c) 2018 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
class vt extends M{static get template(){return d`
    <template id="template">
      <vaadin-grid-tree-toggle leaf="[[__isLeafItem(item, itemHasChildrenPath)]]" expanded="{{expanded}}" level="[[level]]">
        [[__getToggleContent(path, item)]]
      </vaadin-grid-tree-toggle>
    </template>
`}static get is(){return"vaadin-grid-tree-column"}static get properties(){return{path:String,itemHasChildrenPath:{type:String,value:"children"}}}_prepareBodyTemplate(){const t=this._prepareTemplatizer(this.$.template);return t.templatizer.dataHost=this,t}__isLeafItem(t,e){return!(t&&t[e])}__getToggleContent(t,e){return t&&this.get(t,e)}}customElements.define(vt.is,vt);const bt=d`<dom-module id="lumo-grid" theme-for="vaadin-grid">
  <template>
    <style>
      :host {
        font-family: var(--lumo-font-family);
        font-size: var(--lumo-font-size-m);
        line-height: var(--lumo-line-height-s);
        color: var(--lumo-body-text-color);
        background-color: var(--lumo-base-color);
        box-sizing: border-box;
        -webkit-text-size-adjust: 100%;
        -webkit-tap-highlight-color: transparent;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;

        /* For internal use only */
        --_lumo-grid-border-color: var(--lumo-contrast-20pct);
        --_lumo-grid-secondary-border-color: var(--lumo-contrast-10pct);
        --_lumo-grid-border-width: 1px;
        --_lumo-grid-selected-row-color: var(--lumo-primary-color-10pct);
      }

      /* No (outer) border */

      :host(:not([theme~="no-border"])) {
        border: var(--_lumo-grid-border-width) solid var(--_lumo-grid-border-color);
      }

      /* Cell styles */

      [part~="cell"] {
        min-height: var(--lumo-size-m);
        background-color: var(--lumo-base-color);
      }

      [part~="cell"] ::slotted(vaadin-grid-cell-content) {
        cursor: default;
        padding: var(--lumo-space-xs) var(--lumo-space-m);
      }

      /* Apply row borders by default and introduce the "no-row-borders" variant */
      :host(:not([theme~="no-row-borders"])) [part~="cell"]:not([part~="details-cell"]) {
        border-top: var(--_lumo-grid-border-width) solid var(--_lumo-grid-secondary-border-color);
      }

      /* Hide first body row top border */
      :host(:not([theme~="no-row-borders"])) [part="row"][first] [part~="cell"]:not([part~="details-cell"]) {
        border-top: 0;
        min-height: calc(var(--lumo-size-m) - var(--_lumo-grid-border-width));
      }

      /* Focus-ring */

      [part~="cell"]:focus {
        outline: none;
      }

      :host([navigating]) [part~="cell"]:focus::before {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        pointer-events: none;
        box-shadow: inset 0 0 0 2px var(--lumo-primary-color-50pct);
      }

      /* Drag and Drop styles */
      :host([dragover])::after {
        content: "";
        position: absolute;
        z-index: 100;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        pointer-events: none;
        box-shadow: inset 0 0 0 2px var(--lumo-primary-color-50pct);
      }

      [part~="row"][dragover] {
        z-index: 100 !important;
      }

      [part~="row"][dragover] [part~="cell"] {
        overflow: visible;
      }

      [part~="row"][dragover] [part~="cell"]::after {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        height: calc(var(--_lumo-grid-border-width) + 2px);
        pointer-events: none;
        background: var(--lumo-primary-color-50pct);
      }

      :host([theme~="no-row-borders"]) [dragover] [part~="cell"]::after {
        height: 2px;
      }

      [part~="row"][dragover="below"] [part~="cell"]::after {
        top: 100%;
        bottom: auto;
        margin-top: -1px;
      }

      [part~="row"][dragover="above"] [part~="cell"]::after {
        top: auto;
        bottom: 100%;
        margin-bottom: -1px;
      }

      [part~="row"][details-opened][dragover="below"] [part~="cell"]:not([part~="details-cell"])::after,
      [part~="row"][details-opened][dragover="above"] [part~="details-cell"]::after {
        display: none;
      }

      [part~="row"][dragover][dragover="on-top"] [part~="cell"]::after {
        height: 100%;
      }

      [part~="row"][dragstart] {
        /* Add bottom-space to the row so the drag number doesn't get clipped. Needed for IE/Edge */
        border-bottom: 100px solid transparent;
        z-index: 100 !important;
        opacity: 0.9;
      }

      [part~="row"][dragstart] [part~="cell"] {
        border: none !important;
        box-shadow: none !important;
      }

      [part~="row"][dragstart] [part~="cell"][last-column] {
        border-radius: 0 var(--lumo-border-radius-s) var(--lumo-border-radius-s) 0;
      }

      [part~="row"][dragstart] [part~="cell"][first-column] {
        border-radius: var(--lumo-border-radius-s) 0 0 var(--lumo-border-radius-s);
      }

      [ios] [part~="row"][dragstart] [part~="cell"] {
        background: var(--lumo-primary-color-50pct);
      }

      #scroller:not([ios]) [part~="row"][dragstart]:not([dragstart=""])::after {
        display: block;
        position: absolute;
        left: var(--_grid-drag-start-x);
        top: var(--_grid-drag-start-y);
        z-index: 100;
        content: attr(dragstart);
        align-items: center;
        justify-content: center;
        box-sizing: border-box;
        padding: calc(var(--lumo-space-xs) * 0.8);
        color: var(--lumo-error-contrast-color);
        background-color: var(--lumo-error-color);
        border-radius: var(--lumo-border-radius-m);
        font-family: var(--lumo-font-family);
        font-size: var(--lumo-font-size-xxs);
        line-height: 1;
        font-weight: 500;
        text-transform: initial;
        letter-spacing: initial;
        min-width: calc(var(--lumo-size-s) * 0.7);
        text-align: center;
      }

      /* Headers and footers */

      [part~="header-cell"] ::slotted(vaadin-grid-cell-content),
      [part~="footer-cell"] ::slotted(vaadin-grid-cell-content),
      [part~="reorder-ghost"] {
        font-size: var(--lumo-font-size-s);
        font-weight: 500;
      }

      [part~="footer-cell"] ::slotted(vaadin-grid-cell-content) {
        font-weight: 400;
      }

      [part="row"]:only-child [part~="header-cell"] {
        min-height: var(--lumo-size-xl);
      }

      /* Header borders */

      /* Hide first header row top border */
      :host(:not([theme~="no-row-borders"])) [part="row"]:first-child [part~="header-cell"] {
        border-top: 0;
      }

      [part="row"]:last-child [part~="header-cell"] {
        border-bottom: var(--_lumo-grid-border-width) solid transparent;
      }

      :host(:not([theme~="no-row-borders"])) [part="row"]:last-child [part~="header-cell"] {
        border-bottom-color: var(--_lumo-grid-secondary-border-color);
      }

      /* Overflow uses a stronger border color */
      :host([overflow~="top"]) [part="row"]:last-child [part~="header-cell"] {
        border-bottom-color: var(--_lumo-grid-border-color);
      }

      /* Footer borders */

      [part="row"]:first-child [part~="footer-cell"] {
        border-top: var(--_lumo-grid-border-width) solid transparent;
      }

      :host(:not([theme~="no-row-borders"])) [part="row"]:first-child [part~="footer-cell"] {
        border-top-color: var(--_lumo-grid-secondary-border-color);
      }

      /* Overflow uses a stronger border color */
      :host([overflow~="bottom"]) [part="row"]:first-child [part~="footer-cell"] {
        border-top-color: var(--_lumo-grid-border-color);
      }

      /* Column reordering */

      :host([reordering]) [part~="cell"] {
        background: linear-gradient(var(--lumo-shade-20pct), var(--lumo-shade-20pct)) var(--lumo-base-color);
      }

      :host([reordering]) [part~="cell"][reorder-status="allowed"] {
        background: var(--lumo-base-color);
      }

      :host([reordering]) [part~="cell"][reorder-status="dragging"] {
        background: linear-gradient(var(--lumo-contrast-5pct), var(--lumo-contrast-5pct)) var(--lumo-base-color);
      }

      [part~="reorder-ghost"] {
        opacity: 0.85;
        box-shadow: var(--lumo-box-shadow-s);
        /* TODO Use the same styles as for the cell element (reorder-ghost copies styles from the cell element) */
        padding: var(--lumo-space-s) var(--lumo-space-m) !important;
      }

      /* Column resizing */

      [part="resize-handle"] {
        width: 3px;
        background-color: var(--lumo-primary-color-50pct);
        opacity: 0;
        transition: opacity 0.2s;
      }

      :host(:not([reordering])) *:not([column-resizing]) [part~="cell"]:hover [part="resize-handle"],
      [part="resize-handle"]:active {
        opacity: 1;
        transition-delay: 0.15s;
      }

      /* Column borders */

      :host([theme~="column-borders"]) [part~="cell"]:not([last-column]):not([part~="details-cell"]) {
        border-right: var(--_lumo-grid-border-width) solid var(--_lumo-grid-secondary-border-color);
      }

      /* Frozen columns */

      [last-frozen] {
        border-right: var(--_lumo-grid-border-width) solid transparent;
        overflow: hidden;
      }

      :host([overflow~="left"]) [part~="cell"][last-frozen]:not([part~="details-cell"]) {
        border-right-color: var(--_lumo-grid-border-color);
      }

      /* Row stripes */

      :host([theme~="row-stripes"]) [part~="row"]:not([odd]) [part~="body-cell"],
      :host([theme~="row-stripes"]) [part~="row"]:not([odd]) [part~="details-cell"] {
        background-image: linear-gradient(var(--lumo-contrast-5pct), var(--lumo-contrast-5pct));
        background-repeat: repeat-x;
      }

      /* Selected row */

      /* Raise the selected rows above unselected rows (so that box-shadow can cover unselected rows) */
      :host(:not([reordering])) [part~="row"][selected] {
        z-index: 1;
      }

      :host(:not([reordering])) [part~="row"][selected] [part~="body-cell"]:not([part~="details-cell"]) {
        background-image: linear-gradient(var(--_lumo-grid-selected-row-color), var(--_lumo-grid-selected-row-color));
        background-repeat: repeat;
      }

      /* Cover the border of an unselected row */
      :host(:not([theme~="no-row-borders"])) [part~="row"][selected] [part~="cell"]:not([part~="details-cell"]) {
        box-shadow: 0 var(--_lumo-grid-border-width) 0 0 var(--_lumo-grid-selected-row-color);
      }

      /* Compact */

      :host([theme~="compact"]) [part="row"]:only-child [part~="header-cell"] {
        min-height: var(--lumo-size-m);
      }

      :host([theme~="compact"]) [part~="cell"] {
        min-height: var(--lumo-size-s);
      }

      :host([theme~="compact"]) [part="row"][first] [part~="cell"]:not([part~="details-cell"]) {
        min-height: calc(var(--lumo-size-s) - var(--_lumo-grid-border-width));
      }

      :host([theme~="compact"]) [part~="cell"] ::slotted(vaadin-grid-cell-content) {
        padding: var(--lumo-space-xs) var(--lumo-space-s);
      }

      /* Wrap cell contents */

      :host([theme~="wrap-cell-content"]) [part~="cell"] ::slotted(vaadin-grid-cell-content) {
        white-space: normal;
      }

      /* RTL specific styles */

      :host([dir="rtl"]) [part~="row"][dragstart] [part~="cell"][last-column] {
        border-radius: var(--lumo-border-radius-s) 0 0 var(--lumo-border-radius-s);
      }

      :host([dir="rtl"]) [part~="row"][dragstart] [part~="cell"][first-column] {
        border-radius: 0 var(--lumo-border-radius-s) var(--lumo-border-radius-s) 0;
      }

      :host([dir="rtl"][theme~="column-borders"]) [part~="cell"]:not([last-column]):not([part~="details-cell"]) {
        border-right: none;
        border-left: var(--_lumo-grid-border-width) solid var(--_lumo-grid-secondary-border-color);
      }

      :host([dir="rtl"]) [last-frozen] {
        border-right: none;
        border-left: var(--_lumo-grid-border-width) solid transparent;
      }

      :host([dir="rtl"][overflow~="right"]) [part~="cell"][last-frozen]:not([part~="details-cell"]) {
        border-left-color: var(--_lumo-grid-border-color);
      }
    </style>
  </template>
</dom-module><dom-module theme-for="vaadin-checkbox" id="vaadin-grid-select-all-checkbox-lumo">
  <template>
    <style>
      :host(.vaadin-grid-select-all-checkbox) {
        font-size: var(--lumo-font-size-m);
      }
   </style>
  </template>
</dom-module>`;document.head.appendChild(bt.content);
/**
@license
Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at
http://polymer.github.io/LICENSE.txt The complete set of authors may be found at
http://polymer.github.io/AUTHORS.txt The complete set of contributors may be
found at http://polymer.github.io/CONTRIBUTORS.txt Code distributed by Google as
part of the polymer project is also subject to an additional IP rights grant
found at http://polymer.github.io/PATENTS.txt
*/
const At={properties:{scrollTarget:{type:HTMLElement,value:function(){return this._defaultScrollTarget}}},observers:["_scrollTargetChanged(scrollTarget, isAttached)"],_shouldHaveListener:!0,_scrollTargetChanged:function(t,e){if(this._oldScrollTarget&&(this._toggleScrollListener(!1,this._oldScrollTarget),this._oldScrollTarget=null),e)if("document"===t)this.scrollTarget=this._doc;else if("string"==typeof t){var i=this.domHost;this.scrollTarget=i&&i.$?i.$[t]:f(this.ownerDocument).querySelector("#"+t)}else this._isValidScrollTarget()&&(this._oldScrollTarget=t,this._toggleScrollListener(this._shouldHaveListener,t))},_scrollHandler:function(){},get _defaultScrollTarget(){return this._doc},get _doc(){return this.ownerDocument.documentElement},get _scrollTop(){return this._isValidScrollTarget()?this.scrollTarget===this._doc?window.pageYOffset:this.scrollTarget.scrollTop:0},get _scrollLeft(){return this._isValidScrollTarget()?this.scrollTarget===this._doc?window.pageXOffset:this.scrollTarget.scrollLeft:0},set _scrollTop(t){this.scrollTarget===this._doc?window.scrollTo(window.pageXOffset,t):this._isValidScrollTarget()&&(this.scrollTarget.scrollTop=t)},set _scrollLeft(t){this.scrollTarget===this._doc?window.scrollTo(t,window.pageYOffset):this._isValidScrollTarget()&&(this.scrollTarget.scrollLeft=t)},scroll:function(t,e){var i;"object"==typeof t?(i=t.left,e=t.top):i=t,i=i||0,e=e||0,this.scrollTarget===this._doc?window.scrollTo(i,e):this._isValidScrollTarget()&&(this.scrollTarget.scrollLeft=i,this.scrollTarget.scrollTop=e)},get _scrollTargetWidth(){return this._isValidScrollTarget()?this.scrollTarget===this._doc?window.innerWidth:this.scrollTarget.offsetWidth:0},get _scrollTargetHeight(){return this._isValidScrollTarget()?this.scrollTarget===this._doc?window.innerHeight:this.scrollTarget.offsetHeight:0},_isValidScrollTarget:function(){return this.scrollTarget instanceof HTMLElement},_toggleScrollListener:function(t,e){var i=e===this._doc?window:e;t?this._boundScrollHandler||(this._boundScrollHandler=this._scrollHandler.bind(this),i.addEventListener("scroll",this._boundScrollHandler)):this._boundScrollHandler&&(i.removeEventListener("scroll",this._boundScrollHandler),this._boundScrollHandler=null)},toggleScrollListener:function(t){this._shouldHaveListener=t,this._toggleScrollListener(t,this.scrollTarget)}};
/**
@license
Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/var xt=navigator.userAgent.match(/iP(?:hone|ad;(?: U;)? CPU) OS (\d+)/),yt=xt&&xt[1]>=8,wt=l,_t=p,Ct=h;const kt=v({behaviors:[b,At],_ratio:.5,_scrollerPaddingTop:0,_scrollPosition:0,_physicalSize:0,_physicalAverage:0,_physicalAverageCount:0,_physicalTop:0,_virtualCount:0,_estScrollHeight:0,_scrollHeight:0,_viewportHeight:0,_viewportWidth:0,_physicalItems:null,_physicalSizes:null,_firstVisibleIndexVal:null,_collection:null,_lastVisibleIndexVal:null,_maxPages:2,_focusedVirtualIndex:-1,_itemsPerRow:1,_rowHeight:0,_templateCost:0,get _physicalBottom(){return this._physicalTop+this._physicalSize},get _scrollBottom(){return this._scrollPosition+this._viewportHeight},get _virtualEnd(){return this._virtualStart+this._physicalCount-1},get _hiddenContentSize(){return(this.grid?this._physicalRows*this._rowHeight:this._physicalSize)-this._viewportHeight},get _maxScrollTop(){return this._estScrollHeight-this._viewportHeight+this._scrollOffset},get _maxVirtualStart(){var t=this._convertIndexToCompleteRow(this._virtualCount);return Math.max(0,t-this._physicalCount)},set _virtualStart(t){t=this._clamp(t,0,this._maxVirtualStart),this.grid&&(t-=t%this._itemsPerRow),this._virtualStartVal=t},get _virtualStart(){return this._virtualStartVal||0},set _physicalStart(t){(t%=this._physicalCount)<0&&(t=this._physicalCount+t),this.grid&&(t-=t%this._itemsPerRow),this._physicalStartVal=t},get _physicalStart(){return this._physicalStartVal||0},get _physicalEnd(){return(this._physicalStart+this._physicalCount-1)%this._physicalCount},set _physicalCount(t){this._physicalCountVal=t},get _physicalCount(){return this._physicalCountVal||0},get _optPhysicalSize(){return 0===this._viewportHeight?1/0:this._viewportHeight*this._maxPages},get _isVisible(){return Boolean(this.offsetWidth||this.offsetHeight)},get firstVisibleIndex(){var t=this._firstVisibleIndexVal;if(null==t){var e=this._physicalTop+this._scrollOffset;t=this._iterateItems((function(t,i){return(e+=this._getPhysicalSizeIncrement(t))>this._scrollPosition?this.grid?i-i%this._itemsPerRow:i:this.grid&&this._virtualCount-1===i?i-i%this._itemsPerRow:void 0}))||0,this._firstVisibleIndexVal=t}return t},get lastVisibleIndex(){var t=this._lastVisibleIndexVal;if(null==t){if(this.grid)t=Math.min(this._virtualCount,this.firstVisibleIndex+this._estRowsInView*this._itemsPerRow-1);else{var e=this._physicalTop+this._scrollOffset;this._iterateItems((function(i,s){e<this._scrollBottom&&(t=s),e+=this._getPhysicalSizeIncrement(i)}))}this._lastVisibleIndexVal=t}return t},get _scrollOffset(){return this._scrollerPaddingTop},attached:function(){this._debounce("_render",this._render,wt),this.listen(this,"iron-resize","_resizeHandler")},detached:function(){this.unlisten(this,"iron-resize","_resizeHandler")},updateViewportBoundaries:function(){var t=window.getComputedStyle(this);this._scrollerPaddingTop=this.scrollTarget===this?0:parseInt(t["padding-top"],10),this._isRTL=Boolean("rtl"===t.direction),this._viewportWidth=this.$.items.offsetWidth,this._viewportHeight=this._scrollTargetHeight,this.grid&&this._updateGridMetrics()},_scrollHandler:function(){var t=Math.max(0,Math.min(this._maxScrollTop,this._scrollTop)),e=t-this._scrollPosition,i=e>=0;if(this._scrollPosition=t,this._firstVisibleIndexVal=null,this._lastVisibleIndexVal=null,Math.abs(e)>this._physicalSize&&this._physicalSize>0){e-=this._scrollOffset;var s=Math.round(e/this._physicalAverage)*this._itemsPerRow;this._virtualStart=this._virtualStart+s,this._physicalStart=this._physicalStart+s,this._physicalTop=Math.floor(this._virtualStart/this._itemsPerRow)*this._physicalAverage,this._update()}else if(this._physicalCount>0){var r=this._getReusables(i);i?(this._physicalTop=r.physicalTop,this._virtualStart=this._virtualStart+r.indexes.length,this._physicalStart=this._physicalStart+r.indexes.length):(this._virtualStart=this._virtualStart-r.indexes.length,this._physicalStart=this._physicalStart-r.indexes.length),this._update(r.indexes,i?null:r.indexes),this._debounce("_increasePoolIfNeeded",this._increasePoolIfNeeded.bind(this,0),Ct)}},_getReusables:function(t){var e,i,s,r=[],o=this._hiddenContentSize*this._ratio,n=this._virtualStart,a=this._virtualEnd,l=this._physicalCount,h=this._physicalTop+this._scrollOffset,d=this._scrollTop,c=this._scrollBottom;for(t?(e=this._physicalStart,i=d-h):(e=this._physicalEnd,i=this._physicalBottom+this._scrollOffset-c);i-=s=this._getPhysicalSizeIncrement(e),!(r.length>=l||i<=o);)if(t){if(a+r.length+1>=this._virtualCount)break;if(h+s>=d-this._scrollOffset)break;r.push(e),h+=s,e=(e+1)%l}else{if(n-r.length<=0)break;if(h+this._physicalSize-s<=c)break;r.push(e),h-=s,e=0===e?l-1:e-1}return{indexes:r,physicalTop:h-this._scrollOffset}},_update:function(t,e){if(!(t&&0===t.length||0===this._physicalCount)){if(this._manageFocus(),this._assignModels(t),this._updateMetrics(t),e)for(;e.length;){var i=e.pop();this._physicalTop-=this._getPhysicalSizeIncrement(i)}this._positionItems(),this._updateScrollerSize()}},_isClientFull:function(){return 0!=this._scrollBottom&&this._physicalBottom-1>=this._scrollBottom&&this._physicalTop<=this._scrollPosition},_increasePoolIfNeeded:function(t){var e=this._clamp(this._physicalCount+t,3,this._virtualCount-this._virtualStart),i=(e=this._convertIndexToCompleteRow(e))-this._physicalCount,s=Math.round(.5*this._physicalCount);if(!(i<0)){if(i>0){var r=window.performance.now();[].push.apply(this._physicalItems,this._createPool(i));for(var o=0;o<i;o++)this._physicalSizes.push(0);this._physicalCount=this._physicalCount+i,this._physicalStart>this._physicalEnd&&this._isIndexRendered(this._focusedVirtualIndex)&&this._getPhysicalIndex(this._focusedVirtualIndex)<this._physicalEnd&&(this._physicalStart=this._physicalStart+i),this._update(),this._templateCost=(window.performance.now()-r)/i,s=Math.round(.5*this._physicalCount)}this._virtualEnd>=this._virtualCount-1||0===s||(this._isClientFull()?this._physicalSize<this._optPhysicalSize&&this._debounce("_increasePoolIfNeeded",this._increasePoolIfNeeded.bind(this,this._clamp(Math.round(50/this._templateCost),1,s)),_t):this._debounce("_increasePoolIfNeeded",this._increasePoolIfNeeded.bind(this,s),Ct))}},_render:function(){if(this.isAttached&&this._isVisible)if(0!==this._physicalCount){var t=this._getReusables(!0);this._physicalTop=t.physicalTop,this._virtualStart=this._virtualStart+t.indexes.length,this._physicalStart=this._physicalStart+t.indexes.length,this._update(t.indexes),this._update(),this._increasePoolIfNeeded(0)}else this._virtualCount>0&&(this.updateViewportBoundaries(),this._increasePoolIfNeeded(3))},_itemsChanged:function(t){"items"===t.path&&(this._virtualStart=0,this._physicalTop=0,this._virtualCount=this.items?this.items.length:0,this._collection=null,this._physicalIndexForKey={},this._firstVisibleIndexVal=null,this._lastVisibleIndexVal=null,this._physicalCount=this._physicalCount||0,this._physicalItems=this._physicalItems||[],this._physicalSizes=this._physicalSizes||[],this._physicalStart=0,this._scrollTop>this._scrollOffset&&this._resetScrollPosition(0),this._removeFocusedItem(),this._debounce("_render",this._render,wt))},_iterateItems:function(t,e){var i,s,r,o;if(2===arguments.length&&e){for(o=0;o<e.length;o++)if(s=this._computeVidx(i=e[o]),null!=(r=t.call(this,i,s)))return r}else{for(i=this._physicalStart,s=this._virtualStart;i<this._physicalCount;i++,s++)if(null!=(r=t.call(this,i,s)))return r;for(i=0;i<this._physicalStart;i++,s++)if(null!=(r=t.call(this,i,s)))return r}},_computeVidx:function(t){return t>=this._physicalStart?this._virtualStart+(t-this._physicalStart):this._virtualStart+(this._physicalCount-this._physicalStart)+t},_updateMetrics:function(t){A();var e=0,i=0,s=this._physicalAverageCount,r=this._physicalAverage;this._iterateItems((function(t){i+=this._physicalSizes[t],this._physicalSizes[t]=this._physicalItems[t].offsetHeight,e+=this._physicalSizes[t],this._physicalAverageCount+=this._physicalSizes[t]?1:0}),t),this.grid?(this._updateGridMetrics(),this._physicalSize=Math.ceil(this._physicalCount/this._itemsPerRow)*this._rowHeight):(i=1===this._itemsPerRow?i:Math.ceil(this._physicalCount/this._itemsPerRow)*this._rowHeight,this._physicalSize=this._physicalSize+e-i,this._itemsPerRow=1),this._physicalAverageCount!==s&&(this._physicalAverage=Math.round((r*s+e)/this._physicalAverageCount))},_positionItems:function(){this._adjustScrollPosition();var t=this._physicalTop;this._iterateItems((function(e){this.translate3d(0,t+"px",0,this._physicalItems[e]),t+=this._physicalSizes[e]}))},_getPhysicalSizeIncrement:function(t){return this.grid?this._computeVidx(t)%this._itemsPerRow!=this._itemsPerRow-1?0:this._rowHeight:this._physicalSizes[t]},_adjustScrollPosition:function(){var t=0===this._virtualStart?this._physicalTop:Math.min(this._scrollPosition+this._physicalTop,0);if(0!==t){this._physicalTop=this._physicalTop-t;var e=this._scrollTop;!yt&&e>0&&this._resetScrollPosition(e-t)}},_resetScrollPosition:function(t){this.scrollTarget&&t>=0&&(this._scrollTop=t,this._scrollPosition=this._scrollTop)},_updateScrollerSize:function(t){this._estScrollHeight=this.grid?this._virtualRowCount*this._rowHeight:this._physicalBottom+Math.max(this._virtualCount-this._physicalCount-this._virtualStart,0)*this._physicalAverage,((t=(t=(t=t||0===this._scrollHeight)||this._scrollPosition>=this._estScrollHeight-this._physicalSize)||this.grid&&this.$.items.style.height<this._estScrollHeight)||Math.abs(this._estScrollHeight-this._scrollHeight)>=this._viewportHeight)&&(this.$.items.style.height=this._estScrollHeight+"px",this._scrollHeight=this._estScrollHeight)},scrollToIndex:function(t){if(!("number"!=typeof t||t<0||t>this.items.length-1)&&(A(),0!==this._physicalCount)){t=this._clamp(t,0,this._virtualCount-1),(!this._isIndexRendered(t)||t>=this._maxVirtualStart)&&(this._virtualStart=this.grid?t-2*this._itemsPerRow:t-1),this._manageFocus(),this._assignModels(),this._updateMetrics(),this._physicalTop=Math.floor(this._virtualStart/this._itemsPerRow)*this._physicalAverage;for(var e=this._physicalStart,i=this._virtualStart,s=0,r=this._hiddenContentSize;i<t&&s<=r;)s+=this._getPhysicalSizeIncrement(e),e=(e+1)%this._physicalCount,i++;this._updateScrollerSize(!0),this._positionItems(),this._resetScrollPosition(this._physicalTop+this._scrollOffset+s),this._increasePoolIfNeeded(0),this._firstVisibleIndexVal=null,this._lastVisibleIndexVal=null}},_resetAverage:function(){this._physicalAverage=0,this._physicalAverageCount=0},_resizeHandler:function(){this._debounce("_render",(function(){this._firstVisibleIndexVal=null,this._lastVisibleIndexVal=null,this.updateViewportBoundaries(),this._isVisible?(this.toggleScrollListener(!0),this._resetAverage(),this._render()):this.toggleScrollListener(!1)}),wt)},_convertIndexToCompleteRow:function(t){return this._itemsPerRow=this._itemsPerRow||1,this.grid?Math.ceil(t/this._itemsPerRow)*this._itemsPerRow:t},_isIndexRendered:function(t){return t>=this._virtualStart&&t<=this._virtualEnd},_getPhysicalIndex:function(t){return(this._physicalStart+(t-this._virtualStart))%this._physicalCount},_clamp:function(t,e,i){return Math.min(i,Math.max(e,t))},_debounce:function(t,e,i){this._debouncers=this._debouncers||{},this._debouncers[t]=a.debounce(this._debouncers[t],i,e.bind(this)),m(this._debouncers[t])}});
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/class zt extends kt{static get is(){return"vaadin-grid-scroller"}static get properties(){return{size:{type:Number,notify:!0},_vidxOffset:{value:0}}}static get observers(){return["_effectiveSizeChanged(_effectiveSize)"]}connectedCallback(){super.connectedCallback(),this._scrollHandler()}_updateScrollerItem(t,e){}_afterScroll(){}_getRowTarget(){}_createScrollerRows(){}_canPopulate(){}scrollToIndex(t){this._warnPrivateAPIAccess("scrollToIndex"),t>0&&(this._pendingScrollToIndex=null),!parseInt(this.$.items.style.borderTopWidth)&&t>0&&(this._pendingScrollToIndex=t),this._scrollingToIndex=!0,t=Math.min(Math.max(t,0),this._effectiveSize-1),this.$.table.scrollTop=t/this._effectiveSize*(this.$.table.scrollHeight-this.$.table.offsetHeight),this._scrollHandler(),this._accessIronListAPI(()=>this._maxScrollTop)&&this._virtualCount<this._effectiveSize&&this._adjustVirtualIndexOffset(1e6),this._accessIronListAPI(()=>super.scrollToIndex(t-this._vidxOffset)),this._scrollHandler();const e=Array.from(this.$.items.children).filter(e=>e.index===t)[0];if(e){const t=e.getBoundingClientRect().top-this.$.header.getBoundingClientRect().bottom;Math.abs(t)>1&&(this.$.table.scrollTop+=t,this._scrollHandler())}this._scrollingToIndex=!1}_effectiveSizeChanged(t){let e,i=0;this._iterateItems((t,s)=>{if(s===this._firstVisibleIndex){const s=this._physicalItems[t];e=s.index,i=s.getBoundingClientRect().top}}),this.items&&t<this.items.length&&(this._scrollTop=0),Array.isArray(this.items)||(this.items={length:Math.min(t,this._edge||this._ie?3e4:1e5)}),this._accessIronListAPI(()=>super._itemsChanged({path:"items"})),this._virtualCount=Math.min(this.items.length,t)||0,0===this._scrollTop&&(this._accessIronListAPI(()=>this._scrollToIndex(Math.min(t-1,e))),this._iterateItems(t=>{const s=this._physicalItems[t];if(s.index===e&&(this.$.table.scrollTop+=Math.round(s.getBoundingClientRect().top-i)),s.index===this._focusedItemIndex&&this._itemsFocusable&&this.$.items.contains(this.shadowRoot.activeElement)){const t=Array.from(this._itemsFocusable.parentElement.children).indexOf(this._itemsFocusable);s.children[t].focus()}})),this._assignModels(),requestAnimationFrame(()=>this._update())}_positionItems(){let t;this._adjustScrollPosition(),isNaN(this._physicalTop)&&(t=!0,this._physicalTop=0);let e=this._physicalTop;this._iterateItems(t=>{this._physicalItems[t].style.transform=`translateY(${e}px)`,e+=this._physicalSizes[t]}),t&&this._scrollToIndex(0)}_increasePoolIfNeeded(t){0===t&&this._scrollingToIndex||!this._canPopulate()||!this._effectiveSize||(this._initialPoolCreated?this._optPhysicalSize!==1/0&&(this._debounceIncreasePool=a.debounce(this._debounceIncreasePool,l,()=>{this._updateMetrics();let t=Math.ceil((this._optPhysicalSize-this._physicalSize)/this._physicalAverage);this._physicalCount+t>this._effectiveSize&&(t=Math.max(0,this._effectiveSize-this._physicalCount)),this._physicalSize&&t>0&&this._optPhysicalSize!==1/0&&(super._increasePoolIfNeeded(t),this.__reorderChildNodes())})):(this._initialPoolCreated=!0,super._increasePoolIfNeeded(25)))}__reorderChildNodes(){const t=Array.from(this.$.items.childNodes);t.reduce((t,e,i,s)=>{if(0===i||s[i-1].index===e.index-1)return t},!0)||t.sort((t,e)=>t.index-e.index).forEach(t=>this.$.items.appendChild(t))}_createPool(t){const e=document.createDocumentFragment(),i=this._createScrollerRows(t);i.forEach(t=>e.appendChild(t)),this._getRowTarget().appendChild(e);const s=this.querySelector("[slot]");if(s){const t=s.getAttribute("slot");s.setAttribute("slot","foo-bar"),s.setAttribute("slot",t)}return this._updateHeaderFooterMetrics(),x(this,()=>this.notifyResize()),i}_assignModels(t){this._iterateItems((t,e)=>{const i=this._physicalItems[t];this._toggleAttribute("hidden",e>=this._effectiveSize,i),this._updateScrollerItem(i,e+(this._vidxOffset||0))},t)}_scrollHandler(){const t=this.$.table.scrollTop-this._scrollPosition;this._accessIronListAPI(super._scrollHandler);const e=this._vidxOffset;this._accessIronListAPI(()=>this._maxScrollTop)&&this._virtualCount<this._effectiveSize?this._adjustVirtualIndexOffset(t):this._vidxOffset=0,this._vidxOffset!==e&&this._update(),this._afterScroll()}_adjustVirtualIndexOffset(t){if(Math.abs(t)>1e4){if(this._noScale)return void(this._noScale=!1);const t=this.$.table.scrollTop/(this.$.table.scrollHeight-this.$.table.offsetHeight);this._vidxOffset=Math.round(t*this._effectiveSize-t*this._virtualCount)}else{const t=this._vidxOffset||0,e=1e3,i=100;0===this._scrollTop?(this._vidxOffset=0,t!==this._vidxOffset&&super.scrollToIndex(0)):this.firstVisibleIndex<e&&this._vidxOffset>0&&(this._vidxOffset-=Math.min(this._vidxOffset,i),t!==this._vidxOffset&&super.scrollToIndex(this.firstVisibleIndex+(t-this._vidxOffset)),this._noScale=!0);const s=this._effectiveSize-this._virtualCount;this._scrollTop>=this._maxScrollTop&&this._maxScrollTop>0?(this._vidxOffset=s,t!==this._vidxOffset&&super.scrollToIndex(this._virtualCount)):this.firstVisibleIndex>this._virtualCount-e&&this._vidxOffset<s&&(this._vidxOffset+=Math.min(s-this._vidxOffset,i),t!==this._vidxOffset&&super.scrollToIndex(this.firstVisibleIndex-(this._vidxOffset-t)),this._noScale=!0)}}_accessIronListAPI(t){this._warnPrivateAPIAccessAsyncEnabled=!1;const e=t.apply(this);return this._debouncerWarnPrivateAPIAccess=a.debounce(this._debouncerWarnPrivateAPIAccess,l,()=>this._warnPrivateAPIAccessAsyncEnabled=!0),e}_debounceRender(t,e){super._debounceRender(()=>this._accessIronListAPI(t),e)}_warnPrivateAPIAccess(t){this._warnPrivateAPIAccessAsyncEnabled&&console.warn(`Accessing private API (${t})!`)}_render(){this._accessIronListAPI(super._render)}_createFocusBackfillItem(){}_multiSelectionChanged(){}clearSelection(){}_itemsChanged(){}_manageFocus(){}_removeFocusedItem(){}get _firstVisibleIndex(){return this._accessIronListAPI(()=>super.firstVisibleIndex)}get _lastVisibleIndex(){return this._accessIronListAPI(()=>super.lastVisibleIndex)}_scrollToIndex(t){this._accessIronListAPI(()=>this.scrollToIndex(t))}get firstVisibleIndex(){return this._warnPrivateAPIAccess("firstVisibleIndex"),super.firstVisibleIndex}set firstVisibleIndex(t){this._warnPrivateAPIAccess("firstVisibleIndex"),super.firstVisibleIndex=t}get lastVisibleIndex(){return this._warnPrivateAPIAccess("lastVisibleIndex"),super.lastVisibleIndex}set lastVisibleIndex(t){this._warnPrivateAPIAccess("lastVisibleIndex"),super.lastVisibleIndex=t}updateViewportBoundaries(){this._warnPrivateAPIAccess("updateViewportBoundaries"),super.updateViewportBoundaries.apply(this,arguments)}_resizeHandler(){super._resizeHandler(),A()}}customElements.define(zt.is,zt);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
const It=t=>class extends t{static get observers(){return["_a11yUpdateGridSize(size, _columnTree, _columnTree.*)"]}_a11yGetHeaderRowCount(t){return t.filter(t=>t.some(t=>t._headerTemplate||t.headerRenderer||t.path||t.header)).length}_a11yGetFooterRowCount(t){return t.filter(t=>t.some(t=>t._headerTemplate||t.headerRenderer)).length}_a11yUpdateGridSize(t,e){if(void 0===t||void 0===e)return;const i=e[e.length-1];this.$.table.setAttribute("aria-rowcount",t+this._a11yGetHeaderRowCount(e)+this._a11yGetFooterRowCount(e)),this.$.table.setAttribute("aria-colcount",i&&i.length||0),this._a11yUpdateHeaderRows(),this._a11yUpdateFooterRows()}_a11yUpdateHeaderRows(){Array.from(this.$.header.children).forEach((t,e)=>t.setAttribute("aria-rowindex",e+1))}_a11yUpdateFooterRows(){Array.from(this.$.footer.children).forEach((t,e)=>t.setAttribute("aria-rowindex",this._a11yGetHeaderRowCount(this._columnTree)+this.size+e+1))}_a11yUpdateRowRowindex(t,e){t.setAttribute("aria-rowindex",e+this._a11yGetHeaderRowCount(this._columnTree)+1)}_a11yUpdateRowSelected(t,e){t.setAttribute("aria-selected",Boolean(e)),Array.from(t.children).forEach(t=>t.setAttribute("aria-selected",Boolean(e)))}_a11yUpdateRowLevel(t,e){t.setAttribute("aria-level",e+1)}_a11yUpdateRowDetailsOpened(t,e){Array.from(t.children).forEach(t=>{"boolean"==typeof e?t.setAttribute("aria-expanded",e):t.hasAttribute("aria-expanded")&&t.removeAttribute("aria-expanded")})}_a11ySetRowDetailsCell(t,e){Array.from(t.children).forEach(t=>{t!==e&&t.setAttribute("aria-controls",e.id)})}_a11yUpdateCellColspan(t,e){t.setAttribute("aria-colspan",Number(e))}_a11yUpdateSorters(){Array.from(this.querySelectorAll("vaadin-grid-sorter")).forEach(t=>{let e=t.parentNode;for(;e&&"vaadin-grid-cell-content"!==e.localName;)e=e.parentNode;e&&e.assignedSlot&&e.assignedSlot.parentNode.setAttribute("aria-sort",{asc:"ascending",desc:"descending"}[String(t.direction)]||"none")})}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,St=t=>class extends t{static get properties(){return{items:Array}}static get observers(){return["_itemsChanged(items, items.*, isAttached)"]}_itemsChanged(t,e,i){if(i){if(!Array.isArray(t))return null==t&&(this.size=0),void(this.dataProvider===this._arrayDataProvider&&(this.dataProvider=void 0));this.size=t.length,this.dataProvider=this.dataProvider||this._arrayDataProvider,this.clearCache(),this._ensureFirstPageLoaded()}}_arrayDataProvider(t,e){let i=(Array.isArray(this.items)?this.items:[]).slice(0);this._filters&&this._checkPaths(this._filters,"filtering",i)&&(i=this._filter(i)),this.size=i.length,t.sortOrders.length&&this._checkPaths(this._sorters,"sorting",i)&&(i=i.sort(this._multiSort.bind(this)));const s=t.page*t.pageSize;e(i.slice(s,s+t.pageSize),i.length)}_checkPaths(t,e,i){if(!i.length)return!1;let s=!0;for(var r in t){const o=t[r].path;if(!o||-1===o.indexOf("."))continue;const n=o.replace(/\.[^\.]*$/,"");void 0===y.get(n,i[0])&&(console.warn(`Path "${o}" used for ${e} does not exist in all of the items, ${e} is disabled.`),s=!1)}return s}_multiSort(t,e){return this._sorters.map(i=>"asc"===i.direction?this._compare(y.get(i.path,t),y.get(i.path,e)):"desc"===i.direction?this._compare(y.get(i.path,e),y.get(i.path,t)):0).reduce((t,e)=>t||e,0)}_normalizeEmptyValue(t){return[void 0,null].indexOf(t)>=0?"":isNaN(t)?t.toString():t}_compare(t,e){return(t=this._normalizeEmptyValue(t))<(e=this._normalizeEmptyValue(e))?-1:t>e?1:0}_filter(t){return t.filter(t=>0===this._filters.filter(e=>{const i=this._normalizeEmptyValue(y.get(e.path,t)),s=this._normalizeEmptyValue(e.value).toString().toLowerCase();return-1===i.toString().toLowerCase().indexOf(s)}).length)}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Tt=t=>class extends(g(t)){ready(){super.ready();const t=this.$.scroller;w(t,"track",this._onHeaderTrack.bind(this)),t.addEventListener("touchmove",e=>t.hasAttribute("column-resizing")&&e.preventDefault()),t.addEventListener("contextmenu",t=>"resize-handle"==t.target.getAttribute("part")&&t.preventDefault()),t.addEventListener("mousedown",t=>"resize-handle"===t.target.getAttribute("part")&&t.preventDefault())}_onHeaderTrack(t){const e=t.target;if("resize-handle"===e.getAttribute("part")){let o=e.parentElement._column;for(this._toggleAttribute("column-resizing",!0,this.$.scroller);"vaadin-grid-column-group"===o.localName;)o=Array.prototype.slice.call(o._childColumns,0).sort((function(t,e){return t._order-e._order})).filter((function(t){return!t.hidden})).pop();const n=Array.from(this.$.header.querySelectorAll('[part~="row"]:last-child [part~="cell"]'));var i=n.filter(t=>t._column===o)[0];if(i.offsetWidth){var s=window.getComputedStyle(i),r=10+parseInt(s.paddingLeft)+parseInt(s.paddingRight)+parseInt(s.borderLeftWidth)+parseInt(s.borderRightWidth)+parseInt(s.marginLeft)+parseInt(s.marginRight);const e=i.offsetWidth+(this.__isRTL?i.getBoundingClientRect().left-t.detail.x:t.detail.x-i.getBoundingClientRect().right);o.width=Math.max(r,e)+"px",o.flexGrow=0}n.sort((function(t,e){return t._column._order-e._column._order})).forEach((function(t,e,s){e<s.indexOf(i)&&(t._column.width=t.offsetWidth+"px",t._column.flexGrow=0)})),"end"===t.detail.state&&(this._toggleAttribute("column-resizing",!1,this.$.scroller),this.dispatchEvent(new CustomEvent("column-resize",{detail:{resizedColumn:o}}))),this._resizeHandler()}}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Bt=class t{constructor(t,e,i){this.grid=t,this.parentCache=e,this.parentItem=i,this.itemCaches={},this.items={},this.effectiveSize=0,this.size=0,this.pendingRequests={}}isLoading(){return Boolean(Object.keys(this.pendingRequests).length||Object.keys(this.itemCaches).filter(t=>this.itemCaches[t].isLoading())[0])}getItemForIndex(t){const{cache:e,scaledIndex:i}=this.getCacheAndIndex(t);return e.items[i]}updateSize(){this.effectiveSize=!this.parentItem||this.grid._isExpanded(this.parentItem)?this.size+Object.keys(this.itemCaches).reduce((t,e)=>{const i=this.itemCaches[e];return i.updateSize(),t+i.effectiveSize},0):0}ensureSubCacheForScaledIndex(e){if(!this.itemCaches[e]){const i=new t(this.grid,this,this.items[e]);this.itemCaches[e]=i,this.grid._loadPage(0,i)}}getCacheAndIndex(t){let e=t;const i=Object.keys(this.itemCaches);for(var s=0;s<i.length;s++){const t=Number(i[s]),r=this.itemCaches[t];if(e<=t)return{cache:this,scaledIndex:e};if(e<=t+r.effectiveSize)return r.getCacheAndIndex(e-t-1);e-=r.effectiveSize}return{cache:this,scaledIndex:e}}},Et=t=>class extends t{static get properties(){return{pageSize:{type:Number,value:50,observer:"_pageSizeChanged"},dataProvider:{type:Object,notify:!0,observer:"_dataProviderChanged"},loading:{type:Boolean,notify:!0,readOnly:!0,reflectToAttribute:!0},_cache:{type:Object,value:function(){return new Bt(this)}},itemIdPath:{type:String,value:null},expandedItems:{type:Object,notify:!0,value:()=>[]}}}static get observers(){return["_sizeChanged(size)","_itemIdPathChanged(itemIdPath)","_expandedItemsChanged(expandedItems.*)"]}_sizeChanged(t){const e=t-this._cache.size;this._cache.size+=e,this._cache.effectiveSize+=e,this._effectiveSize=this._cache.effectiveSize}_getItem(t,e){if(t>=this._effectiveSize)return;e.index=t;const{cache:i,scaledIndex:s}=this._cache.getCacheAndIndex(t),r=i.items[s];r?(this._toggleAttribute("loading",!1,e),this._updateItem(e,r),this._isExpanded(r)&&i.ensureSubCacheForScaledIndex(s)):(this._toggleAttribute("loading",!0,e),this._loadPage(this._getPageForIndex(s),i))}_expandedInstanceChangedCallback(t,e){void 0!==t.item&&(e?this.expandItem(t.item):this.collapseItem(t.item))}getItemId(t){return this.itemIdPath?this.get(this.itemIdPath,t):t}_isExpanded(t){return this.__expandedKeys.has(this.getItemId(t))}_expandedItemsChanged(t){this.__cacheExpandedKeys(),this._cache.updateSize(),this._effectiveSize=this._cache.effectiveSize,this._assignModels()}_itemIdPathChanged(t){this.__cacheExpandedKeys()}__cacheExpandedKeys(){this.expandedItems&&(this.__expandedKeys=new Set,this.expandedItems.forEach(t=>{this.__expandedKeys.add(this.getItemId(t))}))}expandItem(t){this._isExpanded(t)||this.push("expandedItems",t)}collapseItem(t){this._isExpanded(t)&&this.splice("expandedItems",this._getItemIndexInArray(t,this.expandedItems),1)}_getIndexLevel(t){let{cache:e}=this._cache.getCacheAndIndex(t),i=0;for(;e.parentCache;)e=e.parentCache,i++;return i}_canPopulate(){return Boolean(this._hasData&&this._columnTree)}_loadPage(t,e){if(!e.pendingRequests[t]&&this.dataProvider){this._setLoading(!0),e.pendingRequests[t]=!0;const i={page:t,pageSize:this.pageSize,sortOrders:this._mapSorters(),filters:this._mapFilters(),parentItem:e.parentItem};this.dataProvider(i,(s,r)=>{void 0!==r?e.size=r:i.parentItem&&(e.size=s.length);const o=Array.from(this.$.items.children).map(t=>t._item);s.forEach((i,s)=>{const r=t*this.pageSize+s;e.items[r]=i,this._isExpanded(i)&&o.indexOf(i)>-1&&e.ensureSubCacheForScaledIndex(r)}),this._hasData=!0,delete e.pendingRequests[t],this._debouncerApplyCachedData=a.debounce(this._debouncerApplyCachedData,c.after(0),()=>{this._setLoading(!1),this._cache.updateSize(),this._effectiveSize=this._cache.effectiveSize,Array.from(this.$.items.children).filter(t=>!t.hidden).forEach(t=>{const e=this._cache.getItemForIndex(t.index);e&&(this._toggleAttribute("loading",!1,t),this._updateItem(t,e))}),this._increasePoolIfNeeded(0)}),this._cache.isLoading()||this._debouncerApplyCachedData.flush(),this.__itemsReceived()})}}_getPageForIndex(t){return Math.floor(t/this.pageSize)}clearCache(){this._cache=new Bt(this),Array.from(this.$.items.children).forEach(t=>{Array.from(t.children).forEach(t=>{t._instance&&t._instance._setPendingProperty("item",{},!1)})}),this._cache.size=this.size||0,this._cache.updateSize(),this._hasData=!1,this._assignModels(),this._effectiveSize||this._loadPage(0,this._cache)}_pageSizeChanged(t,e){void 0!==e&&t!==e&&this.clearCache()}_checkSize(){void 0===this.size&&0===this._effectiveSize&&console.warn("The <vaadin-grid> needs the total number of items in order to display rows. Set the total number of items to the `size` property, or provide the total number of items in the second argument of the `dataProvider`’s `callback` call.")}_dataProviderChanged(t,e){void 0!==e&&this.clearCache(),t&&this.items&&this.items.length&&this._scrollToIndex(this._firstVisibleIndex),this._ensureFirstPageLoaded(),this._debouncerCheckSize=a.debounce(this._debouncerCheckSize,c.after(2e3),this._checkSize.bind(this)),this._scrollHandler()}_ensureFirstPageLoaded(){this._hasData||this._loadPage(0,this._cache,()=>{const t=this._hasData;this._hasData=!0,t||this.notifyResize()})}_itemsEqual(t,e){return this.getItemId(t)===this.getItemId(e)}_getItemIndexInArray(t,e){let i=-1;return e.forEach((e,s)=>{this._itemsEqual(e,t)&&(i=s)}),i}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Ft=t=>class extends t{ready(){super.ready(),this._addNodeObserver()}_hasColumnGroups(t){for(let e=0;e<t.length;e++)if("vaadin-grid-column-group"===t[e].localName)return!0;return!1}_getChildColumns(t){return n.getFlattenedNodes(t).filter(this._isColumnElement)}_flattenColumnGroups(t){return t.map(t=>"vaadin-grid-column-group"===t.localName?this._getChildColumns(t):[t]).reduce((t,e)=>t.concat(e),[])}_getColumnTree(){for(var t=[],e=n.getFlattenedNodes(this).filter(this._isColumnElement);t.push(e),this._hasColumnGroups(e);)e=this._flattenColumnGroups(e);return t}_updateColumnTree(){var t=this._getColumnTree();this._arrayEquals(t,this._columnTree)||(this._columnTree=t)}_addNodeObserver(){this._observer=new n(this,t=>{const e=t.addedNodes.filter(t=>"template"===t.localName&&t.classList.contains("row-details"))[0];e&&this._rowDetailsTemplate!==e&&(this._rowDetailsTemplate=e),(t.addedNodes.filter(this._isColumnElement).length>0||t.removedNodes.filter(this._isColumnElement).length>0)&&this._updateColumnTree(),this._debouncerCheckImports=a.debounce(this._debouncerCheckImports,c.after(2e3),this._checkImports.bind(this)),this._ensureFirstPageLoaded()})}_arrayEquals(t,e){if(!t||!e||t.length!=e.length)return!1;for(var i=0,s=t.length;i<s;i++)if(t[i]instanceof Array&&e[i]instanceof Array){if(!this._arrayEquals(t[i],e[i]))return!1}else if(t[i]!=e[i])return!1;return!0}_checkImports(){["vaadin-grid-column-group","vaadin-grid-filter","vaadin-grid-filter-column","vaadin-grid-tree-toggle","vaadin-grid-selection-column","vaadin-grid-sort-column","vaadin-grid-sorter"].forEach(t=>{var e=this.querySelector(t);!e||e instanceof r||console.warn(`Make sure you have imported the required module for <${t}> element.`)})}_updateFirstAndLastColumn(){Array.from(this.shadowRoot.querySelectorAll("tr")).forEach(t=>this._updateFirstAndLastColumnForRow(t))}_updateFirstAndLastColumnForRow(t){Array.from(t.querySelectorAll('[part~="cell"]:not([part~="details-cell"])')).sort((t,e)=>t._column._order-e._column._order).forEach((t,e,i)=>{this._toggleAttribute("first-column",0===e,t),this._toggleAttribute("last-column",e===i.length-1,t)})}_isColumnElement(t){return t.nodeType===Node.ELEMENT_NODE&&/\bcolumn\b/.test(t.localName)}}
/**
@license
Copyright (c) 2018 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Mt=t=>class extends t{getEventContext(t){const e={},i=t.composedPath(),s=i[i.indexOf(this.$.table)-3];return s?(e.section=["body","header","footer","details"].filter(t=>s.getAttribute("part").indexOf(t)>-1)[0],s._column&&(e.column=s._column),"body"!==e.section&&"details"!==e.section||Object.assign(e,this.__getRowModel(s.parentElement)),e):e}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Gt=t=>class extends t{static get properties(){return{_filters:{type:Array,value:function(){return[]}}}}ready(){super.ready(),this.addEventListener("filter-changed",this._filterChanged.bind(this))}_filterChanged(t){-1===this._filters.indexOf(t.target)&&this._filters.push(t.target),t.stopPropagation(),this.dataProvider&&this.clearCache()}_mapFilters(){return this._filters.map(t=>({path:t.path,value:t.value}))}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Dt=t=>class extends t{static get properties(){return{detailsOpenedItems:{type:Array,value:function(){return[]}},_rowDetailsTemplate:Object,rowDetailsRenderer:Function,_detailsCells:{type:Array}}}static get observers(){return["_detailsOpenedItemsChanged(detailsOpenedItems.*, _rowDetailsTemplate, rowDetailsRenderer)","_rowDetailsTemplateOrRendererChanged(_rowDetailsTemplate, rowDetailsRenderer)"]}_rowDetailsTemplateOrRendererChanged(t,e){if(t&&e)throw new Error("You should only use either a renderer or a template for row details");if(t||e){if(t&&!t.templatizer){var i=new E;i._grid=this,i.dataHost=this.dataHost,i.template=t,t.templatizer=i}this._columnTree&&Array.from(this.$.items.children).forEach(t=>{t.querySelector("[part~=details-cell]")||(this._updateRow(t,this._columnTree[this._columnTree.length-1]),this._a11yUpdateRowDetailsOpened(t,!1)),delete t.querySelector("[part~=details-cell]")._instance}),this.detailsOpenedItems.length&&(Array.from(this.$.items.children).forEach(this._toggleDetailsCell,this),this._update())}}_detailsOpenedItemsChanged(t,e,i){"detailsOpenedItems.length"!==t.path&&t.value&&Array.from(this.$.items.children).forEach(t=>{this._toggleDetailsCell(t,t._item),this._a11yUpdateRowDetailsOpened(t,this._isDetailsOpened(t._item)),this._toggleAttribute("details-opened",this._isDetailsOpened(t._item),t)})}_configureDetailsCell(t){t.setAttribute("part","cell details-cell"),this._toggleAttribute("frozen",!0,t)}_toggleDetailsCell(t,e){const i=t.querySelector('[part~="details-cell"]');if(!i)return;const s=!this._isDetailsOpened(e),r=!!i.hidden!==s;(i._instance||i._renderer)&&i.hidden===s||(i.hidden=s,s?t.style.removeProperty("padding-bottom"):(this.rowDetailsRenderer?(i._renderer=this.rowDetailsRenderer,i._renderer.call(this,i._content,this,{index:t.index,item:e})):this._rowDetailsTemplate&&!i._instance&&(i._instance=this._rowDetailsTemplate.templatizer.createInstance(),i._content.innerHTML="",i._content.appendChild(i._instance.root),this._updateItem(t,e)),A(),t.style.setProperty("padding-bottom",`${i.offsetHeight}px`),requestAnimationFrame(()=>this.notifyResize()))),r&&(this._updateMetrics(),this._positionItems())}_updateDetailsCellHeights(){Array.from(this.$.items.querySelectorAll('[part~="details-cell"]:not([hidden])')).forEach(t=>{t.parentElement.style.setProperty("padding-bottom",`${t.offsetHeight}px`)})}_isDetailsOpened(t){return this.detailsOpenedItems&&-1!==this._getItemIndexInArray(t,this.detailsOpenedItems)}openItemDetails(t){this._isDetailsOpened(t)||this.push("detailsOpenedItems",t)}closeItemDetails(t){this._isDetailsOpened(t)&&this.splice("detailsOpenedItems",this._getItemIndexInArray(t,this.detailsOpenedItems),1)}_detailsOpenedInstanceChangedCallback(t,e){super._detailsOpenedInstanceChangedCallback&&super._detailsOpenedInstanceChangedCallback(t,e),e?this.openItemDetails(t.item):this.closeItemDetails(t.item)}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Ot=t=>class extends t{get _timeouts(){return{SCROLL_PERIOD:1e3,WHEEL_SCROLLING:200,SCROLLING:100,IGNORE_WHEEL:500}}static get properties(){return{_frozenCells:{type:Array,value:function(){return[]}},_scrollbarWidth:{type:Number,value:function(){var t=document.createElement("div");t.style.width="100px",t.style.height="100px",t.style.overflow="scroll",t.style.position="absolute",t.style.top="-9999px",document.body.appendChild(t);var e=t.offsetWidth-t.clientWidth;return document.body.removeChild(t),e}},_rowWithFocusedElement:Element,_deltaYAcc:{type:Number,value:0}}}static get observers(){return["_scrollHeightUpdated(_estScrollHeight)","_scrollViewportHeightUpdated(_viewportHeight)"]}set _scrollTop(t){this.$.table.scrollTop=t}get _scrollTop(){return this.$.table.scrollTop}constructor(){super(),this._scrollLineHeight=this._getScrollLineHeight()}_getScrollLineHeight(){const t=document.createElement("div");t.style.fontSize="initial",t.style.display="none",document.body.appendChild(t);const e=window.getComputedStyle(t).fontSize;return document.body.removeChild(t),e?window.parseInt(e):void 0}_scrollViewportHeightUpdated(t){this._scrollPageHeight=t-this.$.header.clientHeight-this.$.footer.clientHeight-this._scrollLineHeight}ready(){super.ready(),this.scrollTarget=this.$.table,this.addEventListener("wheel",t=>{this._wheelScrolling=!0,this._debouncerWheelScrolling=a.debounce(this._debouncerWheelScrolling,c.after(this._timeouts.WHEEL_SCROLLING),()=>this._wheelScrolling=!1),this._onWheel(t)}),this.$.table.addEventListener("scroll",t=>{this.$.outerscroller.outerScrolling&&t.stopImmediatePropagation()},!0),this.$.items.addEventListener("focusin",t=>{const e=t.composedPath().indexOf(this.$.items);this._rowWithFocusedElement=t.composedPath()[e-1]}),this.$.items.addEventListener("focusout",()=>this._rowWithFocusedElement=void 0)}scrollToIndex(t){this._accessIronListAPI(()=>super.scrollToIndex(t))}_onWheel(t){if(t.ctrlKey||this._hasScrolledAncestor(t.target,t.deltaX,t.deltaY))return;const e=this.$.table;let i=t.deltaY;if(t.deltaMode===WheelEvent.DOM_DELTA_LINE?i*=this._scrollLineHeight:t.deltaMode===WheelEvent.DOM_DELTA_PAGE&&(i*=this._scrollPageHeight),this._wheelAnimationFrame)return this._deltaYAcc+=i,void t.preventDefault();i+=this._deltaYAcc,this._deltaYAcc=0,this._wheelAnimationFrame=!0,this._debouncerWheelAnimationFrame=a.debounce(this._debouncerWheelAnimationFrame,l,()=>this._wheelAnimationFrame=!1);var s=Math.abs(t.deltaX)+Math.abs(i);this._canScroll(e,t.deltaX,i)?(t.preventDefault(),e.scrollTop+=i,e.scrollLeft+=t.deltaX,this._scrollHandler(),this._hasResidualMomentum=!0,this._ignoreNewWheel=!0,this._debouncerIgnoreNewWheel=a.debounce(this._debouncerIgnoreNewWheel,c.after(this._timeouts.IGNORE_WHEEL),()=>this._ignoreNewWheel=!1)):this._hasResidualMomentum&&s<=this._previousMomentum||this._ignoreNewWheel?t.preventDefault():s>this._previousMomentum&&(this._hasResidualMomentum=!1),this._previousMomentum=s}_hasScrolledAncestor(t,e,i){return"vaadin-grid-cell-content"!==t.localName&&(!(!this._canScroll(t,e,i)||-1===["auto","scroll"].indexOf(getComputedStyle(t).overflow))||(t!==this&&t.parentElement?this._hasScrolledAncestor(t.parentElement,e,i):void 0))}_canScroll(t,e,i){return i>0&&t.scrollTop<t.scrollHeight-t.offsetHeight||i<0&&t.scrollTop>0||e>0&&t.scrollLeft<t.scrollWidth-t.offsetWidth||e<0&&t.scrollLeft>0}_scheduleScrolling(){this._scrollingFrame||(this._scrollingFrame=requestAnimationFrame(()=>this._toggleAttribute("scrolling",!0,this.$.scroller))),this._debounceScrolling=a.debounce(this._debounceScrolling,c.after(this._timeouts.SCROLLING),()=>{cancelAnimationFrame(this._scrollingFrame),delete this._scrollingFrame,this._toggleAttribute("scrolling",!1,this.$.scroller),this.$.outerscroller.outerScrolling||this._reorderRows()}),this._scrollPeriodFrame||(this._scrollPeriodFrame=requestAnimationFrame(()=>this._toggleAttribute("scroll-period",!0,this.$.scroller))),this._debounceScrollPeriod=a.debounce(this._debounceScrollPeriod,c.after(this._timeouts.SCROLL_PERIOD),()=>{cancelAnimationFrame(this._scrollPeriodFrame),delete this._scrollPeriodFrame,this._toggleAttribute("scroll-period",!1,this.$.scroller)})}_afterScroll(){this._translateStationaryElements(),this.hasAttribute("reordering")||this._scheduleScrolling();const t=this.$.outerscroller;if(this._ios||!this._wheelScrolling&&!t.passthrough||t.syncOuterScroller(),this._ios){const e=Math.max(-t.scrollTop,0)||Math.min(0,t.scrollHeight-t.scrollTop-t.offsetHeight);this.$.items.style.transform=`translateY(${e}px)`}this._updateOverflow()}_updateOverflow(){let t="";const e=this.$.table;e.scrollTop<e.scrollHeight-e.clientHeight&&(t+=" bottom"),e.scrollTop>0&&(t+=" top"),e.scrollLeft<e.scrollWidth-e.clientWidth&&(t+=" right"),e.scrollLeft>0&&(t+=" left"),this._debounceOverflow=a.debounce(this._debounceOverflow,l,()=>{const e=t.trim();e.length>0&&this.getAttribute("overflow")!==e?this.setAttribute("overflow",e):0==e.length&&this.hasAttribute("overflow")&&this.removeAttribute("overflow")})}_reorderRows(){const t=this.$.items,e=t.querySelectorAll("tr");if(!e.length)return;const i=this._virtualStart+this._vidxOffset,s=this._rowWithFocusedElement||Array.from(e).filter(t=>!t.hidden)[0];if(!s)return;const r=s.index-i,o=Array.from(e).indexOf(s)-r;if(o>0)for(let n=0;n<o;n++)t.appendChild(e[n]);else if(o<0)for(let n=e.length+o;n<e.length;n++)t.insertBefore(e[n],e[0])}_frozenCellsChanged(){this._debouncerCacheElements=a.debounce(this._debouncerCacheElements,h,()=>{Array.from(this.shadowRoot.querySelectorAll('[part~="cell"]')).forEach((function(t){t.style.transform=""})),this._frozenCells=Array.prototype.slice.call(this.$.table.querySelectorAll("[frozen]")),this._updateScrollerMeasurements(),this._translateStationaryElements()}),this._updateLastFrozen()}_updateScrollerMeasurements(){this._frozenCells.length>0&&this.__isRTL&&(this.__scrollerMetrics={scrollWidth:this.$.outerscroller.scrollWidth,clientWidth:this.$.outerscroller.clientWidth})}_updateLastFrozen(){if(!this._columnTree)return;const t=this._columnTree[this._columnTree.length-1].slice(0);t.sort((t,e)=>t._order-e._order);const e=t.reduce((t,e,i)=>(e._lastFrozen=!1,e.frozen&&!e.hidden?i:t),void 0);void 0!==e&&(t[e]._lastFrozen=!0)}_translateStationaryElements(){if(this._edge&&!this._scrollbarWidth?(this.$.items.style.transform=this._getTranslate(-this._scrollLeft||0,-this._scrollTop||0),this.$.footer.style.transform=this.$.header.style.transform=this._getTranslate(-this._scrollLeft||0,0)):this.$.footer.style.transform=this.$.header.style.transform=this._getTranslate(0,this._scrollTop),this._frozenCells.length>0){const i=this.__isRTL?this.__getNormalizedScrollLeft(this.$.table)+this.__scrollerMetrics.clientWidth-this.__scrollerMetrics.scrollWidth:this._scrollLeft;for(var t=this._getTranslate(i,0),e=0;e<this._frozenCells.length;e++)this._frozenCells[e].style.transform=t}}_getTranslate(t,e){return"translate("+t+"px,"+e+"px)"}_scrollHeightUpdated(t){this.$.outersizer.style.top=this.$.fixedsizer.style.top=t+"px"}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Rt=t=>class extends t{static get properties(){return{selectedItems:{type:Object,notify:!0,value:()=>[]}}}static get observers(){return["_selectedItemsChanged(selectedItems.*)"]}_isSelected(t){return this.selectedItems&&this._getItemIndexInArray(t,this.selectedItems)>-1}selectItem(t){this._isSelected(t)||this.push("selectedItems",t)}deselectItem(t){const e=this._getItemIndexInArray(t,this.selectedItems);e>-1&&this.splice("selectedItems",e,1)}_toggleItem(t){-1===this._getItemIndexInArray(t,this.selectedItems)?this.selectItem(t):this.deselectItem(t)}_selectedItemsChanged(t){!this.$.items.children.length||"selectedItems"!==t.path&&"selectedItems.splices"!==t.path||Array.from(this.$.items.children).forEach(t=>{this._updateItem(t,t._item)})}_selectedInstanceChangedCallback(t,e){super._selectedInstanceChangedCallback&&super._selectedInstanceChangedCallback(t,e),e?this.selectItem(t.item):this.deselectItem(t.item)}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Ht=t=>class extends t{static get properties(){return{multiSort:{type:Boolean,value:!1},_sorters:{type:Array,value:function(){return[]}},_previousSorters:{type:Array,value:function(){return[]}}}}ready(){super.ready(),this.addEventListener("sorter-changed",this._onSorterChanged),window.ShadyDOM&&h.run(()=>{const t=this.querySelectorAll("vaadin-grid-sorter");Array.from(t).forEach(t=>{t instanceof r&&t.dispatchEvent(new CustomEvent("sorter-changed",{bubbles:!0,composed:!0}))})})}_onSorterChanged(t){const e=t.target;this._removeArrayItem(this._sorters,e),e._order=null,this.multiSort?(e.direction&&this._sorters.unshift(e),this._sorters.forEach((t,e)=>t._order=this._sorters.length>1?e:null,this)):e.direction&&(this._sorters.forEach(t=>{t._order=null,t.direction=null}),this._sorters=[e]),t.stopPropagation(),this.dataProvider&&JSON.stringify(this._previousSorters)!==JSON.stringify(this._mapSorters())&&this.clearCache(),this._a11yUpdateSorters(),this._previousSorters=this._mapSorters()}_mapSorters(){return this._sorters.map(t=>({path:t.path,direction:t.direction}))}_removeArrayItem(t,e){const i=t.indexOf(e);i>-1&&t.splice(i,1)}}
/**
@license
Copyright (c) 2018 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Nt=t=>class extends t{static get properties(){return{cellClassNameGenerator:Function}}static get observers(){return["__cellClassNameGeneratorChanged(cellClassNameGenerator)"]}__cellClassNameGeneratorChanged(t){this.generateCellClassNames()}generateCellClassNames(){Array.from(this.$.items.children).filter(t=>!t.hidden).forEach(t=>this._generateCellClassNames(t,this.__getRowModel(t)))}_generateCellClassNames(t,e){Array.from(t.children).forEach(t=>{if(t.__generatedClasses&&t.__generatedClasses.forEach(e=>t.classList.remove(e)),this.cellClassNameGenerator){const i=this.cellClassNameGenerator(t._column,e);t.__generatedClasses=i&&i.split(" ").filter(t=>t.length>0),t.__generatedClasses&&t.__generatedClasses.forEach(e=>t.classList.add(e))}})}}
/**
@license
Copyright (c) 2019 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Pt=t=>class extends t{static get properties(){return{dropMode:String,rowsDraggable:Boolean,dragFilter:Function,dropFilter:Function,__dndAutoScrollThreshold:{value:50}}}static get observers(){return["_dragDropAccessChanged(rowsDraggable, dropMode, dragFilter, dropFilter)"]}ready(){super.ready(),this.$.table.addEventListener("dragstart",this._onDragStart.bind(this)),this.$.table.addEventListener("dragend",this._onDragEnd.bind(this)),this.$.table.addEventListener("dragover",this._onDragOver.bind(this)),this.$.table.addEventListener("dragleave",this._onDragLeave.bind(this)),this.$.table.addEventListener("drop",this._onDrop.bind(this)),this.$.table.addEventListener("dragenter",t=>{this.dropMode&&(t.preventDefault(),t.stopPropagation())})}_onDragStart(t){if(this.rowsDraggable){let e=t.target;if("vaadin-grid-cell-content"===e.localName&&(e=e.assignedSlot.parentNode.parentNode),e.parentNode!==this.$.items)return;if(t.stopPropagation(),this._toggleAttribute("dragging-rows",!0,this),this._safari){const t=e.style.transform;e.style.top=/translateY\((.*)\)/.exec(t)[1],e.style.transform="none",requestAnimationFrame(()=>{e.style.top="",e.style.transform=t})}const i=e.getBoundingClientRect();window.ShadyDOM||(this._ios?t.dataTransfer.setDragImage(e):t.dataTransfer.setDragImage(e,t.clientX-i.left,t.clientY-i.top));let s=[e];this._isSelected(e._item)&&(s=this.__getViewportRows().filter(t=>this._isSelected(t._item)).filter(t=>!this.dragFilter||this.dragFilter(this.__getRowModel(t)))),t.dataTransfer.setData("text",this.__formatDefaultTransferData(s)),e.setAttribute("dragstart",s.length>1?s.length:""),this.updateStyles({"--_grid-drag-start-x":`${t.clientX-i.left+20}px`,"--_grid-drag-start-y":`${t.clientY-i.top+10}px`}),requestAnimationFrame(()=>{e.removeAttribute("dragstart"),this.updateStyles({"--_grid-drag-start-x":"","--_grid-drag-start-y":""})});const r=new CustomEvent("grid-dragstart",{detail:{draggedItems:s.map(t=>t._item),setDragData:(e,i)=>t.dataTransfer.setData(e,i),setDraggedItemsCount:t=>e.setAttribute("dragstart",t)}});r.originalEvent=t,this.dispatchEvent(r)}}_onDragEnd(t){this._toggleAttribute("dragging-rows",!1,this),t.stopPropagation();const e=new CustomEvent("grid-dragend");e.originalEvent=t,this.dispatchEvent(e)}_onDragLeave(t){t.stopPropagation(),this._clearDragStyles()}_onDragOver(t){if(this.dropMode){if(this._dropLocation=void 0,this._dragOverItem=void 0,this.__dndAutoScroll(t.clientY))return void this._clearDragStyles();let e=t.composedPath().filter(t=>"tr"===t.localName)[0];if(this._effectiveSize&&"on-grid"!==this.dropMode)if(e&&e.parentNode===this.$.items){const i=e.getBoundingClientRect();this._dropLocation="on-top","between"===this.dropMode?this._dropLocation=t.clientY-i.top<i.bottom-t.clientY?"above":"below":"on-top-or-between"===this.dropMode&&(t.clientY-i.top<i.height/3?this._dropLocation="above":t.clientY-i.top>i.height/3*2&&(this._dropLocation="below"))}else{if(e)return;if("between"!==this.dropMode&&"on-top-or-between"!==this.dropMode)return;e=Array.from(this.$.items.children).filter(t=>!t.hidden).pop(),this._dropLocation="below"}else this._dropLocation="empty";if(e&&e.hasAttribute("drop-disabled"))return void(this._dropLocation=void 0);t.stopPropagation(),t.preventDefault(),"empty"===this._dropLocation?this._toggleAttribute("dragover",!0,this):e?(this._dragOverItem=e._item,e.getAttribute("dragover")!==this._dropLocation&&e.setAttribute("dragover",this._dropLocation)):this._clearDragStyles()}}__dndAutoScroll(t){if(this.__dndAutoScrolling)return!0;const e=this.$.header.getBoundingClientRect().bottom,i=this.$.footer.getBoundingClientRect().top,s=e-t+this.__dndAutoScrollThreshold,r=t-i+this.__dndAutoScrollThreshold;let o=0;if(r>0?o=2*r:s>0&&(o=2*-s),o){const t=this.$.table.scrollTop;if(this.$.table.scrollTop+=o,t!==this.$.table.scrollTop)return this.__dndAutoScrolling=!0,setTimeout(()=>this.__dndAutoScrolling=!1,20),this._scrollHandler(),!0}}__getViewportRows(){const t=this.$.header.getBoundingClientRect().bottom,e=this.$.footer.getBoundingClientRect().top;return Array.from(this.$.items.children).filter(i=>{const s=i.getBoundingClientRect();return s.bottom>t&&s.top<e})}_clearDragStyles(){this.removeAttribute("dragover"),Array.from(this.$.items.children).forEach(t=>t.removeAttribute("dragover"))}_onDrop(t){if(this.dropMode){t.stopPropagation(),t.preventDefault();const e=t.dataTransfer.types&&Array.from(t.dataTransfer.types).map(e=>({type:e,data:t.dataTransfer.getData(e)}));this._clearDragStyles();const i=new CustomEvent("grid-drop",{bubbles:t.bubbles,cancelable:t.cancelable,detail:{dropTargetItem:this._dragOverItem,dropLocation:this._dropLocation,dragData:e}});i.originalEvent=t,this.dispatchEvent(i)}}__formatDefaultTransferData(t){return t.map(t=>Array.from(t.children).filter(t=>!t.hidden&&-1===t.getAttribute("part").indexOf("details-cell")).sort((t,e)=>t._column._order>e._column._order?1:-1).map(t=>t._content.textContent.trim()).filter(t=>t).join("\t")).join("\n")}_dragDropAccessChanged(t,e,i,s){this.filterDragAndDrop()}filterDragAndDrop(){Array.from(this.$.items.children).filter(t=>!t.hidden).forEach(t=>{this._filterDragAndDrop(t,this.__getRowModel(t))})}_filterDragAndDrop(t,e){const i=!this.rowsDraggable||this.dragFilter&&!this.dragFilter(e),s=!this.dropMode||this.dropFilter&&!this.dropFilter(e);(window.ShadyDOM?[t]:Array.from(t.children).map(t=>t._content)).forEach(t=>{i?t.removeAttribute("draggable"):t.setAttribute("draggable",!0)}),this._toggleAttribute("drag-disabled",i,t),this._toggleAttribute("drop-disabled",s,t)}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,Yt=t=>class extends t{static get properties(){return{_headerFocusable:{type:Object,observer:"_focusableChanged"},_itemsFocusable:{type:Object,observer:"_focusableChanged"},_footerFocusable:{type:Object,observer:"_focusableChanged"},_navigatingIsHidden:Boolean,_focusedItemIndex:{type:Number,value:0},_focusedColumnOrder:Number}}ready(){super.ready(),this._ios||this._android||(this.addEventListener("keydown",this._onKeyDown),this.addEventListener("keyup",this._onKeyUp),this.addEventListener("focusin",this._onFocusIn),this.addEventListener("focusout",this._onFocusOut),this.$.table.addEventListener("focusin",this._onCellFocusIn.bind(this)),this.$.table.addEventListener("focusout",this._onCellFocusOut.bind(this)),this.addEventListener("mousedown",()=>{this._toggleAttribute("navigating",!1,this),this._isMousedown=!0}),this.addEventListener("mouseup",()=>this._isMousedown=!1))}_focusableChanged(t,e){e&&e.setAttribute("tabindex","-1"),t&&t.setAttribute("tabindex","0")}_onKeyDown(t){let e,i=t.key;switch("Up"!==i&&"Down"!==i&&"Left"!==i&&"Right"!==i||(i="Arrow"+i),"Esc"===i&&(i="Escape"),"Spacebar"===i&&(i=" "),i){case"ArrowUp":case"ArrowDown":case"ArrowLeft":case"ArrowRight":case"PageUp":case"PageDown":case"Home":case"End":e="Navigation";break;case"Enter":case"Escape":case"F2":e="Interaction";break;case"Tab":e="Tab";break;case" ":e="Space"}this._detectInteracting(t),this.hasAttribute("interacting")&&"Interaction"!==e&&(e=void 0),e&&this[`_on${e}KeyDown`](t,i)}_ensureScrolledToIndex(t){Array.from(this.$.items.children).filter(e=>e.index===t)[0]||this._scrollToIndex(t)}_onNavigationKeyDown(t,e){function i(t){return Array.prototype.indexOf.call(t.parentNode.children,t)}t.preventDefault();const s=this._lastVisibleIndex-this._firstVisibleIndex-1;let r=0,o=0;switch(e){case"ArrowRight":r=this.__isRTL?-1:1;break;case"ArrowLeft":r=this.__isRTL?1:-1;break;case"Home":r=-1/0,t.ctrlKey&&(o=-1/0);break;case"End":r=1/0,t.ctrlKey&&(o=1/0);break;case"ArrowDown":o=1;break;case"ArrowUp":o=-1;break;case"PageDown":o=s;break;case"PageUp":o=-s}const n=t.composedPath()[0],a=i(n),l=this._elementMatches(n,'[part~="details-cell"]'),h=n.parentNode,d=h.parentNode,c=(d===this.$.items?this._effectiveSize:d.children.length)-1,u=d===this.$.items?void 0!==this._focusedItemIndex?this._focusedItemIndex:h.index:i(h);let p=Math.max(0,Math.min(u+o,c)),m=!1;if(d===this.$.items){const t=h._item,e=this._cache.getItemForIndex(p);m=l?0===o:1===o&&this._isDetailsOpened(t)||-1===o&&p!==u&&this._isDetailsOpened(e),m!==l&&(1===o&&m||-1===o&&!m)&&(p=u)}if(d!==this.$.items)if(p>u)for(;p<c&&d.children[p].hidden;)p++;else if(p<u)for(;p>0&&d.children[p].hidden;)p--;void 0===this._focusedColumnOrder&&(this._focusedColumnOrder=l?0:this._getColumns(d,u).filter(t=>!t.hidden)[a]._order);const g=this._getColumns(d,p).filter(t=>!t.hidden),f=g.map(t=>t._order).sort((t,e)=>t-e),v=f.length-1,b=f.indexOf(f.slice(0).sort((t,e)=>Math.abs(t-this._focusedColumnOrder)-Math.abs(e-this._focusedColumnOrder))[0]),A=0===o&&l?b:Math.max(0,Math.min(b+r,v));A!==b&&(this._focusedColumnOrder=void 0),d===this.$.items&&this._ensureScrolledToIndex(p),this._toggleAttribute("navigating",!0,this);const x=g.reduce((t,e,i)=>(t[e._order]=i,t),{})[f[A]],y=d===this.$.items?Array.from(d.children).filter(t=>t.index===p)[0]:d.children[p];if(!y)return;const w=m?Array.from(y.children).filter(t=>this._elementMatches(t,'[part~="details-cell"]'))[0]:y.children[x];if(this._scrollHorizontallyToCell(w),d===this.$.items&&(this._focusedItemIndex=p),d===this.$.items){const t=w.getBoundingClientRect(),e=this.$.footer.getBoundingClientRect().top,i=this.$.header.getBoundingClientRect().bottom;t.bottom>e?(this.$.table.scrollTop+=t.bottom-e,this._scrollHandler()):t.top<i&&(this.$.table.scrollTop-=i-t.top,this._scrollHandler())}w.focus()}_parseEventPath(t){const e=t.indexOf(this.$.table);return{rowGroup:t[e-1],row:t[e-2],cell:t[e-3]}}_onInteractionKeyDown(t,e){const i=t.composedPath()[0],s="input"===i.localName&&!/^(button|checkbox|color|file|image|radio|range|reset|submit)$/i.test(i.type);let r;switch(e){case"Enter":r=!this.hasAttribute("interacting")||!s;break;case"Escape":r=!1;break;case"F2":r=!this.hasAttribute("interacting")}const{cell:o}=this._parseEventPath(t.composedPath());if(this.hasAttribute("interacting")!==r)if(r){const e=o._content.querySelector("[focus-target]")||o._content.firstElementChild;e&&(t.preventDefault(),e.focus(),this._toggleAttribute("interacting",!0,this),this._toggleAttribute("navigating",!1,this))}else t.preventDefault(),this._focusedColumnOrder=void 0,o.focus(),this._toggleAttribute("interacting",!1,this),this._toggleAttribute("navigating",!0,this)}_predictFocusStepTarget(t,e){const i=[this.$.table,this._headerFocusable,this._itemsFocusable,this._footerFocusable,this.$.focusexit];let s=i.indexOf(t);for(s+=e;s>=0&&s<=i.length-1&&(!i[s]||i[s].parentNode.hidden);)s+=e;return i[s]}_onTabKeyDown(t){const e=this._predictFocusStepTarget(t.composedPath()[0],t.shiftKey?-1:1);if(e===this.$.table)this.$.table.focus();else if(e===this.$.focusexit)this.$.focusexit.focus();else if(e===this._itemsFocusable){let i=e;const s=this._itemsFocusable.parentNode;if(this._ensureScrolledToIndex(this._focusedItemIndex),s.index!==this._focusedItemIndex){const t=Array.from(s.children).indexOf(this._itemsFocusable),e=Array.from(this.$.items.children).filter(t=>t.index===this._focusedItemIndex)[0];e&&(i=e.children[t])}t.preventDefault(),i.focus()}else t.preventDefault(),e.focus();this._toggleAttribute("navigating",!0,this)}_onSpaceKeyDown(t){t.preventDefault();const e=t.composedPath()[0];e._content&&e._content.firstElementChild||this.dispatchEvent(new CustomEvent("cell-activate",{detail:{model:this.__getRowModel(e.parentElement)}}))}_onKeyUp(t){if(!/^( |SpaceBar)$/.test(t.key))return;t.preventDefault();const e=t.composedPath()[0];if(e._content&&e._content.firstElementChild){const t=this.hasAttribute("navigating");e._content.firstElementChild.click(),this._toggleAttribute("navigating",t,this)}}_onFocusIn(t){this._isMousedown||this._toggleAttribute("navigating",!0,this);const e=t.composedPath()[0];e===this.$.table||e===this.$.focusexit?(this._predictFocusStepTarget(e,e===this.$.table?1:-1).focus(),this._toggleAttribute("interacting",!1,this)):this._detectInteracting(t)}_onFocusOut(t){this._toggleAttribute("navigating",!1,this),this._detectInteracting(t)}_onCellFocusIn(t){if(this._detectInteracting(t),3===t.composedPath().indexOf(this.$.table)){const e=t.composedPath()[0];this._activeRowGroup=e.parentNode.parentNode,this._activeRowGroup===this.$.header?this._headerFocusable=e:this._activeRowGroup===this.$.items?this._itemsFocusable=e:this._activeRowGroup===this.$.footer&&(this._footerFocusable=e),e._content.dispatchEvent(new CustomEvent("cell-focusin",{bubbles:!1}))}this._detectFocusedItemIndex(t)}_onCellFocusOut(t){3===t.composedPath().indexOf(this.$.table)&&t.composedPath()[0]._content.dispatchEvent(new CustomEvent("cell-focusout",{bubbles:!1}))}_detectInteracting(t){this._toggleAttribute("interacting",t.composedPath().some(t=>"vaadin-grid-cell-content"===t.localName),this)}_detectFocusedItemIndex(t){const{rowGroup:e,row:i}=this._parseEventPath(t.composedPath());e===this.$.items&&(this._focusedItemIndex=i.index)}_preventScrollerRotatingCellFocus(t,e){t.index===this._focusedItemIndex&&this.hasAttribute("navigating")&&this._activeRowGroup===this.$.items&&(this._navigatingIsHidden=!0,this._toggleAttribute("navigating",!1,this)),e===this._focusedItemIndex&&this._navigatingIsHidden&&(this._navigatingIsHidden=!1,this._toggleAttribute("navigating",!0,this))}_getColumns(t,e){let i=this._columnTree.length-1;return t===this.$.header?i=e:t===this.$.footer&&(i=this._columnTree.length-1-e),this._columnTree[i]}_resetKeyboardNavigation(){if(this.$.header.firstElementChild&&(this._headerFocusable=Array.from(this.$.header.firstElementChild.children).filter(t=>!t.hidden)[0]),this.$.items.firstElementChild){const t=this._iterateItems((t,e)=>{if(this._firstVisibleIndex===e)return this.$.items.children[t]});t&&(this._itemsFocusable=Array.from(t.children).filter(t=>!t.hidden)[0])}this.$.footer.firstElementChild&&(this._footerFocusable=Array.from(this.$.footer.firstElementChild.children).filter(t=>!t.hidden)[0])}_scrollHorizontallyToCell(t){if(t.hasAttribute("frozen")||this._elementMatches(t,'[part~="details-cell"]'))return;const e=t.getBoundingClientRect(),i=t.parentNode,s=Array.from(i.children).indexOf(t),r=this.$.table.getBoundingClientRect();let o=r.left,n=r.right;for(let a=s-1;a>=0;a--){const t=i.children[a];if(!t.hasAttribute("hidden")&&!this._elementMatches(t,'[part~="details-cell"]')&&t.hasAttribute("frozen")){o=t.getBoundingClientRect().right;break}}for(let a=s+1;a<i.children.length;a++){const t=i.children[a];if(!t.hasAttribute("hidden")&&!this._elementMatches(t,'[part~="details-cell"]')&&t.hasAttribute("frozen")){n=t.getBoundingClientRect().left;break}}e.left<o&&(this.$.table.scrollLeft+=Math.round(e.left-o)),e.right>n&&(this.$.table.scrollLeft+=Math.round(e.right-n))}_elementMatches(t,e){return t.matches?t.matches(e):-1!==Array.from(t.parentNode.querySelectorAll(e)).indexOf(t)}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/,jt=t=>class extends(g(t)){static get properties(){return{columnReorderingAllowed:{type:Boolean,value:!1},_orderBaseScope:{type:Number,value:1e7}}}static get observers(){return["_updateOrders(_columnTree, _columnTree.*)"]}ready(){super.ready(),w(this,"track",this._onTrackEvent),this._reorderGhost=this.shadowRoot.querySelector('[part="reorder-ghost"]'),this.addEventListener("touchstart",this._onTouchStart.bind(this)),this.addEventListener("touchmove",this._onTouchMove.bind(this)),this.addEventListener("touchend",this._onTouchEnd.bind(this)),this.addEventListener("contextmenu",this._onContextMenu.bind(this))}_onContextMenu(t){this.hasAttribute("reordering")&&t.preventDefault()}_onTouchStart(t){this._startTouchReorderTimeout=setTimeout(()=>{this._onTrackStart({detail:{x:t.touches[0].clientX,y:t.touches[0].clientY}})},100)}_onTouchMove(t){this._draggedColumn&&t.preventDefault(),clearTimeout(this._startTouchReorderTimeout)}_onTouchEnd(){clearTimeout(this._startTouchReorderTimeout),this._onTrackEnd()}_onTrackEvent(t){if("start"===t.detail.state){const e=t.composedPath(),i=e[e.indexOf(this.$.header)-2];if(!i||!i._content)return;const s=this.getRootNode().activeElement;if(i._content.contains(this.getRootNode().activeElement)&&(!this._ie||!this._isFocusable(s)))return;if(this.$.scroller.hasAttribute("column-resizing"))return;this._touchDevice||this._onTrackStart(t)}else"track"===t.detail.state?this._onTrack(t):"end"===t.detail.state&&this._onTrackEnd(t)}_onTrackStart(t){if(!this.columnReorderingAllowed)return;const e=t.path||f(t).path;if(e&&e.filter(t=>t.hasAttribute&&t.hasAttribute("draggable"))[0])return;const i=this._cellFromPoint(t.detail.x,t.detail.y);if(i&&-1!==i.getAttribute("part").indexOf("header-cell")){for(this._toggleAttribute("reordering",!0,this),this._draggedColumn=i._column;1===this._draggedColumn.parentElement.childElementCount;)this._draggedColumn=this._draggedColumn.parentElement;this._setSiblingsReorderStatus(this._draggedColumn,"allowed"),this._draggedColumn._reorderStatus="dragging",this._updateGhost(i),this._reorderGhost.style.visibility="visible",this._updateGhostPosition(t.detail.x,this._touchDevice?t.detail.y-50:t.detail.y),this._autoScroller()}}_onTrack(t){if(!this._draggedColumn)return;const e=this._cellFromPoint(t.detail.x,t.detail.y);if(!e)return;const i=this._getTargetColumn(e,this._draggedColumn);this._isSwapAllowed(this._draggedColumn,i)&&this._isSwappableByPosition(i,t.detail.x)&&this._swapColumnOrders(this._draggedColumn,i),this._updateGhostPosition(t.detail.x,this._touchDevice?t.detail.y-50:t.detail.y),this._lastDragClientX=t.detail.x}_onTrackEnd(){this._draggedColumn&&(this._toggleAttribute("reordering",!1,this),this._draggedColumn._reorderStatus="",this._setSiblingsReorderStatus(this._draggedColumn,""),this._draggedColumn=null,this._lastDragClientX=null,this._reorderGhost.style.visibility="hidden",this.dispatchEvent(new CustomEvent("column-reorder",{detail:{columns:this._getColumnsInOrder()}})))}_getColumnsInOrder(){return this._columnTree.slice(0).pop().filter(t=>!t.hidden).sort((t,e)=>t._order-e._order)}_cellFromPoint(t,e){let i;if(t=t||0,e=e||0,this._draggedColumn||this._toggleAttribute("no-content-pointer-events",!0,this.$.scroller),_?i=this.shadowRoot.elementFromPoint(t,e):(i=document.elementFromPoint(t,e),"vaadin-grid-cell-content"===i.localName&&(i=i.assignedSlot.parentNode)),this._toggleAttribute("no-content-pointer-events",!1,this.$.scroller),i&&i._column)return i}_updateGhostPosition(t,e){const i=this._reorderGhost.getBoundingClientRect(),s=t-i.width/2,r=e-i.height/2,o=parseInt(this._reorderGhost._left||0),n=parseInt(this._reorderGhost._top||0);this._reorderGhost._left=o-(i.left-s),this._reorderGhost._top=n-(i.top-r),this._reorderGhost.style.transform=`translate(${this._reorderGhost._left}px, ${this._reorderGhost._top}px)`}_getInnerText(t){return t.localName?"none"===getComputedStyle(t).display?"":Array.from(t.childNodes).map(t=>this._getInnerText(t)).join(""):t.textContent}_updateGhost(t){const e=this._reorderGhost;e.textContent=this._getInnerText(t._content);const i=window.getComputedStyle(t);return["boxSizing","display","width","height","background","alignItems","padding","border","flex-direction","overflow"].forEach(t=>e.style[t]=i[t]),e}_updateOrders(t,e){void 0!==t&&void 0!==e&&(t[0].forEach(t=>t._order=0),t[0].forEach((t,e)=>t._order=(e+1)*this._orderBaseScope))}_setSiblingsReorderStatus(t,e){Array.from(t.parentNode.children).filter(e=>/column/.test(e.localName)&&this._isSwapAllowed(e,t)).forEach(t=>t._reorderStatus=e)}_autoScroller(){if(this._lastDragClientX){const t=this._lastDragClientX-this.getBoundingClientRect().right+50,e=this.getBoundingClientRect().left-this._lastDragClientX+50;t>0?this.$.table.scrollLeft+=t/10:e>0&&(this.$.table.scrollLeft-=e/10),this._scrollHandler()}this._draggedColumn&&this.async(this._autoScroller,10)}_isSwapAllowed(t,e){if(t&&e){const i=t.parentElement===e.parentElement,s=t.frozen===e.frozen;return t!==e&&i&&s}}_isSwappableByPosition(t,e){const i=Array.from(this.$.header.querySelectorAll('tr:not([hidden]) [part~="cell"]')).filter(e=>t.contains(e._column))[0],s=this.$.header.querySelector("tr:not([hidden]) [reorder-status=dragging]").getBoundingClientRect(),r=i.getBoundingClientRect();return r.left>s.left?e>r.right-s.width:e<r.left+s.width}_swapColumnOrders(t,e){const i=t._order;t._order=e._order,e._order=i,this._updateLastFrozen(),this._updateFirstAndLastColumn()}_getTargetColumn(t,e){if(t&&e){let i=t._column;for(;i.parentElement!==e.parentElement&&i!==this;)i=i.parentElement;return i.parentElement===e.parentElement?i:t._column}}}
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/;class Lt extends class extends r{}{static get template(){return d`
    <style>
      :host {
        display: block;
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        box-sizing: border-box;
        overflow: auto;
      }

      :host([passthrough]) {
        pointer-events: none;
      }
    </style>

    <slot></slot>
`}static get is(){return"vaadin-grid-outer-scroller"}static get properties(){return{scrollTarget:{type:Object},scrollHandler:{type:Object},passthrough:{type:Boolean,reflectToAttribute:!0,value:!0},outerScrolling:Boolean,noScrollbars:Boolean,_touchDevice:Boolean}}ready(){super.ready(),this.addEventListener("scroll",()=>this._syncScrollTarget()),this.parentElement.addEventListener("mousemove",this._onMouseMove.bind(this)),this.style.webkitOverflowScrolling="touch",this.addEventListener("mousedown",()=>this.outerScrolling=!0),this.addEventListener("mouseup",()=>{this.outerScrolling=!1,this.scrollHandler._scrollHandler()})}_onMouseMove(t){this._touchDevice||(this.passthrough=this.noScrollbars&&this.parentElement.hasAttribute("scroll-period")?t.offsetY<=this.clientHeight-20&&t.offsetX<=this.clientWidth-20:t.offsetY<=this.clientHeight&&t.offsetX<=this.clientWidth)}syncOuterScroller(){this.scrollTop=this.scrollTarget.scrollTop,this.scrollLeft=this.scrollTarget.scrollLeft}_syncScrollTarget(){requestAnimationFrame(()=>{this.scrollTarget.scrollTop=this.scrollTop,this.scrollTarget.scrollLeft=this.scrollLeft,this.scrollHandler._scrollHandler()})}}customElements.define(Lt.is,Lt);
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
const Ut=document.createElement("dom-module");Ut.appendChild(d`
  <style>
    @keyframes vaadin-grid-appear {
      to {
        opacity: 1;
      }
    }

    :host {
      display: block;
      animation: 1ms vaadin-grid-appear;
      height: 400px;
      flex: 1 1 auto;
      align-self: stretch;
      position: relative;
    }

    :host([hidden]) {
      display: none !important;
    }

    #scroller {
      display: block;
      transform: translateY(0);
      width: auto;
      height: auto;
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
    }

    :host([height-by-rows]) {
      height: auto;
      align-self: flex-start;
      flex-grow: 0;
      width: 100%;
    }

    :host([height-by-rows]) #scroller {
      width: 100%;
      height: 100%;
      position: relative;
    }

    #table {
      display: block;
      width: 100%;
      height: 100%;
      overflow: auto;
      z-index: -2;
      position: relative;
      outline: none;
    }

    #header {
      display: block;
      position: absolute;
      top: 0;
      width: 100%;
    }

    th {
      text-align: inherit;
    }

    /* Safari doesn't work with "inherit" */
    [safari] th {
      text-align: initial;
    }

    #footer {
      display: block;
      position: absolute;
      bottom: 0;
      width: 100%;
    }

    #items {
      display: block;
      width: 100%;
      position: relative;
      z-index: -1;
    }

    #items,
    #outersizer,
    #fixedsizer {
      border-top: 0 solid transparent;
      border-bottom: 0 solid transparent;
    }

    [part~="row"] {
      display: flex;
      width: 100%;
      box-sizing: border-box;
      margin: 0;
    }

    [part~="row"][loading] [part~="body-cell"] ::slotted(vaadin-grid-cell-content) {
      opacity: 0;
    }

    #items [part~="row"] {
      position: absolute;
    }

    #items [part~="row"]:empty {
      height: 1em;
    }

    [part~="cell"]:not([part~="details-cell"]) {
      flex-shrink: 0;
      flex-grow: 1;
      box-sizing: border-box;
      display: flex;
      width: 100%;
      position: relative;
      align-items: center;
      padding: 0;
      white-space: nowrap;
    }

    [part~="details-cell"] {
      position: absolute;
      bottom: 0;
      width: 100%;
      box-sizing: border-box;
      padding: 0;
    }

    [part~="cell"] ::slotted(vaadin-grid-cell-content) {
      display: block;
      width: 100%;
      box-sizing: border-box;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    [hidden] {
      display: none !important;
    }

    [frozen] {
      z-index: 2;
      will-change: transform;
    }

    #outerscroller {
      /* Needed (at least) for Android Chrome */
      z-index: 0;
    }

    #scroller:not([safari]) #outerscroller {
      /* Needed for Android Chrome (#1020). Can't be applied to Safari
      since it would re-introduce the sub-pixel overflow bug (#853) */
      will-change: transform;
    }

    [no-scrollbars]:not([safari]):not([firefox]) #outerscroller,
    [no-scrollbars][safari] #table,
    [no-scrollbars][firefox] #table {
      overflow: hidden;
    }

    [no-scrollbars]:not([safari]):not([firefox]) #outerscroller {
      pointer-events: none;
    }

    /* Reordering styles */
    :host([reordering]) [part~="cell"] ::slotted(vaadin-grid-cell-content),
    :host([reordering]) [part~="resize-handle"],
    #scroller[no-content-pointer-events] [part~="cell"] ::slotted(vaadin-grid-cell-content) {
      pointer-events: none;
    }

    [part~="reorder-ghost"] {
      visibility: hidden;
      position: fixed;
      pointer-events: none;
      opacity: 0.5;

      /* Prevent overflowing the grid in Firefox */
      top: 0;
      left: 0;
    }

    :host([reordering]) {
      -moz-user-select: none;
      -webkit-user-select: none;
      user-select: none;
    }

    #scroller[ie][column-reordering-allowed] [part~="header-cell"] {
      -ms-user-select: none;
    }

    :host([reordering]) #outerscroller {
      -webkit-overflow-scrolling: auto !important;
    }

    /* Resizing styles */
    [part~="resize-handle"] {
      position: absolute;
      top: 0;
      right: 0;
      height: 100%;
      cursor: col-resize;
      z-index: 1;
    }

    [part~="resize-handle"]::before {
      position: absolute;
      content: "";
      height: 100%;
      width: 35px;
      transform: translateX(-50%);
    }

    [last-column] [part~="resize-handle"]::before,
    [last-frozen] [part~="resize-handle"]::before {
      width: 18px;
      transform: none;
      right: 0;
    }

    #scroller[column-resizing] {
      -ms-user-select: none;
      -moz-user-select: none;
      -webkit-user-select: none;
      user-select: none;
    }

    /* Sizer styles */
    .sizer {
      display: flex;
      position: relative;
      width: 100%;
      visibility: hidden;
    }

    .sizer [part~="details-cell"] {
      display: none !important;
    }

    .sizer [part~="cell"][hidden] {
      display: none !important;
    }

    .sizer [part~="cell"] {
      display: block;
      flex-shrink: 0;
      line-height: 0;
      margin-top: -1em;
      height: 0 !important;
      min-height: 0 !important;
      max-height: 0 !important;
      padding: 0 !important;
    }

    .sizer [part~="cell"]::before {
      content: "-";
    }

    .sizer [part~="cell"] ::slotted(vaadin-grid-cell-content) {
      display: none !important;
    }

    /* Fixed mode (Tablet Edge) */
    #fixedsizer {
      position: absolute;
    }

    :not([edge][no-scrollbars]) #fixedsizer {
      display: none;
    }

    [edge][no-scrollbars] {
      /* Any value other than ‘none’ for the transform results in the creation of both a stacking context and
      a containing block. The object acts as a containing block for fixed positioned descendants. */
      transform: translateZ(0);
      overflow: hidden;
    }

    [edge][no-scrollbars] #header,
    [edge][no-scrollbars] #footer {
      position: fixed;
    }

    [edge][no-scrollbars] #items {
      position: fixed;
      width: 100%;
      will-change: transform;
    }

    /* RTL specific styles */

    :host([dir="rtl"]) [part~="reorder-ghost"] {
      left: auto;
      right: 0;
    }

    :host([dir="rtl"]) [part~="resize-handle"] {
      left: 0;
      right: auto;
    }

    :host([dir="rtl"]) [part~="resize-handle"]::before {
      transform: translateX(50%);
    }

    :host([dir="rtl"]) [last-column] [part~="resize-handle"]::before,
    :host([dir="rtl"]) [last-frozen] [part~="resize-handle"]::before {
      left: 0;
      right: auto;
    }
  </style>
`);const Zt=/^((?!chrome|android).)*safari/i.test(navigator.userAgent),Wt=navigator.userAgent.toLowerCase().indexOf("firefox")>-1;if(Zt||Wt){const t=document.createElement("style");t.textContent="\n    [scrolling][safari] #outerscroller,\n    [scrolling][firefox] #outerscroller {\n      pointer-events: auto;\n    }\n\n    [ios] #outerscroller {\n      pointer-events: auto;\n      z-index: -3;\n    }\n\n    [ios][scrolling] #outerscroller {\n      z-index: 0;\n    }\n\n    [ios] [frozen] {\n      will-change: auto;\n    }\n  ",Ut.querySelector("template").content.appendChild(t)}Ut.register("vaadin-grid-styles");
/**
@license
Copyright (c) 2017 Vaadin Ltd.
This program is available under Apache License Version 2.0, available at https://vaadin.com/license/
*/
const Qt=(()=>{try{return document.createEvent("TouchEvent"),!0}catch(t){return!1}})();class Vt extends(et(K(Et(St(Ft(pt(Ot(Rt(Ht(Dt(Yt(It(Gt(jt(Tt(Mt(Pt(Nt(zt))))))))))))))))))){static get template(){return d`
    <style include="vaadin-grid-styles"></style>

    <div id="scroller" no-scrollbars\$="[[!_scrollbarWidth]]" wheel-scrolling\$="[[_wheelScrolling]]" safari\$="[[_safari]]" ios\$="[[_ios]]" loading\$="[[loading]]" edge\$="[[_edge]]" firefox\$="[[_firefox]]" ie\$="[[_ie]]" column-reordering-allowed\$="[[columnReorderingAllowed]]">

      <table id="table" role="grid" aria-multiselectable="true" tabindex="0">
        <caption id="fixedsizer" class="sizer" part="row"></caption>
        <thead id="header" role="rowgroup"></thead>
        <tbody id="items" role="rowgroup"></tbody>
        <tfoot id="footer" role="rowgroup"></tfoot>
      </table>

      <div part="reorder-ghost"></div>
      <vaadin-grid-outer-scroller id="outerscroller" _touch-device="[[_touchDevice]]" scroll-target="[[scrollTarget]]" scroll-handler="[[_this]]" no-scrollbars="[[!_scrollbarWidth]]">
        <div id="outersizer" class="sizer" part="row"></div>
      </vaadin-grid-outer-scroller>
    </div>

    <!-- The template needs at least one slot or else shady doesn't distribute -->
    <slot name="nodistribute"></slot>

    <div id="focusexit" tabindex="0"></div>
`}static get is(){return"vaadin-grid"}static get version(){return"5.7.1"}static get observers(){return["_columnTreeChanged(_columnTree, _columnTree.*)"]}static get properties(){return{_this:{type:Object,value:function(){return this}},_safari:{type:Boolean,value:/^((?!chrome|android).)*safari/i.test(navigator.userAgent)},_ios:{type:Boolean,value:/iPad|iPhone|iPod/.test(navigator.userAgent)&&!window.MSStream||"MacIntel"===navigator.platform&&navigator.maxTouchPoints>1},_edge:{type:Boolean,value:"undefined"!=typeof CSS&&CSS.supports("(-ms-ime-align:auto)")},_ie:{type:Boolean,value:!(!navigator.userAgent.match(/Trident/)||navigator.userAgent.match(/MSIE/))},_firefox:{type:Boolean,value:navigator.userAgent.toLowerCase().indexOf("firefox")>-1},_android:{type:Boolean,value:/android/i.test(navigator.userAgent)},_touchDevice:{type:Boolean,value:Qt},heightByRows:{type:Boolean,value:!1,reflectToAttribute:!0,observer:"_heightByRowsChanged"},_recalculateColumnWidthOnceLoadingFinished:{type:Boolean,value:!0}}}constructor(){super(),this.addEventListener("animationend",this._onAnimationEnd)}connectedCallback(){super.connectedCallback(),this.recalculateColumnWidths()}attributeChangedCallback(t,e,i){super.attributeChangedCallback(t,e,i),"dir"===t&&(this.__isRTL="rtl"===i,this._updateScrollerMeasurements())}__hasRowsWithClientHeight(){return!!Array.from(this.$.items.children).filter(t=>t.clientHeight).length}__itemsReceived(){this._recalculateColumnWidthOnceLoadingFinished&&!this._cache.isLoading()&&this.__hasRowsWithClientHeight()&&(this._recalculateColumnWidthOnceLoadingFinished=!1,this.recalculateColumnWidths())}_recalculateColumnWidths(t){t.forEach(t=>{t.width="auto",t._origFlexGrow=t.flexGrow,t.flexGrow=0}),t.forEach(t=>{t._currentWidth=0,t._allCells.forEach(e=>{t._currentWidth=Math.max(t._currentWidth,e.offsetWidth+1)})}),t.forEach(t=>{t.width=`${t._currentWidth}px`,t.flexGrow=t._origFlexGrow,t._currentWidth=void 0,t._origFlexGrow=void 0})}recalculateColumnWidths(){if(this._columnTree)if(this._cache.isLoading())this._recalculateColumnWidthOnceLoadingFinished=!0;else{const t=this._getColumns().filter(t=>!t.hidden&&t.autoWidth);this._recalculateColumnWidths(t)}}_createScrollerRows(t){const e=[];for(var i=0;i<t;i++){const t=document.createElement("tr");t.setAttribute("part","row"),t.setAttribute("role","row"),this._columnTree&&this._updateRow(t,this._columnTree[this._columnTree.length-1],"body",!1,!0),e.push(t)}return this._columnTree&&this._columnTree[this._columnTree.length-1].forEach(t=>t.notifyPath&&t.notifyPath("_cells.*",t._cells)),C(this,()=>{this._updateFirstAndLastColumn(),this._resetKeyboardNavigation()}),e}_getRowTarget(){return this.$.items}_createCell(t){const e="vaadin-grid-cell-content-"+(this._contentIndex=this._contentIndex+1||0),i=document.createElement("vaadin-grid-cell-content");i.setAttribute("slot",e);const s=document.createElement(t);s.id=e.replace("-content-","-"),s.setAttribute("tabindex","-1"),s.setAttribute("role","td"===t?"gridcell":"columnheader");const r=document.createElement("slot");return r.setAttribute("name",e),s.appendChild(r),s._content=i,i.addEventListener("mousedown",()=>{if(window.chrome){const t=()=>{i.contains(this.getRootNode().activeElement)||s.focus(),document.removeEventListener("mouseup",t,!0)};document.addEventListener("mouseup",t,!0)}else setTimeout(()=>{i.contains(this.getRootNode().activeElement)||s.focus()})}),s}_updateRow(t,e,i,s,r){i=i||"body";const o=document.createDocumentFragment();Array.from(t.children).forEach(t=>t._vacant=!0),t.innerHTML="","outersizer"!==t.id&&"fixedsizer"!==t.id&&(t.hidden=!0),e.filter(t=>!t.hidden).forEach((e,n,a)=>{let l;if("body"===i){if(e._cells=e._cells||[],l=e._cells.filter(t=>t._vacant)[0],l||(l=this._createCell("td"),e._cells.push(l)),l.setAttribute("part","cell body-cell"),t.appendChild(l),n===a.length-1&&(this._rowDetailsTemplate||this.rowDetailsRenderer)){this._detailsCells=this._detailsCells||[];const e=this._detailsCells.filter(t=>t._vacant)[0]||this._createCell("td");-1===this._detailsCells.indexOf(e)&&this._detailsCells.push(e),e._content.parentElement||o.appendChild(e._content),this._configureDetailsCell(e),t.appendChild(e),this._a11ySetRowDetailsCell(t,e),e._vacant=!1}e.notifyPath&&!r&&e.notifyPath("_cells.*",e._cells)}else{const r="header"===i?"th":"td";s||"vaadin-grid-column-group"===e.localName?(l=e[`_${i}Cell`]||this._createCell(r),l._column=e,t.appendChild(l),e[`_${i}Cell`]=l):(e._emptyCells=e._emptyCells||[],l=e._emptyCells.filter(t=>t._vacant)[0]||this._createCell(r),l._column=e,t.appendChild(l),-1===e._emptyCells.indexOf(l)&&e._emptyCells.push(l)),l.setAttribute("part",`cell ${i}-cell`),this.__updateHeaderFooterRowVisibility(t)}l._content.parentElement||o.appendChild(l._content),l._vacant=!1,l._column=e}),this.appendChild(o),this._frozenCellsChanged(),this._updateFirstAndLastColumnForRow(t)}__updateHeaderFooterRowVisibility(t){if(!t)return;const e=Array.from(t.children).filter(e=>{const i=e._column;if(i._emptyCells&&i._emptyCells.indexOf(e)>-1)return!1;if(t.parentElement===this.$.header){if(i.headerRenderer||i._headerTemplate)return!0;if(null===i.header)return!1;if(i.path||void 0!==i.header)return!0}else if(i.footerRenderer||i._footerTemplate)return!0});t.hidden!==!e.length&&(t.hidden=!e.length,this.notifyResize())}_updateScrollerItem(t,e){this._preventScrollerRotatingCellFocus(t,e),this._columnTree&&(this._toggleAttribute("first",0===e,t),this._toggleAttribute("odd",e%2,t),this._a11yUpdateRowRowindex(t,e),this._getItem(e,t))}_columnTreeChanged(t,e){this._renderColumnTree(t),this.recalculateColumnWidths()}_renderColumnTree(t){for(Array.from(this.$.items.children).forEach(e=>this._updateRow(e,t[t.length-1],null,!1,!0));this.$.header.children.length<t.length;){const t=document.createElement("tr");t.setAttribute("part","row"),t.setAttribute("role","row"),this.$.header.appendChild(t);const e=document.createElement("tr");e.setAttribute("part","row"),e.setAttribute("role","row"),this.$.footer.appendChild(e)}for(;this.$.header.children.length>t.length;)this.$.header.removeChild(this.$.header.firstElementChild),this.$.footer.removeChild(this.$.footer.firstElementChild);Array.from(this.$.header.children).forEach((e,i)=>this._updateRow(e,t[i],"header",i===t.length-1)),Array.from(this.$.footer.children).forEach((e,i)=>this._updateRow(e,t[t.length-1-i],"footer",0===i)),this._updateRow(this.$.outersizer,t[t.length-1],null,!1,!0),this._updateRow(this.$.fixedsizer,t[t.length-1]),this._resizeHandler(),this._frozenCellsChanged(),this._updateFirstAndLastColumn(),this._resetKeyboardNavigation(),this._a11yUpdateHeaderRows(),this._a11yUpdateFooterRows()}_updateItem(t,e){t._item=e;const i=this.__getRowModel(t);this._toggleAttribute("selected",i.selected,t),this._a11yUpdateRowSelected(t,i.selected),this._a11yUpdateRowLevel(t,i.level),this._toggleAttribute("expanded",i.expanded,t),(this._rowDetailsTemplate||this.rowDetailsRenderer)&&this._toggleDetailsCell(t,e),this._generateCellClassNames(t,i),this._filterDragAndDrop(t,i),Array.from(t.children).forEach(t=>{if(t._renderer){const e=t._column||this;t._renderer.call(e,t._content,e,i)}else t._instance&&(t._instance.__detailsOpened__=i.detailsOpened,t._instance.__selected__=i.selected,t._instance.__level__=i.level,t._instance.__expanded__=i.expanded,t._instance.setProperties(i))}),this._debouncerUpdateHeights=a.debounce(this._debouncerUpdateHeights,c.after(1),()=>{this._updateMetrics(),this._positionItems(),this._updateScrollerSize()})}_resizeHandler(){this._updateDetailsCellHeights(),this._accessIronListAPI(super._resizeHandler,!0),this._updateScrollerMeasurements(),this._updateHeaderFooterMetrics()}_updateHeaderFooterMetrics(){const t=this.$.header.clientHeight+"px",e=this.$.footer.clientHeight+"px";[this.$.outersizer,this.$.fixedsizer,this.$.items].forEach(i=>{i.style.borderTopWidth=t,i.style.borderBottomWidth=e}),x(this.$.header,()=>{this._pendingScrollToIndex&&this._scrollToIndex(this._pendingScrollToIndex)})}_onAnimationEnd(t){0===t.animationName.indexOf("vaadin-grid-appear")&&(this._render(),this._updateHeaderFooterMetrics(),t.stopPropagation(),this.notifyResize(),this.__itemsReceived())}_toggleAttribute(t,e,i){i.hasAttribute(t)===!e&&(e?i.setAttribute(t,""):i.removeAttribute(t))}__getRowModel(t){return{index:t.index,item:t._item,level:this._getIndexLevel(t.index),expanded:this._isExpanded(t._item),selected:this._isSelected(t._item),detailsOpened:!(!this._rowDetailsTemplate&&!this.rowDetailsRenderer)&&this._isDetailsOpened(t._item)}}render(){this._columnTree&&(this._columnTree.forEach(t=>{t.forEach(t=>t._renderHeaderAndFooter())}),this._update())}notifyResize(){super.notifyResize()}_heightByRowsChanged(t,e){(t||e)&&this.notifyResize()}__forceReflow(){this._debouncerForceReflow=a.debounce(this._debouncerForceReflow,l,()=>{this.$.scroller.style.overflow="hidden",setTimeout(()=>this.$.scroller.style.overflow="")})}}customElements.define(Vt.is,Vt);const Jt=class{constructor(i){t(this,i),this._lastColumnsReceived=[],this._selectedActionItems=[],this.columns=[],this.expandedItems=[],this.itemActions=[],this.selectedItems=[],this.tableData=[],this._groupByColumn=!1,this.activeItem=null,this.defaultColumnFlexGrow=1,this.loadingSpinnerDebounce=500,this.pageSize=200,this.size=void 0,this.fontSize="25px",this._expandableRows=!1,this._loading=!1,this._spinnerHidden=!0,this.allowSortBySelection=!1,this.editable=!1,this.flexToSize=!1,this.hideActionMenu=!1,this.hideColumnFilter=!1,this.hideSelectionColumn=!1,this.instantSortWhenSelection=!1,this.multiSort=!1,this.resizable=!1,this.selectable=!1,this.sortable=!1,this.striped=!1,this.useKeyIfMissing=!0,this.defaultColumnWidth="100px",this.height="",this.selectionMode="none",this.item=void 0,this.waitForLoad=!1,this.pxTableSelectAll=e(this,"pxTableSelectAll",7),this.pxTableSelect=e(this,"pxTableSelect",7),this.pxTableDeselect=e(this,"pxTableDeselect",7)}componentWillLoad(){}componentDidLoad(){this._columnSelectorLabel=this._computeColumnSelectorLabel(this.selectedItems),this._vaadinGrid.deselectItem=t=>this._handledeSelectItem(t),this._vaadinGrid.selectItem=t=>this._handleSelectItem(t),this._vaadinGrid._getItemIndexInArray=(t,e)=>this._getItemIndexInArray(t,e);const t=this._vaadinGrid;let e=this;customElements.whenDefined("vaadin-grid").then((function(){const i=t.shadowRoot.querySelector("style");if("multi"===e.selectionMode&&t.querySelector(".vaadin-grid-select-all-checkbox").addEventListener("change",e._handleSelectAllItems.bind(e)),"single"===e.selectionMode){const e=t.querySelector(".vaadin-grid-select-all-checkbox");e&&!e.hasAttribute("hidden")&&e.setAttribute("hidden","true")}i.innerHTML="\n      @keyframes vaadin-grid-appear {\n        to {\n          opacity: 1;\n        }\n\n        }\n\n        :host {\n          display: block;\n          animation: 1ms vaadin-grid-appear;\n          height: "+(""!==e.height?e.height:"400px")+";\n          transform: translateZ(0);\n          font-size:"+e.fontSize+';\n        }\n\n        :host([hidden]) {\n          display: none !important;\n        }\n\n        #scroller {\n          display: block;\n          position: relative;\n          height: 100%;\n          width: 100%;\n          transform: translateY(0);\n        }\n\n        #table {\n          display: block;\n          width: 100%;\n          height: 100%;\n          overflow: auto;\n          z-index: -2;\n          position: relative;\n          outline: none;\n        }\n\n        [wheel-scrolling][edge] #table, [wheel-scrolling][ie] #table {\n          z-index: auto;\n        }\n\n        #header {\n          display: block;\n          position: absolute;\n          top: 0;\n          width: 100%;\n        }\n\n        [part~="header-cell"] {\n          font-weight: 500;\n          text-align: left;\n        }\n\n        #footer {\n          display: block;\n          position: absolute;\n          bottom: 0;\n          width: 100%;\n        }\n\n        #items {\n          display: block;\n          width: 100%;\n          position: relative;\n          z-index: -1;\n        }\n\n        #items, #outersizer, #fixedsizer {\n          border-top: 0 solid transparent;\n          border-bottom: 0 solid transparent;\n        }\n\n        [part~="row"] {\n          display: flex;\n          width: 100%;\n          box-sizing: border-box;\n          margin: 0;\n        }\n\n        #items [part~="row"] {\n          position: absolute;\n        }\n\n        #items [part~="row"]:empty {\n          height: 1em;\n        }\n\n        [part~="cell"]:not([part~="details-cell"]) {\n          flex-shrink: 0;\n          flex-grow: 1;\n          box-sizing: border-box;\n          display: flex;\n          width: 100%;\n          position: relative;\n        }\n\n        [part~="details-cell"] {\n          position: absolute;\n          bottom: 0;\n          width: 100%;\n          box-sizing: border-box;\n        }\n\n        [part~="cell"]:not([part~="details-cell"]) ::slotted(vaadin-grid-cell-content) {\n          width: 100%;\n          display: inline-flex;\n          justify-content: center;\n          flex-direction: column;\n          white-space: nowrap;\n          overflow: hidden;\n        }\n\n        [hidden] {\n          display: none !important;\n        }\n\n        [frozen] {\n          z-index: 2;\n        }\n\n        #outerscroller {\n          z-index: 0;\n        }\n\n        #scroller:not([safari]) #outerscroller {\n          will-change: transform;\n        }\n\n        [no-scrollbars]:not([safari]):not([firefox]) #outerscroller, [no-scrollbars][safari] #table, [no-scrollbars][firefox] #table {\n          overflow: hidden;\n        }\n\n        [no-scrollbars]:not([safari]):not([firefox]) #outerscroller {\n          pointer-events: none;\n        }\n\n        [scrolling][safari] #outerscroller, [scrolling][firefox] #outerscroller {\n          pointer-events: auto;\n        }\n\n        [ios] #outerscroller {\n          pointer-events: auto;\n                z-index: -3;\n        }\n\n        [ios][scrolling] #outerscroller {\n          z-index: 0;\n        }\n\n        :host([reordering]) [part~="cell"] ::slotted(vaadin-grid-cell-content), :host([reordering]) [part~="resize-handle"], #scroller[no-content-pointer-events] [part~="cell"] ::slotted(vaadin-grid-cell-content) {\n          pointer-events: none;\n        }\n\n        [part~="reorder-ghost"] {\n          visibility: hidden;\n          position: fixed;\n          opacity: 0.5;\n          pointer-events: none;\n        }\n\n        :host([reordering]) {\n          -moz-user-select: none;\n          -webkit-user-select: none;\n          user-select: none;\n        }\n\n        #scroller[ie][column-reordering-allowed] [part~="header-cell"] {\n          -ms-user-select: none;\n        }\n\n        :host([reordering]) #outerscroller {\n          -webkit-overflow-scrolling: auto !important;\n        }\n\n        [part~="resize-handle"] {\n          position: absolute;\n          top: 0;\n          right: 0;\n          height: 100%;\n          cursor: col-resize;\n          z-index: 1;\n        }\n\n        [part~="resize-handle"]::before {\n          position: absolute;\n          content: "";\n          height: 100%;\n          width: 35px;\n          transform: translateX(-50%);\n        }\n\n        [last-column] [part~="resize-handle"]::before, [last-frozen] [part~="resize-handle"]::before {\n          width: 18px;\n          transform: translateX(-100%);\n        }\n\n        #scroller[column-resizing] {\n          -ms-user-select: none;\n          -moz-user-select: none;\n          -webkit-user-select: none;\n          user-select: none;\n        }\n\n        .sizer {\n          display: flex;\n          position: relative;\n          width: 100%;\n          visibility: hidden;\n        }\n\n        .sizer [part~="details-cell"] {\n          display: none;\n        }\n\n        .sizer [part~="cell"][hidden] {\n          display: none;\n        }\n\n        .sizer [part~="cell"] {\n          display: block;\n          flex-shrink: 0;\n          line-height: 0;\n          margin-top: -1em;\n          height: 0 !important;\n          min-height: 0 !important;\n          max-height: 0 !important;\n          padding: 0 !important;\n        }\n\n        .sizer [part~="cell"]::before {\n          content: "-";\n        }\n\n        .sizer [part~="cell"] ::slotted(vaadin-grid-cell-content) {\n          display: none;\n        }\n\n        #fixedsizer {\n          position: absolute;\n        }\n\n        :not([edge][no-scrollbars]) #fixedsizer {\n          display: none;\n        }\n\n        [edge][no-scrollbars] {\n          transform: translateZ(0);\n          overflow: hidden;\n        }\n\n        [edge][no-scrollbars] #header, [edge][no-scrollbars] #footer {\n          position: fixed;\n        }\n\n        [edge][no-scrollbars] #items {\n          position: fixed;\n          width: 100%;\n          will-change: transform;\n        }\n\n        :host {\n          --px-data-grid-padding-top: 12px;\n          --px-data-grid-padding-bottom: 12px;\n          --px-data-grid-padding-left: 12px;\n          --px-data-grid-padding-right: 12px;\n          --px-data-grid-cell-focus-outline-width: 2px;\n          --px-data-grid-focused-padding-top: 10px;\n          --px-data-grid-focused-padding-bottom: 10px;\n          --px-data-grid-focused-padding-left: 10px;\n          --px-data-grid-focused-padding-right: 10px;\n          --px-data-grid-cell-focused-color: #72b9dd;\n          --px-data-grid-reorder-ghost-outline-color: rgb(3, 44, 54);\n          --px-data-grid-reorder-ghost-box-shadow-color: #a8a8a8;\n\n\n          --px-data-grid-details-padding-left: 42px;\n\n          background-color: var(--px-base-background-color, white);\n          color: var(--px-base-text-color, rgb(64, 64, 64));\n          border: 0;\n          border-bottom: 1px solid var(--px-data-grid-border-color, rgb(126, 126, 126));\n        }\n\n        [part~="cell"] {\n          min-height: 0;\n          background: var(--px-base-background-color, white);\n          padding: 0;\n        }\n\n        [part~="header-cell"], [part~="footer-cell"] {\n          background: var(--px-data-grid-cell-details-background-color , transparent);\n          color: var(--px-data-grid-header-text-color, rgb(44, 64, 76));\n          text-transform: uppercase;\n        }\n\n        [part~="header-cell"] ::slotted(*), [part~="footer-cell"] ::slotted(*) {\n          font-size: var(--px-data-grid-header-font-size, 25px);\n        }\n\n        [part~="resize-handle"]::before {\n          width: 15px;\n          transform: translateX(-10px);\n        }\n\n        [part~="row"]:last-child [part~="header-cell"] {\n          border-bottom: 1px solid var(--px-data-grid-border-color, rgb(126, 126, 126));\n        }\n\n        [part~="row"]:first-child [part~="footer-cell"] {\n          border-top: 1px solid var(--px-data-grid-border-color, rgb(126, 126, 126));\n        }\n\n        [part~="body-cell"] {\n          border-bottom: 1px solid var(--px-data-grid-separator-color, rgb(209, 209, 209));\n        }\n\n        :host([auto-height]) tr:last-child [part~="body-cell"] {\n          border-bottom: 0;\n        }\n\n        [part~="row"][loading] [part~="body-cell"] ::slotted(vaadin-grid-cell-content) {\n          opacity: 0;\n        }\n\n        :host(:not([reordering])) [part~="row"][selected] [part~="cell"] {\n          background: var(--px-data-grid-cell-background-color--selected, rgb(195, 195, 195));\n          border-bottom: 1px solid var(--px-data-grid-cell-border-color--selected, rgba(154, 154, 154, 0.2));\n        }\n\n        :host([tree-grid]) [part~="row"][aria-level="1"] [part~="cell"] {\n          background: var(--px-data-grid-background-color-striped--groupby, rgb(195, 195, 195));\n        }\n\n        :host([horizontal-offset]) [part~="cell"][last-frozen] {\n          border-right: 1px solid var(--px-data-grid-separator-color, rgb(209, 209, 209));\n        }\n\n        [part~="cell"][last-frozen] {\n          border-right: 0px solid transparent;\n          transition: border-right 0.3s ease-out;\n        }\n\n        [part~="cell"]:focus {\n          outline: none;\n        }\n\n        :host([navigating]) [part~="cell"]:focus {\n          box-shadow: inset 0 0 0 var(--px-data-grid-cell-focus-outline-width) var(--px-data-grid-cell-focused-color, transparent);\n        }\n\n        :host([navigating]) [part~="cell"]:not([part~="header-cell"]):not([part~="details-cell"]):focus ::slotted(vaadin-grid-cell-content) {\n          padding-top: var(--px-data-grid-focused-padding-top);\n          padding-bottom: var(--px-data-grid-focused-padding-bottom);\n          padding-left: var(--px-data-grid-focused-padding-left);\n          padding-right: var(--px-data-grid-focused-padding-right);\n          border: var(--px-data-grid-cell-focus-outline-width) solid var(--px-data-grid-cell-focused-color, transparent);\n        }\n\n        :host([striped]) [part~="row"][odd] {\n          background: var(--px-data-grid-background-color-striped--odd, rgb(224, 224, 224));\n        }\n\n        :host([striped]) [part~="row"][selected] {\n          background: var(--px-data-grid-background-color-striped--selected, rgb(195, 195, 195));\n        }\n\n        :host([striped]) [part~="row"][odd] [part~="cell"] {\n          background: var(--px-data-grid-background-color-striped--odd, rgb(224, 224, 224));\n        }\n\n        :host([striped]) [part~="row"][selected] [part~="cell"] {\n          background: var(--px-data-grid-background-color-striped--selected, rgb(195, 195, 195));\n        }\n\n        [part~="cell"]:not(:empty):not([details-cell]) {\n          padding: 6px;\n        }\n\n        [part~="cell"]:not([part~="details-cell"]) ::slotted(vaadin-grid-cell-content) {\n          padding-top: var(--px-data-grid-padding-top);\n          padding-bottom: var(--px-data-grid-padding-bottom);\n          padding-left: var(--px-data-grid-padding-left);\n          padding-right: var(--px-data-grid-padding-right);\n          white-space: normal;\n        }\n\n        [part~="header-cell"]:not([part~="details-cell"]) ::slotted(vaadin-grid-cell-content), [part~="footer-cell"]:not([part~="details-cell"]) ::slotted(vaadin-grid-cell-content) {\n          padding-top: 0;\n          padding-bottom: 0;\n          height: 35px;\n        }\n\n        [part~="reorder-ghost"] {\n          opacity: 0.65;\n          outline: 1px solid var(--px-data-grid-reorder-ghost-outline-color);\n          box-shadow: 0 0 0 1px var(--px-data-grid-reorder-ghost-box-shadow-color), 0 2px 8px 0 var(--px-data-grid-reorder-ghost-box-shadow-color);\n          padding-top: var(--px-data-grid-padding-top) !important;\n          padding-bottom: var(--px-data-grid-padding-bottom) !important;\n          padding-left: var(--px-data-grid-padding-left) !important;\n          padding-right: var(--px-data-grid-padding-right) !important;\n        }\n\n        [part~="cell"][reorder-status="dragging"] {\n          background: var(--px-data-grid-dragged-column-background-color, rgb(251, 251, 251));\n          --px-data-grid-cell-dragging-color: transparent;\n        }\n\n        [part~="cell"][reorder-status="dragging"] ::slotted(vaadin-grid-cell-content) {\n          background: var(--px-data-grid-dragged-column-background-color, rgb(251, 251, 251));\n        }\n\n        [part~="body-cell"][aria-expanded="true"] {\n          border-bottom: none;\n          background-color: var(--px-data-grid-cell-details-background-color, rgb(168, 168, 168));\n        }\n\n        :host(:not([reordering])) [part~="row"][selected] [part~="body-cell"][aria-expanded="true"] {\n          border-bottom: none;\n        }\n\n        [part~="details-cell"] {\n          background-color: var(--px-data-grid-cell-details-background-color, rgb(168, 168, 168));\n          border-bottom: 1px solid var(--px-data-grid-separator-color, rgb(209, 209, 209));\n          padding-top: var(--px-data-grid-padding-top);\n          padding-bottom: var(--px-data-grid-padding-bottom);\n          padding-left: var(--px-data-grid-details-padding-left);\n          padding-right: var(--px-data-grid-padding-right);\n          white-space: normal;\n        }\n\n        [part~="row"] [part~="body-cell"][hidden-unless-hover] ::slotted(vaadin-grid-cell-content) {\n          visibility: hidden;\n        }\n\n        [part~="row"]:hover [part~="body-cell"][hidden-unless-hover] ::slotted(vaadin-grid-cell-content), [part~="row"] [part~="body-cell"][hidden-unless-hover]:focus ::slotted(vaadin-grid-cell-content), [part~="row"] [part~="body-cell"][hidden-unless-hover]:active ::slotted(vaadin-grid-cell-content), .safari-hover [part~="body-cell"][hidden-unless-hover] ::slotted(vaadin-grid-cell-content) {\n          visibility: visible;\n        }\n\n      '})),customElements.whenDefined("vaadin-checkbox").then((function(){e._chkStyleFix()})),customElements.whenDefined("vaadin-grid-sorter").then((function(){const e=t.querySelectorAll("vaadin-grid-sorter");for(let t=0;t<e.length;t++){const i=e[t].shadowRoot.querySelector("style");i.innerHTML=i.innerHTML+' \n :host(:not([direction])) [part="indicators"]::before {   opacity: 0.8; }'}})),this._vaadinGrid.items=this.tableData}disconnectedCallback(){"multi"===this.selectionMode&&this._vaadinGrid.querySelector(".vaadin-grid-select-all-checkbox").removeEventListener("on-checked-changed",this._handleSelectAllItems)}async _chkStyleFix(){let t=this;const e=t._vaadinGrid.querySelectorAll("vaadin-checkbox");for(const i of e)if(t.hideSelectionColumn)i.setAttribute("hidden","true");else{const t=i.shadowRoot.querySelector("style");t.innerHTML=t.innerHTML+' \n [part="checkbox"] { background-color: var(--lumo-primary-color); height: auto;} \n:host(:not([checked]):not([indeterminate]):not([disabled]):hover) [part="checkbox"] {  \n  background-color: var(--lumo-primary-color);  \n} \n'}}_checkColumnResizable(t,e){return!!t&&(void 0===e||e)}_computeColumnSelectorLabel(t){return{columnsSelected:t.length}+" "+{selectionText:"Kolon Seçildi"}}_doInstantSortAfterSelect(){return this.instantSortWhenSelection&&this.allowSortBySelection&&Array.from(this._vaadinGrid._sorters).map(t=>t.path).filter(t=>"--selection--"===t).length>0}_getColumnByPath(t){if(this.columns)return this.columns.filter(e=>e.path===t)[0]}_getColumnElementById(t){return this._getColumns().filter(e=>e.mappedObject&&e.mappedObject.id==t)[0]}_getColumnFlexGrow(t){return void 0===t.flexGrow?this.defaultColumnFlexGrow:t.flexGrow}_getColumns(){return this._vaadinGrid?Array.from(this._vaadinGrid.querySelectorAll("vaadin-grid"+(this.sortable?"-sort":"")+"-column")):[]}_getColumnWidth(t){return t.width?t.width:this.defaultColumnWidth}_getColumnsWithName(t){return this._getColumns().filter(e=>e.name==t)}getItemId(t){return this.itemIdPath?this.get(this.itemIdPath,t):t}_getItemIndexInArray(t,e){let i=this,s=-1,r=0;for(const o of e)-1==s&&(i._itemsEqual(o,t)&&(s=r),r++);return s}_getValue(t,e){return t&&e?this.get(t.path,e):void 0}_gridGetItemId(t){return this._groupByColumn&&t&&t._groupId?t._groupId:this.itemIdPath?this._vaadinGrid.get(this.itemIdPath,t):t}_groupByColumnAllowed(t,e){return t&&!e}async _handledeSelectItem(t){if(t&&this._vaadinGrid._isSelected(t))if(this.pxTableDeselect.emit(t),this._isMultiSelect()){const e=this._getItemIndexInArray(t,this._vaadinGrid.selectedItems);e>-1&&this._vaadinGrid.splice("selectedItems",e,1)}else this._vaadinGrid.pop("selectedItems")}_handleSelectAllItems(){this.pxTableSelectAll.emit()}_handleSelectItem(t){let e=this;if(t&&!this._vaadinGrid._isSelected(t)){this._isMultiSelect()||this._vaadinGrid.pop("selectedItems"),this._vaadinGrid.push("selectedItems",t);const i=this._getItemIndexInArray(t,e.tableData);this.pxTableSelect.emit(i>-1?e.tableData[i]:t)}}_isAutoHeight(t){return"auto"===t}_isMultiSelect(){return this._isSelectable()&&"multi"==this.selectionMode}_isSelectable(){return"single"==this.selectionMode||"multi"==this.selectionMode}_isStriped(t,e){return t&&!e}_itemsEqual(t,e){return this.getItemId(t)===this.getItemId(e)}_offerActionColumn(t,e){return t||e&&e.length>0}_offerEditColumn(t){return t}_resolveColumnPath(t){return void 0===t.path&&console.warn(`column.path for column ${JSON.stringify(t)} should be initialized.`),t.path?t.path:""}_selectedItemsChanged(){this._vaadinGrid&&this._doInstantSortAfterSelect()&&this._vaadinGrid.clearCache()}_undefinedToFalse(t){return void 0!==t&&t}get(t,e){return e[t]?e[t]:void 0}getData(t){let e=[];if(t){e=Array.from(this._vaadinGrid.querySelectorAll("px-data-grid-cell-content-wrapper"));const t=this._vaadinGrid.getBoundingClientRect(),i=this._vaadinGrid.querySelector("px-data-grid-header-cell").getBoundingClientRect();e=e.filter(e=>{const s=e.getBoundingClientRect();return s.top<t.bottom&&s.bottom>i.bottom}).map(t=>t.item).filter((t,e,i)=>i.indexOf(t)===e&&t)}else{const t=this._vaadinGrid._cache.items;Object.keys(t).forEach(i=>{e.push(t[i])})}return e.map(t=>{const e={};return this.getVisibleColumns().forEach(i=>{e[i.path]=t[i.path]}),e})}async clearSelections(){let t=this;const e=await t.getSelectedItems();for(const i of e)await t._handledeSelectItem(i)}async getSelectedItems(){return this._vaadinGrid.selectedItems}getVisibleColumns(){return this._getColumns().filter(t=>!t.hidden).sort((t,e)=>t._order-e._order)}resolveColumnHeader(t){return t.header?t.header:t.name}async selectAllRows(){if("multi"===this.selectionMode){const t=this._vaadinGrid.querySelector(".vaadin-grid-select-all-checkbox");t.checked=!t.checked}}async selectRow(t){this._handleSelectItem(t)}async setData(t){let e=this;if(await e._vaadinGrid.clearCache(),await e.clearSelections(),e.tableData=t,"#"===e.columns[0].name)for(let i=0;i<this.tableData.length;i++)e.tableData[i]["__#__"]=i+1;e._vaadinGrid.items=e.tableData,e.waitForLoad=!0,await e.waitForLoadFN()}delay(t){return new Promise(e=>setTimeout(e,t))}async waitForLoadFN(){let t=this;if(!1===t.waitForLoad)return await t.delay(100),!0;await t.delay(50),t.waitForLoadFN()}async load(){let t=this;t._vaadinGrid&&await t._chkStyleFix(),t.waitForLoad=!1}getTemplateSelectable(){if(this._isSelectable())return i("vaadin-grid-selection-column",{"auto-select":!0})}getTemplateColumnHeader(t){return i("vaadin-grid-sort-column",{path:this._resolveColumnPath(t),header:this.resolveColumnHeader(t)})}getTemplateColumns(){const t=this.sortable;return this.columns.map(e=>i(t?"vaadin-grid-sort-column":"vaadin-grid-column",{name:e.name,header:e.header,path:e.path,frozen:this._undefinedToFalse(e.frozen),hidden:e.hidden,type:e.type,textAlign:"number"==e.type?"end":"start",resizable:this._checkColumnResizable(this.resizable,e.resizable),mappedObject:e,width:this._getColumnWidth(e),flexGrow:this._getColumnFlexGrow(e),groupByColumnAllowed:this._groupByColumnAllowed(this._hasLocalDataProvider,this._expandableRows),isDataColumn:!0},this.getTemplateColumnHeader(e)))}getTemplateOfferEditColumn(){this._offerEditColumn(this.editable)}render(){return[i("vaadin-grid",{ref:t=>this._vaadinGrid=t,size:this.size,activeItem:this.activeItem,columnReorderingAllowed:!1,expandedItems:this.expandedItems,striped:this._isStriped(this.striped,this._groupByColumn),selectedItems:this.selectedItems,multiSort:this.multiSort,itemIdPath:this.itemIdPath,pageSize:this.pageSize,autoHeight:this._isAutoHeight(this.gridHeight),loading:this._loading,onLoad:this.load()},this.getTemplateSelectable(),this.getTemplateColumns(),i("vaadin-grid-column-group",null,this.getTemplateOfferEditColumn())),i("px-spinner",{size:40,finished:this._spinnerHidden})]}get myElement(){return s(this)}static get watchers(){return{selectedItems:["_selectedItemsChanged"]}}static get style(){return"\@charset \"UTF-8\";:host{--px-data-grid-padding-top:12px;--px-data-grid-padding-bottom:12px;--px-data-grid-padding-left:12px;--px-data-grid-padding-right:12px;--px-data-grid-cell-focus-outline-width:2px;--px-data-grid-focused-padding-top:10px;--px-data-grid-focused-padding-bottom:10px;--px-data-grid-focused-padding-left:10px;--px-data-grid-focused-padding-right:10px;--px-data-grid-cell-focused-color:#72b9dd;--px-data-grid-reorder-ghost-outline-color:rgb(3, 44, 54);--px-data-grid-reorder-ghost-box-shadow-color:#a8a8a8;--px-data-grid-details-padding-left:42px;background-color:var(--px-base-background-color, white);color:var(--px-base-text-color, #404040);border:0;border-bottom:1px solid var(--px-data-grid-border-color, #7e7e7e)}[part~=cell]{min-height:0;background:var(--px-base-background-color, white);padding:0}[part~=header-cell],[part~=footer-cell]{background:var(--px-base-background-color, white);color:var(--px-data-grid-header-text-color, #2c404c);text-transform:uppercase}[part~=header-cell] ::slotted(*),[part~=footer-cell] ::slotted(*){font-size:var(--px-data-grid-header-font-size, 12px)}[part~=resize-handle]::before{width:15px;-webkit-transform:translateX(-10px);transform:translateX(-10px)}[part~=row]:last-child [part~=header-cell]{border-bottom:1px solid var(--px-data-grid-border-color, #7e7e7e)}[part~=row]:first-child [part~=footer-cell]{border-top:1px solid var(--px-data-grid-border-color, #7e7e7e)}[part~=body-cell]{border-bottom:1px solid var(--px-data-grid-separator-color, #d1d1d1)}:host([auto-height]) tr:last-child [part~=body-cell]{border-bottom:0}[part~=row][loading] [part~=body-cell] ::slotted(vaadin-grid-cell-content){opacity:0}:host(:not([reordering])) [part~=row][selected] [part~=cell]{background:var(--px-data-grid-cell-background-color--selected, #c3c3c3);border-bottom:1px solid var(--px-data-grid-cell-border-color--selected, rgba(154, 154, 154, 0.2))}:host([tree-grid]) [part~=row][aria-level=\"1\"] [part~=cell]{background:var(--px-data-grid-background-color-striped--groupby, #c3c3c3)}:host([horizontal-offset]) [part~=cell][last-frozen]{border-right:1px solid var(--px-data-grid-separator-color, #d1d1d1)}[part~=cell][last-frozen]{border-right:0px solid transparent;-webkit-transition:border-right 0.3s ease-out;transition:border-right 0.3s ease-out}[part~=cell]:focus{outline:none}:host([navigating]) [part~=cell]:focus{-webkit-box-shadow:inset 0 0 0 var(--px-data-grid-cell-focus-outline-width) var(--px-data-grid-cell-focused-color, transparent);box-shadow:inset 0 0 0 var(--px-data-grid-cell-focus-outline-width) var(--px-data-grid-cell-focused-color, transparent)}:host([navigating]) [part~=cell]:not([part~=header-cell]):not([part~=details-cell]):focus ::slotted(vaadin-grid-cell-content){padding-top:var(--px-data-grid-focused-padding-top);padding-bottom:var(--px-data-grid-focused-padding-bottom);padding-left:var(--px-data-grid-focused-padding-left);padding-right:var(--px-data-grid-focused-padding-right);border:var(--px-data-grid-cell-focus-outline-width) solid var(--px-data-grid-cell-focused-color, transparent)}:host([striped]) [part~=row][odd]{background:var(--px-data-grid-background-color-striped--odd, #e0e0e0)}:host([striped]) [part~=row][selected]{background:var(--px-data-grid-background-color-striped--selected, #c3c3c3)}:host([striped]) [part~=row][odd] [part~=cell]{background:var(--px-data-grid-background-color-striped--odd, #e0e0e0)}:host([striped]) [part~=row][selected] [part~=cell]{background:var(--px-data-grid-background-color-striped--selected, #c3c3c3)}[part~=cell]:not([part~=details-cell]) ::slotted(vaadin-grid-cell-content){padding-top:var(--px-data-grid-padding-top);padding-bottom:var(--px-data-grid-padding-bottom);padding-left:var(--px-data-grid-padding-left);padding-right:var(--px-data-grid-padding-right);white-space:normal}[part~=header-cell]:not([part~=details-cell]) ::slotted(vaadin-grid-cell-content),[part~=footer-cell]:not([part~=details-cell]) ::slotted(vaadin-grid-cell-content){padding-top:0;padding-bottom:0;height:35px}[part~=reorder-ghost]{opacity:0.65;outline:1px solid var(--px-data-grid-reorder-ghost-outline-color);-webkit-box-shadow:0 0 0 1px var(--px-data-grid-reorder-ghost-box-shadow-color), 0 2px 8px 0 var(--px-data-grid-reorder-ghost-box-shadow-color);box-shadow:0 0 0 1px var(--px-data-grid-reorder-ghost-box-shadow-color), 0 2px 8px 0 var(--px-data-grid-reorder-ghost-box-shadow-color);padding-top:var(--px-data-grid-padding-top) !important;padding-bottom:var(--px-data-grid-padding-bottom) !important;padding-left:var(--px-data-grid-padding-left) !important;padding-right:var(--px-data-grid-padding-right) !important}[part~=cell][reorder-status=dragging]{background:var(--px-data-grid-dragged-column-background-color, #fbfbfb);--px-data-grid-cell-dragging-color:transparent}[part~=cell][reorder-status=dragging] ::slotted(vaadin-grid-cell-content){background:var(--px-data-grid-dragged-column-background-color, #fbfbfb)}[part~=body-cell][aria-expanded=true]{border-bottom:none;background-color:var(--px-data-grid-cell-details-background-color, #a8a8a8)}:host(:not([reordering])) [part~=row][selected] [part~=body-cell][aria-expanded=true]{border-bottom:none}[part~=details-cell]{background-color:var(--px-data-grid-cell-details-background-color, #a8a8a8);border-bottom:1px solid var(--px-data-grid-separator-color, #d1d1d1);padding-top:var(--px-data-grid-padding-top);padding-bottom:var(--px-data-grid-padding-bottom);padding-left:var(--px-data-grid-details-padding-left);padding-right:var(--px-data-grid-padding-right);white-space:normal}[part~=row] [part~=body-cell][hidden-unless-hover] ::slotted(vaadin-grid-cell-content){visibility:hidden}[part~=row]:hover [part~=body-cell][hidden-unless-hover] ::slotted(vaadin-grid-cell-content),[part~=row] [part~=body-cell][hidden-unless-hover]:focus ::slotted(vaadin-grid-cell-content),[part~=row] [part~=body-cell][hidden-unless-hover]:active ::slotted(vaadin-grid-cell-content),.safari-hover [part~=body-cell][hidden-unless-hover] ::slotted(vaadin-grid-cell-content){visibility:visible}/*! normalize.css v3.0.2 | MIT License | git.io/normalize */html{background-color:var(--px-base-background-color, white);font-size:15px;overflow-y:scroll;min-height:100%;}:host,html{color:var(--px-base-text-color, #2c404c);line-height:1.3333333333;-webkit-text-size-adjust:100%;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;text-size-adjust:100%;-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;}body{margin:0}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline;}audio:not([controls]){display:none;height:0}[hidden],template{display:none}h1,h2,h3,h4,h5,h6,ul,ol,dl,blockquote,p,address,hr,table,fieldset,figure,pre{margin-bottom:1rem}li>ul,li>ol{margin-bottom:0}ul,ol,dd{margin-left:2rem}img{max-width:100%;border:0}svg:not(:root){overflow:hidden}figure{margin:0}hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0}pre{overflow:auto}fieldset{margin:0}fieldset,fieldset legend{border:0;padding:0}legend{margin-bottom:1.3333333333rem;font-weight:bold}.form-field{margin-bottom:1.3333333333rem}.form-field:last-child{margin-bottom:2.6666666667rem}label{display:block;margin-bottom:0.2666666667rem;cursor:pointer;text-transform:uppercase;font-size:12px;color:var(--px-input-label-text-color, #677e8c);letter-spacing:var(--px-headings-letter-spacing, 0.3px)}label.disabled{cursor:not-allowed}.label--inline{display:inline-block;margin-bottom:0}.label--inline+.text-input{margin-left:0.6666666667rem}.text-input,textarea,select{font:inherit;outline:0;background-color:var(--px-input-background-color, transparent);font-weight:700;-webkit-transition:background 0.4s, border-color 0.4s, color 0.4s;transition:background 0.4s, border-color 0.4s, color 0.4s}.text-input:disabled,.text-input[readonly],textarea:disabled,textarea[readonly],select:disabled,select[readonly]{border-color:var(--px-input-border-color--disabled, rgba(0, 0, 0, 0.2));color:var(--px-input-text-color--disabled, rgba(103, 126, 140, 0.6));cursor:not-allowed;background-color:var(--px-input-background-color--disabled, transparent)}.text-input:hover,textarea:hover,select:hover{background-color:var(--px-input-background-color--hover, #d8e0e5)}.text-input::-webkit-input-placeholder,textarea::-webkit-input-placeholder,select::-webkit-input-placeholder{color:var(--px-input-text-color--placeholder, rgba(0, 0, 0, 0.3))}.text-input::-moz-placeholder,textarea::-moz-placeholder,select::-moz-placeholder{color:var(--px-input-text-color--placeholder, rgba(0, 0, 0, 0.3))}.text-input:-ms-input-placeholder,textarea:-ms-input-placeholder,select:-ms-input-placeholder{color:var(--px-input-text-color--placeholder, rgba(0, 0, 0, 0.3))}.text-input::-ms-input-placeholder,textarea::-ms-input-placeholder,select::-ms-input-placeholder{color:var(--px-input-text-color--placeholder, rgba(0, 0, 0, 0.3))}.text-input::placeholder,textarea::placeholder,select::placeholder{color:var(--px-input-text-color--placeholder, rgba(0, 0, 0, 0.3))}.text-input:focus::-webkit-input-placeholder,textarea:focus::-webkit-input-placeholder,select:focus::-webkit-input-placeholder{color:var(--px-input-text-color--placeholder--focused, rgba(0, 0, 0, 0.3))}.text-input:focus::-moz-placeholder,textarea:focus::-moz-placeholder,select:focus::-moz-placeholder{color:var(--px-input-text-color--placeholder--focused, rgba(0, 0, 0, 0.3))}.text-input:focus:-ms-input-placeholder,textarea:focus:-ms-input-placeholder,select:focus:-ms-input-placeholder{color:var(--px-input-text-color--placeholder--focused, rgba(0, 0, 0, 0.3))}.text-input:focus::-ms-input-placeholder,textarea:focus::-ms-input-placeholder,select:focus::-ms-input-placeholder{color:var(--px-input-text-color--placeholder--focused, rgba(0, 0, 0, 0.3))}.text-input:focus::placeholder,textarea:focus::placeholder,select:focus::placeholder{color:var(--px-input-text-color--placeholder--focused, rgba(0, 0, 0, 0.3))}.text-input:disabled::-webkit-input-placeholder,textarea:disabled::-webkit-input-placeholder,select:disabled::-webkit-input-placeholder{color:var(--px-input-text-color--disabled, rgba(103, 126, 140, 0.6))}.text-input:disabled::-moz-placeholder,textarea:disabled::-moz-placeholder,select:disabled::-moz-placeholder{color:var(--px-input-text-color--disabled, rgba(103, 126, 140, 0.6))}.text-input:disabled:-ms-input-placeholder,textarea:disabled:-ms-input-placeholder,select:disabled:-ms-input-placeholder{color:var(--px-input-text-color--disabled, rgba(103, 126, 140, 0.6))}.text-input:disabled::-ms-input-placeholder,textarea:disabled::-ms-input-placeholder,select:disabled::-ms-input-placeholder{color:var(--px-input-text-color--disabled, rgba(103, 126, 140, 0.6))}.text-input:disabled::placeholder,textarea:disabled::placeholder,select:disabled::placeholder{color:var(--px-input-text-color--disabled, rgba(103, 126, 140, 0.6))}.text-input{height:2em;width:100%;border:none;border-radius:0;border-bottom:1px solid var(--px-input-border-color--outer, #889aa5);padding:0 0.3333333333rem;font-weight:700;color:var(--px-input-text-color, #2c404c)}.text-input:focus{border-color:var(--px-input-border-color--outer--focused, #007acc);background-color:var(--px-input-background-color--focused, #eefbff);color:var(--px-input-text-color--focused, inherit)}.text-input+[type=checkbox],.text-input+[type=radio]{margin-left:1.3333333333rem}.text-input+.label--inline{margin-left:0.6666666667rem}.text-input::-webkit-input-placeholder{padding-top:1px}.text-input::-moz-placeholder{padding-top:1px}.text-input:-ms-input-placeholder{padding-top:1px}.text-input::-ms-input-placeholder{padding-top:1px}.text-input::placeholder{padding-top:1px}.text-input::-ms-clear{display:none}textarea{width:100%;color:var(--px-input-text-color, #2c404c);overflow:auto;min-height:calc(120px + 0.6666666667rem);padding:0.3333333333rem 0.6666666667rem;resize:vertical;border:1px solid var(--px-input-border-color--outer, #889aa5);border-radius:0}textarea:focus{color:var(--px-input-text-color--focused, inherit);background-color:var(--px-input-textarea-background--focused, #eefbff);border:1px solid var(--px-input-border-color--outer--focused, #007acc)}select{width:auto;padding:0 3em 0 1em;-webkit-box-shadow:none;box-shadow:none;height:calc(2em);cursor:pointer;background-image:url(\"data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2212%22%20viewBox%3D%220%200%20658%201024%22%3E%3Cpath%20fill%3D%22%232c404c%22%20d%3D%22M614.286%20420.571q0%207.429-5.714%2013.143l-266.286%20266.286q-5.714%205.714-13.143%205.714t-13.143-5.714l-266.286-266.286q-5.714-5.714-5.714-13.143t5.714-13.143l28.571-28.571q5.714-5.714%2013.143-5.714t13.143%205.714l224.571%20224.571%20224.571-224.571q5.714-5.714%2013.143-5.714t13.143%205.714l28.571%2028.571q5.714%205.714%205.714%2013.143z%22%2F%3E%3C%2Fsvg%3E\");background-repeat:no-repeat;background-position:calc(100% - 0.6666666667rem) 37.5%;color:var(--px-select-text-color, #2c404c);background-color:var(--px-select-background-color, #d8e0e5);border:var(--px-select-border-color, transparent);border-radius:0;-webkit-appearance:none;-moz-appearance:none;border-radius:0px}select:hover{background-color:var(--px-select-background-color--hover, #a3b5bf)}select:active{background-color:var(--px-select-background-color--active, #889aa5)}select:disabled,select[readonly]{background-image:url(\"data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2212%22%20viewBox%3D%220%200%20658%201024%22%3E%3Cpath%20fill%3D%22%23c5d1d8%22%20d%3D%22M614.286%20420.571q0%207.429-5.714%2013.143l-266.286%20266.286q-5.714%205.714-13.143%205.714t-13.143-5.714l-266.286-266.286q-5.714-5.714-5.714-13.143t5.714-13.143l28.571-28.571q5.714-5.714%2013.143-5.714t13.143%205.714l224.571%20224.571%20224.571-224.571q5.714-5.714%2013.143-5.714t13.143%205.714l28.571%2028.571q5.714%205.714%205.714%2013.143z%22%2F%3E%3C%2Fsvg%3E\");color:var(--px-input-text-color--disabled, rgba(103, 126, 140, 0.6))}select[multiple]{padding:0.3333333333rem;cursor:pointer;border:none;background:var(--px-multiselect-background-color, #ebeff2)}select::-ms-expand{display:none}\@-moz-document url-prefix(){select{padding-top:0.4em;padding-bottom:0.4em}}\@media all and (-ms-high-contrast: none), (-ms-high-contrast: active){select{padding-top:0.4em;padding-bottom:0.4em}}.text-input+[type=submit],.text-input [type=cancel],.label--inline+[type=submit],.label--inline [type=cancel]{margin-left:1.3333333333rem}[type=checkbox],[type=radio]{margin:0;padding:0;font-size:1.05em;cursor:pointer;outline:none}[type=checkbox][disabled],[type=radio][disabled]{cursor:not-allowed}[type=checkbox][disabled]+.label--inline,[type=radio][disabled]+.label--inline{cursor:not-allowed}[type=checkbox]+.label--inline{padding-left:5px}[type=radio]+.label--inline{padding-left:3px}.form-field__help{display:inline-block;margin-top:0;margin-left:0.3333333333rem;font-size:12px;line-height:inherit;color:var(--px-input-text-color--help, #677e8c)}.input--tiny{max-width:7.0666666667rem}.input--small{max-width:14.1333333333rem}.input--regular{max-width:21.2rem}.validation-warning{color:var(--px-validation-warning-text-color, #ff8b3a)}.validation-error{color:var(--px-validation-error-text-color, #f34336)}.validation-success{color:var(--px-validation-success-text-color, #7fae1b)}.text-input.validation-warning{border-color:var(--px-validation-warning-text-color, #ff8b3a);background-color:var(--px-validation-warning-background-color, #fff0e6);color:var(--px-input-text-color--focused, inherit)}.text-input.validation-error{border-color:var(--px-validation-error-text-color, #f34336);background-color:var(--px-validation-error-background-color, #ffecec);color:var(--px-input-text-color--focused, inherit)}.text-input.validation-success{border-color:var(--px-validation-success-text-color, #7fae1b);background-color:var(--px-validation-success-background-color, #ecffee);color:var(--px-input-text-color--focused, inherit)}.btn{display:inline-block;overflow:visible;height:var(--px-btn-height, 2em);min-width:var(--px-btn-min-width, 4.6666666667em);margin:0;border:1px solid var(--px-btn-border-color, transparent);border-radius:0 !important;padding:0 calc(var(--px-btn-height, 2em) / 2);-webkit-box-shadow:var(--px-btn-shadow--light, none);box-shadow:var(--px-btn-shadow--light, none);font:inherit;line-height:calc(var(--px-btn-height, 2em) - 2px);-webkit-font-smoothing:antialiased;cursor:pointer;text-align:center;text-decoration:none;text-transform:none;white-space:nowrap;background-color:var(--px-btn-background, #d8e0e5);-webkit-transition:background 0.4s, border-color 0.4s, color 0.4s;transition:background 0.4s, border-color 0.4s, color 0.4s}.btn,.btn:link,.btn:visited,.btn:hover,.btn:active{color:var(--px-btn-color, #2c404c)}.btn:hover,.btn:focus{border-color:var(--px-btn-border-color--hover, transparent);-webkit-box-shadow:var(--px-btn-shadow, none);box-shadow:var(--px-btn-shadow, none);background-color:var(--px-btn-background--hover, #a3b5bf)}.btn:active{border-color:var(--px-btn-border-color--pressed, transparent);-webkit-box-shadow:none;box-shadow:none;background-color:var(--px-btn-background--pressed, #889aa5)}.btn:active{outline:none}\@-moz-document url-prefix(){.btn:not(button){line-height:1.8em}}button.btn{-webkit-appearance:button}.btn+.btn{margin-left:0.6666666667rem}.btn--bare{height:var(--px-btn-height, 2em);border:0 !important;border-radius:0 !important;line-height:inherit;padding-left:0;padding-right:0;-webkit-box-shadow:none;box-shadow:none;background:none;outline:none}.btn--bare,.btn--bare:link,.btn--bare:visited,.btn--bare:hover,.btn--bare:active,.btn--bare:focus{-webkit-box-shadow:none;box-shadow:none;background:none;outline:none}.btn--bare,.btn--bare:link,.btn--bare:visited{color:var(--px-btn-bare-color, #2c404c)}.btn--bare:hover,.btn--bare:focus{color:var(--px-btn-bare-color--hover, #007acc)}.btn--bare:active{color:var(--px-btn-bare-color--pressed, #003d66)}.btn--bare--primary{height:var(--px-btn-height, 2em);border:0 !important;border-radius:0 !important;line-height:inherit;padding-left:0;padding-right:0;-webkit-box-shadow:none;box-shadow:none;background:none;outline:none}.btn--bare--primary,.btn--bare--primary:link,.btn--bare--primary:visited,.btn--bare--primary:hover,.btn--bare--primary:active,.btn--bare--primary:focus{-webkit-box-shadow:none;box-shadow:none;background:none;outline:none}.btn--bare--primary,.btn--bare--primary:link,.btn--bare--primary:visited{color:var(--px-btn-primary-bare-color--visited, #007acc)}.btn--bare--primary:hover,.btn--bare--primary:focus{color:var(--px-btn-primary-bare-color--hover, #005c99)}.btn--bare--primary:active,.btn--bare--primary:focus{color:var(--px-btn-primary-bare-color--pressed, #003d66)}.btn--primary{border-color:var(--px-btn-primary-border-color, transparent);-webkit-box-shadow:var(--px-btn-shadow, none);box-shadow:var(--px-btn-shadow, none);background-color:var(--px-btn-primary-background, #364c59)}.btn--primary,.btn--primary:link,.btn--primary:visited,.btn--primary:hover,.btn--primary:active{color:var(--px-btn-primary-color, white)}.btn--primary:hover,.btn--primary:focus{border-color:transparent;background-color:var(--px-btn-primary-background--hover, #23343f)}.btn--primary:active{border-color:transparent;background-color:var(--px-btn-primary-background--pressed, #121f26)}.btn--tertiary{border-color:var(--px-btn-tertiary-border-color, #889aa5);border:1px solid;-webkit-box-shadow:none;box-shadow:none;background-color:var(--px-btn-tertiary-background, transparent)}.btn--tertiary,.btn--tertiary:link,.btn--tertiary:visited,.btn--tertiary:hover,.btn--tertiary:active{border-color:var(--px-btn-tertiary-border-color, #889aa5);color:var(--px-btn-tertiary-color, #007acc)}.btn--tertiary:hover,.btn--tertiary:focus{color:var(--px-btn-tertiary-color--hover, #005c99);border-color:var(--px-btn-tertiary-border-color, #889aa5);-webkit-box-shadow:none;box-shadow:none;background-color:var(--px-btn-tertiary-background--hover, #c5d1d8)}.btn--tertiary:active{color:var(--px-btn-tertiary-color--pressed, #003d66);border-color:var(--px-btn-tertiary-border-color, #889aa5);-webkit-box-shadow:none;box-shadow:none;background-color:var(--px-btn-tertiary-background--pressed, #96a8b2)}.btn--call-to-action{color:var(--px-btn-call-to-action-color, white);border-color:var(--px-btn-call-to-action-border-color, transparent);-webkit-box-shadow:var(--px-btn-shadow, none);box-shadow:var(--px-btn-shadow, none);background-color:var(--px-btn-call-to-action-background, #007acc)}.btn--call-to-action,.btn--call-to-action:link,.btn--call-to-action:visited,.btn--call-to-action:hover,.btn--call-to-action:active{color:var(--px-btn-call-to-action-color, white)}.btn--call-to-action:hover,.btn--call-to-action:focus{background-color:var(--px-btn-call-to-action-background--hover, #005c99)}.btn--call-to-action:active{background-color:var(--px-btn-call-to-action-background--pressed, #003d66)}.btn--disabled,.btn--disabled:link,.btn--disabled:visited,.btn--disabled:hover,.btn--disabled:active,.btn--disabled:focus,.btn[disabled],.btn[disabled]:link,.btn[disabled]:visited,.btn[disabled]:hover,.btn[disabled]:active,.btn[disabled]:focus{color:var(--px-btn-disabled-color, rgba(0, 0, 0, 0.2));border:1px solid;border-color:var(--px-btn-disabled-border-color, rgba(0, 0, 0, 0.2));-webkit-box-shadow:none;box-shadow:none;background-color:var(--px-btn-disabled-background, transparent);pointer-events:none}.btn--small{height:1.6666666667em;font-size:0.8rem;line-height:calc(1.6666666667em - 2px)}\@-moz-document url-prefix(){.btn--small{line-height:1.4em}}.btn--large{font-size:1.3333333333rem}.btn--huge{font-size:2rem}.btn--full{width:100%}.btn--icon{height:var(--px-btn-height, 2em);width:var(--px-btn-height, 2em);min-width:var(--px-btn-height, 2em);padding-right:0;padding-left:0;border:var(--px-btn-icon-border, 0px solid) !important;background-color:var(--px-btn-icon-background, transparent) !important}\@-moz-document url-prefix(){.btn--icon{line-height:1.8em}}.actionable{height:auto;border:0;border-radius:0;padding:0;-webkit-box-shadow:none;box-shadow:none;background:none;white-space:nowrap;line-height:inherit;text-decoration:none;font:inherit;-webkit-font-smoothing:antitodoed;cursor:pointer;outline:none;-webkit-transition:color 0.4s;transition:color 0.4s}.actionable,.actionable:link,.actionable:visited,.actionable:active,.actionable:focus{color:var(--px-actionable-text-color, #007acc);outline:inherit}.actionable:hover{color:var(--px-actionable-text-color--hover, #005c99)}.actionable:active{color:var(--px-actionable-text-color--active, #003d66)}.actionable--select,.actionable--select:link,.actionable--select:visited,.actionable--select:active,.actionable--select:focus{color:var(--px-selectable-text-color, #09819c)}.actionable--select:hover{color:var(--px-selectable-text-color--hover, #065769)}.actionable--select:active{color:var(--px-selectable-text-color--active, #032c36)}.actionable--action,.actionable--action:link,.actionable--action:visited,.actionable--action:active,.actionable--action:focus,.actionable--secondary,.actionable--secondary:link,.actionable--secondary:visited,.actionable--secondary:active,.actionable--secondary:focus{color:var(--px-actionable-alt-text-color, #2c404c)}.actionable--action:hover,.actionable--secondary:hover{color:var(--px-actionable-alt-text-color--hover, #007acc)}.actionable--action:active,.actionable--secondary:active{color:var(--px-actionable-alt-text-color--active, #003d66)}.actionable--disabled,.actionable--disabled:link,.actionable--disabled:visited,.actionable--disabled:hover,.actionable--disabled:active,.actionable[disabled],.actionable[disabled]:link,.actionable[disabled]:visited,.actionable[disabled]:hover,.actionable[disabled]:active{cursor:not-allowed;color:var(--px-actionable-disabled-text-color, rgba(0, 0, 0, 0.3))}.actionable--small{font-size:0.8rem}.actionable--large{font-size:1.3333333333rem}.actionable--huge{font-size:2rem}.text-input{-webkit-box-sizing:border-box;box-sizing:border-box}:host{position:relative;display:block;height:var(--px-data-grid-height, auto);background:var(--px-base-background-color, white);--px-data-grid-padding-top:12px;--px-data-grid-padding-bottom:12px;--px-data-grid-padding-left:12px;--px-data-grid-padding-right:12px}:host(.ellipsis){--px-data-grid-cell-default-column-overflow:hidden;--px-data-grid-cell-default-column-white-space:nowrap;--px-data-grid-cell-default-column-text-overflow:ellipsis;--px-data-grid-deault-content-wrapper-display:block;--px-data-grid-cell-string-column-text-overflow:ellipsis;--px-data-grid-cell-string-column-white-space:nowrap;--px-data-grid-cell-string-column-overflow:hidden;--px-data-grid-cell-number-column-text-overflow:ellipsis;--px-data-grid-cell-number-column-white-space:nowrap;--px-data-grid-cell-number-column-overflow:hidden;--px-data-grid-cell-date-column-text-overflow:ellipsis;--px-data-grid-cell-date-column-white-space:nowrap;--px-data-grid-cell-date-column-overflow:hidden}px-data-grid-cell-content-wrapper{margin-top:calc(0px - var(--px-data-grid-padding-top));margin-bottom:calc(0px - var(--px-data-grid-padding-top));margin-left:calc(0px - var(--px-data-grid-padding-top));margin-right:calc(0px - var(--px-data-grid-padding-top));padding-top:var(--px-data-grid-padding-top);padding-bottom:var(--px-data-grid-padding-bottom);padding-left:var(--px-data-grid-padding-left);padding-right:var(--px-data-grid-padding-right);min-height:100%;display:var(--px-data-grid-default-content-wrapper-display, block);-ms-flex-align:start;align-items:flex-start;background-color:var(--px-data-grid-cell-dragging-color, var(--px-data-grid-highlight-background-color, transparent));line-height:16px;overflow:var(--px-data-grid-cell-default-column-overflow, hidden);white-space:var(--px-data-grid-cell-default-column-white-space, normal);word-wrap:var(--px-data-grid-cell-default-column-word-wrap, break-word);text-overflow:var(--px-data-grid-cell-default-column-text-overflow, clip);word-break:var(--px-data-grid-cell-default-column-word-break, break-word)}vaadin-grid-tree-toggle:not([leaf])>px-data-grid-cell-content-wrapper{background:var(--px-data-grid-background-color-striped--groupby, #c3c3c3) !important}vaadin-grid-tree-toggle[leaf]{margin-left:calc(0px - var(--vaadin-grid-tree-toggle-level-offset))}.action-bar{display:-ms-flexbox;display:flex;-ms-flex-wrap:nowrap;flex-wrap:nowrap;-ms-flex-pack:start;justify-content:flex-start;margin-bottom:20px}.action-bar__right{margin-left:auto;display:-ms-flexbox;display:flex}.action-bar__right>px-dropdown{margin-left:15px}px-spinner{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%, -50%);transform:translate(-50%, -50%)}px-spinner[hidden]{display:none}vaadin-checkbox{height:auto;outline:none}vaadin-checkbox.select-all-checkbox{margin-top:8px}vaadin-radio-button{height:12px;width:12px}vaadin-grid[auto-height]{height:auto}px-icon{--iron-icon-stroke-color:var(--px-data-grid-cell-text-color--sorted);cursor:pointer}.toggle-details{color:var(--px-base-text-color, #000);margin:-10px;cursor:pointer;display:-ms-flexbox;display:flex;-ms-flex-positive:1;flex-grow:1;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center}.toggle-details:focus{outline:none}.toggle-details>px-icon{padding:0;margin:0}:host([flex-to-size]){display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;height:var(--px-data-grid-height, 100%)}:host([flex-to-size]) vaadin-grid{display:-ms-flexbox;display:flex;height:100%;-ms-flex-positive:1;flex-grow:1;-ms-flex-negative:1;flex-shrink:1}.action-column-button{height:19px;-ms-flex-positive:0;flex-grow:0;-ms-flex-negative:1;flex-shrink:1;display:block;-ms-flex-item-align:center;align-self:center}.action-column-button px-icon{pointer-events:none;--iron-icon-width:15px;--iron-icon-height:15px;--iron-icon-stroke-color:var(--px-data-grid-action-button-color, rgb(129,129,129))}.action-column-button px-icon.rounded{width:12px;height:12px;--iron-icon-width:12px;--iron-icon-height:12px;border-radius:50%;border:1px solid var(--px-data-grid-action-button-color, #818181);stroke:var(--px-data-grid-action-button-color, #818181);padding:2px}.item-action-dropdown{-ms-flex-item-align:center;align-self:center}.item-action-dropdown px-icon{--iron-icon-height:16px;--iron-icon-width:16px}"}},Xt=class{constructor(e){t(this,e),this.finished=!1,this.size=80}render(){if(!this.finished)return i("svg",{width:this.size,height:this.size,viewBox:"0 0 100 100"},i("circle",{class:"circle1",cx:"50",cy:"50",r:"45"}),i("circle",{class:"circle2",cx:"50",cy:"50",r:"45",transform:"rotate(-45,50,50)"}))}static get style(){return"\@charset \"UTF-8\";:host{color:var(--px-base-text-color, #2c404c);line-height:1.3333333333;-webkit-text-size-adjust:100%;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;text-size-adjust:100%;-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;}html{-webkit-box-sizing:border-box;box-sizing:border-box}*,*:before,*:after{-webkit-box-sizing:inherit;box-sizing:inherit}:host{/*! Comment to prevent cssmin munging this rule with html above and borking Safari */-webkit-box-sizing:border-box;box-sizing:border-box}a{background-color:transparent;}a:link,a:visited{color:#007acc}a:hover{color:#005c99}a:active{color:#003d66}a:active,a:hover{outline:0}.float--right{float:right !important}.float--left{float:left !important}.float--none{float:none !important}.text--left{text-align:left !important}.text--center{text-align:center !important}.text--right{text-align:right !important}.full-height{height:100% !important}.informative{cursor:help !important}.pointer{cursor:pointer !important}.muted{opacity:0.5 !important}.proceed{text-align:right !important}.caps{text-transform:uppercase !important}.hidden{display:none !important;visibility:hidden}.a11y,.visuallyhidden{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}.a11y.focusable:active,.a11y.focusable:focus,.visuallyhidden.focusable:active,.visuallyhidden.focusable:focus{position:static;overflow:visible;width:auto;height:auto;margin:0;clip:auto}\@media screen and (min-width: 45em) and (max-width: 63.9375em){.a11y-lap,.visuallyhidden-lap{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}}\@media screen and (max-width: 63.9375em){.a11y-portable,.visuallyhidden-portable{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}}\@media screen and (min-width: 64em){.a11y-desk,.visuallyhidden-desk{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}}\@media screen and (max-width: 44.9375em){.a11y-palm,.visuallyhidden-palm{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}}\@media screen and (min-width: 45em){.a11y-lap-and-up,.visuallyhidden-lap-and-up{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}}\@media screen and (min-width: 64em){.a11y-desk-and-up,.visuallyhidden-desk-and-up{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}}\@media screen and (min-width: 120em){.a11y-large-and-up,.visuallyhidden-large-and-up{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}}\@media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi), (min-resolution: 2dppx){.a11y-retina,.visuallyhidden-retina{position:absolute !important;overflow:hidden !important;width:1px !important;height:1px !important;margin:-1px !important;border:0 !important;padding:0 !important;clip:rect(0 0 0 0) !important}}.invisible{visibility:hidden !important}.truncate{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}:host{display:block}.circle1{fill:none;stroke:var(--px-spinner-background-color, lightgray);stroke-width:5px}.circle2{fill:none;stroke:var(--px-spinner-fill-color, black);stroke-width:5px;stroke-dasharray:283;stroke-dashoffset:566;-webkit-animation:rotate 1.5s infinite cubic-bezier(0.78, 0.13, 0.16, 0.87);animation:rotate 1.5s infinite cubic-bezier(0.78, 0.13, 0.16, 0.87)}\@-webkit-keyframes rotate{to{stroke-dashoffset:0}}\@keyframes rotate{to{stroke-dashoffset:0}}.spinner-container{font-size:10px;position:relative;text-indent:-9999em;border-top:5px solid var(--px-spinner-background-color, lightgray);border-right:5px solid var(--px-spinner-background-color, lightgray);border-bottom:5px solid var(--px-spinner-background-color, lightgray);border-left:5px solid var(--px-spinner-fill-color, black);-webkit-transform:translateZ(0);transform:translateZ(0);-webkit-animation:load8 1.1s infinite cubic-bezier(0.78, 0.13, 0.16, 0.87);animation:load8 1.1s infinite cubic-bezier(0.78, 0.13, 0.16, 0.87)}.spinner-container,.spinner-container:after{border-radius:50%;width:10em;height:10em}\@-webkit-keyframes load8{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}\@keyframes load8{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}"}};export{Jt as px_data_grid,Xt as px_spinner};